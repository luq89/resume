package com.example.lukas.resume.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.lukas.resume.R;
import com.example.lukas.resume.helpers.Utils;

/**
 * Created by Lukas on 19.02.2016.
 */
public class CustomPagerAdapterSkill extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    String[] mResources;
    ScrollView sV_container;


    public CustomTag selectedOption = null;


    public CustomPagerAdapterSkill(Context context, String[] resources) {
        mContext = context;
        mResources = resources;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mResources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {


        View rootView = mLayoutInflater.inflate(R.layout.pager_item_skill, container, false);
        sV_container = (ScrollView) rootView.findViewById(R.id.pager_item_skill_sv_container);
        switch (position) {

            case 0:
                sV_container.addView(getTab(mContext.getResources().getStringArray(R.array.fragment_formgenerator_dialog_skills_array_subcategory_general), position));
                break;
            case 1:
                sV_container.addView(getTab(mContext.getResources().getStringArray(R.array.fragment_formgenerator_dialog_skills_array_subcategory_development), position));
                sV_container.setScrollbarFadingEnabled(false);
                break;
            case 2:
                sV_container.addView(getTab(mContext.getResources().getStringArray(R.array.fragment_formgenerator_dialog_skills_array_subcategory_design), position));
                sV_container.setScrollbarFadingEnabled(false);
                break;
        }

        container.addView(rootView);
        return rootView;
    }


    class CustomTag {

        int catPos, subCatPos;

        public CustomTag(int catPos, int subCatPos) {
            this.catPos = catPos;
            this.subCatPos = subCatPos;
        }


        public int getCatPos() {
            return catPos;
        }

        public int getSubCatPos() {
            return subCatPos;
        }
    }

    public int getSelectedCategory() {

        if (null != selectedOption) {
            return selectedOption.getCatPos();
        }
        return -1;

    }

    public int getSelectedSubCategory() {
        if (null != selectedOption) {
            return selectedOption.getSubCatPos();
        }
        return -1;
    }


    private LinearLayout getTab(String[] arr, final int tapPosition) {

        LinearLayout ll = new LinearLayout(mContext);
        ll.setOrientation(LinearLayout.VERTICAL);

        Utils mUtils = Utils.newInstance(mContext);

        int pos = 0;

        for (String ele : arr) {

            RelativeLayout ll_row = new RelativeLayout(mContext);
            RelativeLayout.LayoutParams lp_row = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            ll_row.setPadding(50, 25, 25, 25);

            ImageView iV_icon = new ImageView(mContext);
            iV_icon.setImageBitmap(Utils.optionToBitmap(ele, mContext.getResources().getDimensionPixelSize(R.dimen.fab_icon_size), mContext.getResources().getDimensionPixelSize(R.dimen.fab_icon_size)));
            RelativeLayout.LayoutParams lp_iVicon = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            lp_iVicon.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);


            TextView tV = new TextView(mContext);
            tV.setTextAppearance(mContext.getApplicationContext(), R.style.TextAppearance_AppCompat_Medium);
            RelativeLayout.LayoutParams lp_tv = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            lp_tv.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
            lp_tv.leftMargin = 75;
            tV.setTextColor(mContext.getResources().getColor(R.color.Font_Dark));
            tV.setText(ele);


            ImageView iV_check = new ImageView(mContext);
            iV_check.setImageBitmap(Bitmap.createScaledBitmap(((BitmapDrawable) mContext.getResources().getDrawable(R.drawable.ic_tick)).getBitmap(), mContext.getResources().getDimensionPixelOffset(R.dimen.fab_icon_size), mContext.getResources().getDimensionPixelOffset(R.dimen.fab_icon_size), false));
            iV_check.setColorFilter(mContext.getResources().getColor(R.color.White), PorterDuff.Mode.SRC_ATOP);
            iV_check.setVisibility(View.INVISIBLE);

            RelativeLayout.LayoutParams lp_iVcheck = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            lp_iVcheck.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
            ll_row.addView(iV_icon, lp_iVicon);
            ll_row.addView(tV, lp_tv);
            ll_row.addView(iV_check, lp_iVcheck);
            ll_row.setBackgroundColor(pos % 2 == 0 ? mContext.getResources().getColor(R.color.Tint5) : mContext.getResources().getColor(R.color.Tint6));
            ll_row.setTag(new CustomTag(tapPosition, pos));

            ll.addView(ll_row, lp_row);

            pos++;

            ll_row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    LinearLayout lparent = (LinearLayout) v.getParent();
                    int count = lparent.getChildCount();

                    for (int i = 0; i < count; i++) {
                        lparent.getChildAt(i).setBackgroundColor(i % 2 == 0 ? mContext.getResources().getColor(R.color.Tint5) : mContext.getResources().getColor(R.color.Tint6));
                        ((ImageView) ((RelativeLayout) lparent.getChildAt(i)).getChildAt(0)).setColorFilter(mContext.getResources().getColor(R.color.Black), PorterDuff.Mode.SRC_ATOP);
                        ((TextView) ((RelativeLayout) lparent.getChildAt(i)).getChildAt(1)).setTextColor(mContext.getResources().getColor(R.color.Font_Dark));
                        ((RelativeLayout) lparent.getChildAt(i)).getChildAt(2).setVisibility(View.INVISIBLE);
                    }

                    ((ImageView) ((RelativeLayout) v).getChildAt(0)).setColorFilter(mContext.getResources().getColor(R.color.Font_Light), PorterDuff.Mode.SRC_ATOP);
                    ((TextView) ((RelativeLayout) v).getChildAt(1)).setTextColor(mContext.getResources().getColor(R.color.Font_Light));
                    ((RelativeLayout) v).getChildAt(2).setVisibility(View.VISIBLE);
                    v.setBackgroundColor(mContext.getResources().getColor(R.color.Tint7));

                    selectedOption = (CustomTag) v.getTag();
                }
            });
        }
        return ll;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return mResources[position].toUpperCase();
    }


}
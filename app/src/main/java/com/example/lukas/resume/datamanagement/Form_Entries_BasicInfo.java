package com.example.lukas.resume.datamanagement;

/**
 * Created by Lukas on 06.05.2015.
 */

public class Form_Entries_BasicInfo implements UserEntry {
    private long id;


    private String basicInformation_Profession;
    private String basicInformation_Name;
    private String basicInformation_Born;
    private String basicInformation_Address;
    private String basicInformation_Email;
    private String basicInformation_Phone;

    @Override
    public long getId() {
        return null != Long.valueOf(id) ? id : new Long(0);
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    //Name
    public String getName() {
        return basicInformation_Name;
    }

    public void setName(String name) {
        this.basicInformation_Name = name;
    }

    //Profession
    public String getProfession() {
        return basicInformation_Profession;
    }

    public void setProfession(String profession) {
        this.basicInformation_Profession = profession;
    }


    //Born
    public String getBorn() {
        return basicInformation_Born;
    }

    public void setBorn(String born) {
        this.basicInformation_Born = born;
    }

    //Address
    public String getAddress() {
        return basicInformation_Address;
    }

    public void setAddress(String address) {
        this.basicInformation_Address = address;
    }

    //Email
    public String getEmail() {
        return basicInformation_Email;
    }

    public void setEmail(String email) {
        this.basicInformation_Email = email;
    }

    //Phone
    public String getPhone() {
        return basicInformation_Phone;
    }

    public void setPhone(String phone) {
        this.basicInformation_Phone = phone;
    }


    @Override
    public String toCustomString() {
        return "Name: " + basicInformation_Name + " ; "
                + "Profession: " + basicInformation_Profession + " ; "
                + "Born: " + basicInformation_Born + " ; "
                + "Address: " + basicInformation_Address + " ; "
                + "Email: " + basicInformation_Email + " ; "
                + "Phone: " + basicInformation_Phone + " ; "
                ;
    }

}

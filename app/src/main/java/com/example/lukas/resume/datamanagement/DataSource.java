package com.example.lukas.resume.datamanagement;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.example.lukas.resume.helpers.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lukas on 06.05.2015.
 */
public class DataSource {

    // Database fields
    private SQLiteDatabase database;
    private SQLiteHelper dbHelper;

    //Helpers
    private Utils utils;

    public DataSource(Context context) {
        this.dbHelper = new SQLiteHelper(context);
        this.utils = Utils.newInstance(context);
    }


    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();

    }

    public void close() {
        dbHelper.close();
    }

    public void reset() {
        dbHelper.onUpgrade(database, database.getVersion(), database.getVersion());
    }

    public void insert(String tableName, ContentValues val) {
        database.insert(tableName, null, val);
    }


    public void update(String tableName, ContentValues values, long insertId) {

        database.update(tableName, values, SQLiteHelper.COLUMN_BASICINFORMATION_ID + " = ?",
                new String[]{String.valueOf(insertId)});
    }


    public void delete(String tableName, long id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(tableName, SQLiteHelper.COLUMN_GENERAL_ID + " = ?",
                new String[]{String.valueOf(id)});
    }

    public UserEntry getEntry(String tableName, String key, long id) {
        UserEntry result = null;
        String selectQuery;
        Cursor cursor;

        selectQuery = "SELECT  * FROM " + tableName + " WHERE " + key +
                " = " + id;

        cursor = database.rawQuery(selectQuery, null);

        if (null != cursor) {
            cursor.moveToFirst();
            result = tableNameToCursor(tableName, cursor);
        }


        return result;
    }


    public List<? extends UserEntry> getSingle(String tableName) {
        List<UserEntry> entriesList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + tableName;
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (null != cursor) {
            cursor.moveToFirst();
        }

        entriesList.add(tableNameToCursor(tableName, cursor));
        return entriesList;
    }

    public Form_Entries_BasicInfo getBasicInformationEntry() {

        String selectQuery = "SELECT  * FROM " + SQLiteHelper.TABLE_BASICINFORMATION;
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (null != cursor) {
            cursor.moveToFirst();
        }
        return getBasicInformationEntry(cursor);
    }

    public List<? extends UserEntry> getAll(String tableName) {
        List<UserEntry> entriesList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + tableName;
        Cursor cursor = null;
        try {
            cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    UserEntry entry = tableNameToCursor(tableName, cursor);
                    entriesList.add(entry);
                    cursor.moveToNext();
                }
                while (!cursor.isAfterLast());
            }
            // make sure to close the cursor
            cursor.close();
        } catch (SQLException e) {

        }
        return entriesList;

    }


    public List<Form_Entries_Experience> getAllExperienceEntries() {
        List<Form_Entries_Experience> experienceEntries = new ArrayList<Form_Entries_Experience>();

        String selectQuery = "SELECT  * FROM " + SQLiteHelper.TABLE_EXPERIENCE;

        try {

            Cursor cursor = database.rawQuery(selectQuery, null);


            if (cursor.moveToFirst()) {
                do {
                    Form_Entries_Experience experienceEntry = cursorToExperience(cursor);
                    experienceEntries.add(experienceEntry);
                    cursor.moveToNext();
                }
                while (!cursor.isAfterLast());
            }
            // make sure to close the cursor
            cursor.close();

        } catch (SQLException e) {

        }
        return experienceEntries;

    }

    public List<Form_Entries_Education> getAllEducationEntries() {
        List<Form_Entries_Education> educationEntries = new ArrayList<>();
        Cursor cursor = null;
        String selectQuery = "SELECT  * FROM " + SQLiteHelper.TABLE_EDUCATION;

        try {
            cursor = database.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    Form_Entries_Education educationEntry = cursorToEducation(cursor);
                    educationEntries.add(educationEntry);
                    cursor.moveToNext();
                }
                while (!cursor.isAfterLast());
            }
            cursor.close();
        } catch (SQLException e) {
        }

        return educationEntries;

    }

    public List<Form_Entries_Skill> getAllSkillEntries() {
        List<Form_Entries_Skill> skillEntries = new ArrayList<>();
        Cursor cursor = null;
        String selectQuery = "SELECT  * FROM " + SQLiteHelper.TABLE_SKILL + " ORDER BY category ASC, subcategory ASC";

        try {
            cursor = database.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    Form_Entries_Skill skillEntry = cursorToSkill(cursor);
                    skillEntries.add(skillEntry);
                    cursor.moveToNext();
                }
                while (!cursor.isAfterLast());
            }
            cursor.close();
        } catch (SQLException e) {
        }
        return skillEntries;
    }

    public List<Form_Entries_Language> getAllLanguageEntries() {
        List<Form_Entries_Language> languageEntries = new ArrayList<>();
        Cursor cursor;
        String selectQuery = "SELECT  * FROM " + SQLiteHelper.TABLE_LANGUAGE + " ORDER BY name ASC";

        try {
            cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    Form_Entries_Language languageEntry = cursorToLanguage(cursor);
                    languageEntries.add(languageEntry);
                    cursor.moveToNext();
                }
                while (!cursor.isAfterLast());
            }
            // make sure to close the cursor
            cursor.close();
        } catch (SQLException e) {

        }
        return languageEntries;
    }


    public List<Form_Entries_Connect> getAllConnectEntries() {
        List<Form_Entries_Connect> connectEntries = new ArrayList<Form_Entries_Connect>();

        String selectQuery = "SELECT  * FROM " + SQLiteHelper.TABLE_CONNECT;

        try {

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    Form_Entries_Connect connectEntry = cursorToConnect(cursor);
                    connectEntries.add(connectEntry);
                    cursor.moveToNext();
                }
                while (!cursor.isAfterLast());
            }
            // make sure to close the cursor
            cursor.close();

        } catch (SQLException e) {

        }

        return connectEntries;

    }


    public void replace(String tableName, String keys, ContentValues value) {
        database.replace(tableName, keys, value);
    }


    public Form_Entries_General getGeneralEntry() {

        String selectQuery = "SELECT  * FROM " + SQLiteHelper.TABLE_GENERAL;
        Cursor cursor = database.rawQuery(selectQuery, null);
        Form_Entries_General entry = null;
        if (null != cursor) {
            cursor.moveToFirst();
        }

        entry = cursorToGeneral(cursor);

        return entry;
    }

    private String getString(Cursor cursor, String columnName) {
        String result;

        try {
            result = cursor.getString(cursor.getColumnIndex(columnName));
        } catch (CursorIndexOutOfBoundsException e) {
            result = Utils.getDefaultStringByTableColumnName(columnName);
        }

        return result;
    }

    private Long getLong(Cursor cursor, String columnName) {
        Long result;

        try {
            result = cursor.getLong(cursor.getColumnIndex(columnName));
        } catch (CursorIndexOutOfBoundsException e) {
            result = new Long(0);
        }

        return result;
    }


    private Form_Entries_BasicInfo getBasicInformationEntry(Cursor cursor) {
        Form_Entries_BasicInfo formEntry = new Form_Entries_BasicInfo();
        formEntry.setId(getLong(cursor, SQLiteHelper.COLUMN_BASICINFORMATION_ID));
        formEntry.setName(getString(cursor, SQLiteHelper.COLUMN_BASICINFORMATION_NAME));
        formEntry.setProfession(getString(cursor, SQLiteHelper.COLUMN_BASICINFORMATION_PROFESSION));
        formEntry.setBorn(getString(cursor, SQLiteHelper.COLUMN_BASICINFORMATION_BORN));
        formEntry.setAddress(getString(cursor, SQLiteHelper.COLUMN_BASICINFORMATION_ADDRESS));
        formEntry.setEmail(getString(cursor, SQLiteHelper.COLUMN_BASICINFORMATION_EMAIL));
        formEntry.setPhone(getString(cursor, SQLiteHelper.COLUMN_BASICINFORMATION_PHONE));
        return formEntry;
    }


    private Form_Entries_General cursorToGeneral(Cursor cursor) throws CursorIndexOutOfBoundsException {
        Form_Entries_General formGeneralEntry = new Form_Entries_General();
        formGeneralEntry.setId(getLong(cursor, SQLiteHelper.COLUMN_GENERAL_ID));
        formGeneralEntry.setTypeface(getString(cursor, SQLiteHelper.COLUMN_GENERAL_TYPEFACE));
        formGeneralEntry.setAvatarImage(getString(cursor, SQLiteHelper.COLUMN_GENERAL_AVATARPATH));
        formGeneralEntry.setDateFormat(getString(cursor, SQLiteHelper.COLUMN_GENERAL_DATEFORMAT));
        formGeneralEntry.setColorTheme(getString(cursor, SQLiteHelper.COLUMN_GENERAL_COLORTHEME));
        formGeneralEntry.setShowStartDialog(getString(cursor, SQLiteHelper.COLUMN_GENERAL_SHOWSTARTDIALOG));
        return formGeneralEntry;
    }


    private Form_Entries_Education cursorToEducation(Cursor cursor) {
        Form_Entries_Education formEducationEntry = new Form_Entries_Education();
        formEducationEntry.setId(cursor.getLong(cursor.getColumnIndex(SQLiteHelper.COLUMN_EDUCATION_ID)));
        formEducationEntry.setDateBeginn(cursor.getString(cursor.getColumnIndex(SQLiteHelper.COLUMN_EDUCATION_DATEBEGINN)));
        formEducationEntry.setDateEnd(cursor.getString(cursor.getColumnIndex(SQLiteHelper.COLUMN_EDUCATION_DATEEND)));
        formEducationEntry.setLocation(cursor.getString(cursor.getColumnIndex(SQLiteHelper.COLUMN_EDUCATION_LOCATION)));
        formEducationEntry.setInstitution(cursor.getString(cursor.getColumnIndex(SQLiteHelper.COLUMN_EDUCATION_INSTITUTION)));
        formEducationEntry.setDegree(cursor.getString(cursor.getColumnIndex(SQLiteHelper.COLUMN_EDUCATION_DEGREE)));
        return formEducationEntry;
    }


    private Form_Entries_Experience cursorToExperience(Cursor cursor) {
        Form_Entries_Experience formExperienceEntry = new Form_Entries_Experience();
        formExperienceEntry.setId(cursor.getLong(cursor.getColumnIndex(SQLiteHelper.COLUMN_EXPERIENCE_ID)));
        formExperienceEntry.setDateBeginn(cursor.getString(cursor.getColumnIndex(SQLiteHelper.COLUMN_EXPERIENCE_DATEBEGINN)));
        formExperienceEntry.setDateEnd(cursor.getString(cursor.getColumnIndex(SQLiteHelper.COLUMN_EXPERIENCE_DATEEND)));
        formExperienceEntry.setBorn(cursor.getString(cursor.getColumnIndex(SQLiteHelper.COLUMN_EXPERIENCE_LOCATION)));
        formExperienceEntry.setSubject(cursor.getString(cursor.getColumnIndex(SQLiteHelper.COLUMN_EXPERIENCE_SUBJECT)));
        formExperienceEntry.setDescription(cursor.getString(cursor.getColumnIndex(SQLiteHelper.COLUMN_EXPERIENCE_DESCRIPTION)));
        return formExperienceEntry;
    }

    private Form_Entries_Skill cursorToSkill(Cursor cursor) {
        Form_Entries_Skill formSkillEntry = new Form_Entries_Skill();
        formSkillEntry.setId(cursor.getLong(cursor.getColumnIndex(SQLiteHelper.COLUMN_SKILL_ID)));
        formSkillEntry.setCategory(cursor.getString(cursor.getColumnIndex(SQLiteHelper.COLUMN_SKILL_CATEGORY)));
        formSkillEntry.setSubCategory(cursor.getString(cursor.getColumnIndex(SQLiteHelper.COLUMN_SKILL_SUBCATEGORY)));
        formSkillEntry.setRating(cursor.getDouble(cursor.getColumnIndex(SQLiteHelper.COLUMN_SKILL_RATING)));
        return formSkillEntry;
    }

    private Form_Entries_Language cursorToLanguage(Cursor cursor) {
        Form_Entries_Language formLanguageEntry = new Form_Entries_Language();
        formLanguageEntry.setId(cursor.getLong(cursor.getColumnIndex(SQLiteHelper.COLUMN_LANGUAGE_ID)));
        formLanguageEntry.setLanguage(cursor.getString(cursor.getColumnIndex(SQLiteHelper.COLUMN_LANGUAGE_NAME)));
        formLanguageEntry.setRating(cursor.getDouble(cursor.getColumnIndex(SQLiteHelper.COLUMN_LANGUAGE_RATING)));
        return formLanguageEntry;
    }

    private Form_Entries_Connect cursorToConnect(Cursor cursor) {
        Form_Entries_Connect formConnectEntry = new Form_Entries_Connect();
        formConnectEntry.setId(cursor.getLong(cursor.getColumnIndex(SQLiteHelper.COLUMN_CONNECT_ID)));
        formConnectEntry.setOption(cursor.getString(cursor.getColumnIndex(SQLiteHelper.COLUMN_CONNECT_OPTION)));
        formConnectEntry.setUser(cursor.getString(cursor.getColumnIndex(SQLiteHelper.COLUMN_CONNECT_USER)));
        return formConnectEntry;
    }


    private UserEntry tableNameToCursor(String tableName, Cursor cursor) {
        UserEntry result = null;

        switch (tableName) {
            case SQLiteHelper.TABLE_BASICINFORMATION:
                result = getBasicInformationEntry(cursor);
                break;

            case SQLiteHelper.TABLE_CONNECT:
                result = cursorToConnect(cursor);
                break;

            case SQLiteHelper.TABLE_EDUCATION:
                result = cursorToEducation(cursor);
                break;

            case SQLiteHelper.TABLE_EXPERIENCE:
                result = cursorToExperience(cursor);
                break;

            case SQLiteHelper.TABLE_GENERAL:
                result = cursorToGeneral(cursor);
                break;

            case SQLiteHelper.TABLE_LANGUAGE:
                result = cursorToLanguage(cursor);
                break;

            case SQLiteHelper.TABLE_SKILL:
                result = cursorToSkill(cursor);
                break;

            default:
                break;

        }

        return result;

    }

    public boolean isEmpty(String tableName) {
        boolean isEmpty = true;
        Cursor cur = database.rawQuery("SELECT COUNT(*) FROM " + tableName, null);
        if (cur != null && cur.moveToFirst()) {
            isEmpty = (cur.getInt(0) == 0);
        }
        cur.close();
        return isEmpty;
    }

    public int getCount(String tableName) {
        int count = 0;

        Cursor cur = database.rawQuery("SELECT COUNT(*) FROM " + tableName, null);

        if (cur != null && cur.moveToFirst()) {
            count = cur.getCount();
        }
        cur.close();

        return count;

    }


}

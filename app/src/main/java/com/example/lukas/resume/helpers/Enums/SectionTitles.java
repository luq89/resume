package com.example.lukas.resume.helpers.Enums;

import com.example.lukas.resume.R;

/**
 * Created by luq89 on 17-Mar-17.
 */

public enum SectionTitles {


    BASICINFORMATION {
        private String title = "Basic Information";
        private int colorResource = R.color.Tint4;
        private int dividerColor = R.color.white;

        public String getName() {
            return title;
        }

        public int getResourceColor() {
            return colorResource;
        }

        public int getDividerColor() {
            return dividerColor;
        }
    },

    EDUCATION {
        private String name = "Education";
        private int colorResource = R.color.Tint4;
        private int dividerColor = R.color.white;

        public String getName() {
            return name;
        }

        public int getResourceColor() {
            return colorResource;
        }

        public int getDividerColor() {
            return dividerColor;
        }
    },

    SKILLSET {
        private String title = "Skillset";
        private int colorResource = R.color.Tint4;
        private int dividerColor = R.color.white;

        public String getName() {
            return title;
        }

        public int getResourceColor() {
            return colorResource;
        }

        public int getDividerColor() {
            return dividerColor;
        }
    },

    CONNECT {
        private String title = "Connect";
        private int colorResource = R.color.Tint4;
        private int dividerColor = R.color.white;

        public String getName() {
            return title;
        }

        public int getResourceColor() {
            return colorResource;
        }

        public int getDividerColor() {
            return dividerColor;
        }
    },

    FINISH {
        private String title = "Finish";
        private int colorResource = R.color.Tint4;
        private int dividerColor = R.color.white;

        public String getName() {
            return title;
        }

        public int getResourceColor() {
            return colorResource;
        }

        public int getDividerColor() {
            return dividerColor;
        }
    };

    public abstract String getName();

    public abstract int getResourceColor();

    public abstract int getDividerColor();

}






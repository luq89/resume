package com.example.lukas.resume.helpers;


import android.content.ContentValues;
import android.content.Context;

import com.example.lukas.resume.R;
import com.example.lukas.resume.datamanagement.SQLiteHelper;
import com.example.lukas.resume.datamanagement.UserDataHub;
import com.example.lukas.resume.helpers.Enums.Fonts;

import static com.example.lukas.resume.helpers.Enums.Fonts.CORBEL;
import static com.example.lukas.resume.helpers.Enums.Fonts.JOSEFINSANS;
import static com.example.lukas.resume.helpers.Enums.Fonts.WHITNEYBOOK;

/**
 * Created by luq89 on 17-Mar-17.
 */

public class FontUtils {

    private static Fonts selectedFont;
    private static Context context;


    public static FontUtils newInstance(Context context) {
        return new FontUtils(context);
    }

    private FontUtils(Context context) {
        FontUtils.context = context;
        selectedFont = UserDataHub.getGeneralData().getFont();
    }

    public static Fonts getSelectedFont() {
        return selectedFont;
    }

    public static void setSelectedFont(String newFont) {

        ContentValues contentValues = null;

        if (JOSEFINSANS.equals(newFont)) {
            selectedFont = JOSEFINSANS;
        } else if (WHITNEYBOOK.equals(newFont)) {
            selectedFont = WHITNEYBOOK;
        } else if (CORBEL.equals(newFont)) {
            selectedFont = CORBEL;
        } else {
            selectedFont = JOSEFINSANS;
        }

        if (!selectedFont.equals(UserDataHub.getGeneralData().getFont())) {
            contentValues = new ContentValues();
            contentValues.put(SQLiteHelper.COLUMN_GENERAL_TYPEFACE, selectedFont.getTypefaceName());
            UserDataHub.update(SQLiteHelper.TABLE_GENERAL, contentValues);
        }
    }


    public static String[] getAvailableFonts() {
        return context.getResources().getStringArray(R.array.settings_array_fontstyle);
    }


}

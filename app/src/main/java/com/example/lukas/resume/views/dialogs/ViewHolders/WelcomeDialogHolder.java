package com.example.lukas.resume.views.dialogs.ViewHolders;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;

import com.andexert.expandablelayout.library.ExpandableLayout;
import com.example.lukas.resume.R;
import com.example.lukas.resume.datamanagement.SQLiteHelper;
import com.example.lukas.resume.datamanagement.UserDataHub;
import com.example.lukas.resume.helpers.DateFormatUtils;
import com.example.lukas.resume.helpers.FontUtils;
import com.example.lukas.resume.views.dialogs.ICustomDialog;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class WelcomeDialogHolder implements ICustomDialog {

    private Context context;
    private View mSubView;

    private ExpandableLayout fontStyleExpContainer;
    private LinearLayout fontStyleContent;
    private ExpandableLayout dateFormatExpContainer;
    private ScrollView dateFormatScrollView;
    private LinearLayout dateFormatContent;
    private LinearLayout dismissLinearLayout;
    private TextView saveTextView;
    ExpandableTextView mExpandableTextView;
    Switch mSwitch;

    private String selectedDateFormat;
    boolean selectedSwitchToggle;
    int[] mPagerResources;

    FontUtils fontUtils;
    DateFormatUtils dateFormatUtils;

    public WelcomeDialogHolder(Context context, View subView) {
        this.context = context;
        this.mSubView = subView;
        this.fontUtils = FontUtils.newInstance(context);
        this.dateFormatUtils = DateFormatUtils.newInstance(context);
        readEntry();
        initDialog();
        initOnClickListeners();
    }

    @Override
    public void initDialog() {
        this.dismissLinearLayout = (LinearLayout) mSubView.findViewById(R.id
                .fragment_formgenerator_dialog_start_dismiss);

        this.mExpandableTextView = (ExpandableTextView) mSubView.findViewById(R.id
                .fragment_formgenerator_dialog_start_expandabletextview);
        mExpandableTextView.setText(context.getString(R.string.general_loremipsum));

        this.mExpandableTextView = (ExpandableTextView) mSubView.findViewById(R.id
                .fragment_formgenerator_dialog_start_expandabletextview);
        this.mExpandableTextView.setText(context.getString(R.string.general_loremipsum));


        this.saveTextView = (TextView) mSubView.findViewById(R.id
                .fragment_formgenerator_dialog_start_beginn);

        initFontstyle();
        initDateFormat();
        initSwitch();

    }

    private void initDateFormat() {

        dateFormatExpContainer = (ExpandableLayout) mSubView.findViewById(R.id
                .fragment_formgenerator_dialog_start_expandablelayout_dateformat);
        dateFormatScrollView = (ScrollView) dateFormatExpContainer.getContentLayout().findViewById(R.id.view_dateformat_content_scollview);


        dateFormatContent = new LinearLayout(context);
        dateFormatContent.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout
                .LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        dateFormatScrollView.addView(dateFormatContent, layoutParams);

        ((TextView) dateFormatExpContainer.getHeaderLayout().findViewById(R.id
                .view_dateformat_header_title)).setText(UserDataHub.getGeneralData().getDateFormat());

        setView(ViewMode.DATEFORMAT);

    }

    private void initFontstyle() {

        this.fontStyleExpContainer = (ExpandableLayout) mSubView.findViewById(R.id.welcomedialog_fontstylecontainer);
        this.fontStyleContent = (LinearLayout) fontStyleExpContainer
                .getContentLayout().findViewById(R.id.welcomeDialog_fonstylecontentcontainer);
        this.fontStyleContent.setElevation(12);

        final TextView fontStyleTitle = ((TextView) fontStyleExpContainer.getHeaderLayout().findViewById(R.id.view_fontstyle_header_title));
        fontStyleTitle.setText(UserDataHub.getGeneralData().getTypeface());

        setView(ViewMode.FONTSTYLE);
    }

    private void initSwitch() {
        mSwitch = (Switch) mSubView.findViewById(R.id.fragment_formgenerator_dialog_start_switch);
        mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean newSwitchToggle) {
                selectedSwitchToggle = newSwitchToggle;
            }
        });
    }


    private void setView(ViewMode viewMode) {
        int rowId = 0;

        RelativeLayout row;
        LinearLayout textContainer;

        RelativeLayout.LayoutParams rlLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout.LayoutParams llLayoutParams = new LinearLayout.LayoutParams(LinearLayout
                .LayoutParams
                .WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);


        for (String s : viewMode.getResources()) {
            row = new RelativeLayout(context);
            row.setPadding(50, 25, 25, 25);
            row.setBackgroundColor(getBackgroundColor(s, rowId, viewMode));

            textContainer = new LinearLayout(context);

            TextView textView = new TextView(context);
            textView.setTextAppearance(R.style.TextAppearance_AppCompat_Medium);
            textView.setTextColor(getTextColor(s, viewMode));
            textView.setText(s);


            TextView tVPreview = new TextView(context);
            tVPreview.setTextAppearance(R.style.TextAppearance_AppCompat_Small);

            if (viewMode.equals(ViewMode.DATEFORMAT)) {
                String formattedDate = new SimpleDateFormat(s, Locale.GERMAN).format(new Date()
                        .getTime());

                tVPreview.setText("// " + formattedDate);
            } else if (viewMode.equals((ViewMode.FONTSTYLE))) {
                tVPreview.setText("// " + context.getResources().getString(R.string
                        .general_fontpreview));
                tVPreview.setTypeface(FontUtils.getSelectedFont().getTypeface(context));
            }

            tVPreview.setTextColor(getPreviewTextColor(s, viewMode));
            textContainer.addView(textView, llLayoutParams);


            llLayoutParams.leftMargin = 100;
            textContainer.addView(tVPreview, llLayoutParams);

            row.addView(textContainer);
            row.addView(getImageView(s), getLayoutParamsForCheckImageView());

            if (viewMode.equals(ViewMode.DATEFORMAT)) {
                dateFormatContent.addView(row, rlLayoutParams);
            } else if (viewMode.equals((ViewMode.FONTSTYLE))) {
                fontStyleContent.addView(row, rlLayoutParams);
            }

            setOnClickListenerForRow(row, viewMode);
            rowId++;

        }

    }


    private void setOnClickListenerForRow(View view, final ViewMode mode) {

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LinearLayout lparent = (LinearLayout) v.getParent();
                int count = lparent.getChildCount();

                for (int i = 0; i < count; i++) {

                    lparent.getChildAt(i).setBackgroundColor(i % 2 == 0 ? context.getResources().getColor(R.color.Tint5) : context.getResources().getColor(R.color.Tint6));
                    ((TextView) ((LinearLayout) ((RelativeLayout) lparent.getChildAt(i)).getChildAt(0)).getChildAt(0)).setTextColor(context.getResources().getColor(R.color.Font_Dark));
                    ((TextView) ((LinearLayout) ((RelativeLayout) lparent.getChildAt(i))
                            .getChildAt(0)).getChildAt(1)).setTextColor(context.getResources()
                            .getColor(R.color.Font_Tint));
                    (((RelativeLayout) lparent.getChildAt(i)).getChildAt(1)).setVisibility(View.GONE);
                }
                String textOption = (String) ((TextView) ((LinearLayout) ((RelativeLayout) v).getChildAt(0)).getChildAt(0)).getText();


                if (mode.equals(ViewMode.DATEFORMAT)) {
                    DateFormatUtils.setSelectedDateFormat(textOption);

                    ((TextView) dateFormatExpContainer.getHeaderLayout().findViewById(R.id
                            .view_dateformat_header_title)).setText(textOption);

                }

                if (mode.equals(ViewMode.FONTSTYLE)) {
                    FontUtils.setSelectedFont(textOption);
                    ((TextView) fontStyleExpContainer.getHeaderLayout().findViewById(R.id
                            .view_fontstyle_header_title)).setText(textOption);
                }


                ((TextView) ((LinearLayout) ((RelativeLayout) v).getChildAt(0)).getChildAt(0)).setTextColor(context.getResources().getColor(R.color.Font_Light));
                ((TextView) ((LinearLayout) ((RelativeLayout) v).getChildAt(0)).getChildAt(1)).setTextColor(context.getResources().getColor(R.color.Font_Light));
                ((RelativeLayout) v).getChildAt(1).setVisibility(View.VISIBLE);
                v.setBackgroundColor(context.getResources().getColor(R.color.Tint7));
            }
        });
    }


    private String getSearchQueryByViewMode(ViewMode mode) {
        return mode.equals(ViewMode.DATEFORMAT) ?
                UserDataHub.getGeneralData().getDateFormat() :
                UserDataHub.getGeneralData().getTypeface();
    }


    private int getTextColor(String input, ViewMode mode) {
        int textColor = input.equals(getSearchQueryByViewMode(mode)) ?
                context.getResources().getColor(R.color.Font_Light) : context.getResources()
                .getColor(R.color.Font_Dark);
        return textColor;
    }

    private int getPreviewTextColor(String input, ViewMode mode) {
        int textColor = input.equals(getSearchQueryByViewMode(mode)) ?
                context.getResources().getColor(R.color.Font_Light) : context.getResources()
                .getColor(R.color.Font_Tint);
        return textColor;
    }

    private int getBackgroundColor(String input, int rowId, ViewMode mode) {
        int backGroundColor = input.equals(getSearchQueryByViewMode(mode)) ?
                context.getResources().getColor(R.color.Tint7) :
                rowId % 2 == 0 ? context.getResources().getColor(R.color.Tint5) : context
                        .getResources().getColor(R.color.Tint6);

        return backGroundColor;
    }

    private ImageView getImageView(String input) {
        ImageView iV_check = new ImageView(context);
        iV_check.setImageBitmap(Bitmap.createScaledBitmap(((BitmapDrawable) context.getResources().getDrawable(R.drawable.ic_tick)).getBitmap(), context.getResources().getDimensionPixelOffset(R.dimen.fab_icon_size), context.getResources().getDimensionPixelOffset(R.dimen.fab_icon_size), false));
        iV_check.setColorFilter(context.getResources().getColor(R.color.White), PorterDuff.Mode.SRC_ATOP);
        iV_check.setVisibility(input.equals(UserDataHub.getGeneralData().getDateFormat()) ? View
                .VISIBLE : View.INVISIBLE);

        return iV_check;
    }

    private RelativeLayout.LayoutParams getLayoutParamsForCheckImageView() {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        return params;
    }


    @Override
    public ContentValues getContentValues() {
        return null;
    }

    @Override
    public ContentValues getContentValues(long mId) {
        return null;
    }


    @Override
    public void initOnClickListeners() {


        dateFormatExpContainer.getHeaderLayout().addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (dateFormatExpContainer.isOpened()) {
                    ((ImageView) v.findViewById(R.id
                            .view_dateformat_header_icon)).setImageDrawable
                            (context.getResources().getDrawable(R.drawable
                                    .ic_expand_up));

                } else {
                    ((ImageView) v.findViewById(R.id
                            .view_dateformat_header_icon)).setImageDrawable(
                            context.getResources().getDrawable(R
                                    .drawable
                                    .ic_expand_down));
                }
            }
        });


        fontStyleExpContainer.getHeaderLayout().addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (fontStyleExpContainer.isOpened()) {
                    ((ImageView) v.findViewById(R.id.view_fontstyle_header_icon))
                            .setImageDrawable(context.getResources().getDrawable(R.drawable
                                    .ic_expand_up));
                } else {
                    ((ImageView) v.findViewById(R.id.view_fontstyle_header_icon))
                            .setImageDrawable(context.getResources().getDrawable(R.drawable
                                    .ic_expand_down));
                }
            }
        });

    }

    @Override
    public void readEntry() {
        this.selectedSwitchToggle = UserDataHub.getGeneralData().getShowStartDialogAsBoolean();
        this.selectedDateFormat = UserDataHub.getDateFormat().toPattern();
    }

    @Override
    public void readEntry(long entryId) {

    }

    @Override
    public void setOnClickListenerForSaveAction(View.OnClickListener listener) {
        this.saveTextView.setOnClickListener(listener);
    }

    @Override
    public void setOnClickListenerForDismissAction(View.OnClickListener listener) {
        this.dismissLinearLayout.setOnClickListener(listener);

    }

    @Override
    public void onError(String message) {

    }


    public void onSave() {

        onDateFormatSelected();
        onFontStyleSelected();
        onSwitchToogle();

    }


    public void onDateFormatSelected() {

        if ("" != selectedDateFormat) {

            ContentValues inputValues = new ContentValues();
            inputValues.put(SQLiteHelper.COLUMN_GENERAL_DATEFORMAT, selectedDateFormat);
            UserDataHub.update(SQLiteHelper.TABLE_GENERAL, inputValues);
        }
    }


    public void onFontStyleSelected() {
        ContentValues inputValues = new ContentValues();
        inputValues.put(SQLiteHelper.COLUMN_GENERAL_TYPEFACE, FontUtils.getSelectedFont().getTypefaceName());
        UserDataHub.update(SQLiteHelper.TABLE_GENERAL, inputValues);

    }


    public void onSwitchToogle() {

        ContentValues inputValues = new ContentValues();
        inputValues.put(SQLiteHelper.COLUMN_GENERAL_SHOWSTARTDIALOG,
                selectedSwitchToggle ? "true" : "false");

        if (inputValues.size() > 0) {
            UserDataHub.update(SQLiteHelper.TABLE_GENERAL, inputValues);
            UserDataHub.refreshUserData();
        }
    }


    private enum ViewMode {
        DATEFORMAT {
            @Override
            String[] getResources() {
                return DateFormatUtils.getDateFormats();
            }
        },
        FONTSTYLE {
            @Override
            String[] getResources() {
                return FontUtils.getAvailableFonts();
            }
        };

        abstract String[] getResources();
    }

}

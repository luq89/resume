package com.example.lukas.resume.datamanagement;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Lukas on 06.05.2015.
 */

public class SQLiteHelper extends SQLiteOpenHelper {

    public static final String TABLE_BASICINFORMATION = "basicinformation";
    public static final String COLUMN_BASICINFORMATION_ID = "_id";
    public static final String COLUMN_BASICINFORMATION_PROFESSION = "profession";
    public static final String COLUMN_BASICINFORMATION_NAME = "name";
    public static final String COLUMN_BASICINFORMATION_BORN = "born";
    public static final String COLUMN_BASICINFORMATION_ADDRESS = "address";
    public static final String COLUMN_BASICINFORMATION_EMAIL = "email";
    public static final String COLUMN_BASICINFORMATION_PHONE = "phone";


    public static final String TABLE_EDUCATION = "education";
    public static final String COLUMN_EDUCATION_ID = "_id";
    public static final String COLUMN_EDUCATION_DATEBEGINN = "begin";
    public static final String COLUMN_EDUCATION_DATEEND = "end";
    public static final String COLUMN_EDUCATION_LOCATION = "location";
    public static final String COLUMN_EDUCATION_INSTITUTION = "subject";
    public static final String COLUMN_EDUCATION_DEGREE = "description";


    public static final String TABLE_EXPERIENCE = "experience";
    public static final String COLUMN_EXPERIENCE_ID = "_id";
    public static final String COLUMN_EXPERIENCE_DATEBEGINN = "begin";
    public static final String COLUMN_EXPERIENCE_DATEEND = "end";
    public static final String COLUMN_EXPERIENCE_LOCATION = "location";
    public static final String COLUMN_EXPERIENCE_SUBJECT = "subject";
    public static final String COLUMN_EXPERIENCE_DESCRIPTION = "description";


    public static final String TABLE_SKILL = "skill";
    public static final String COLUMN_SKILL_ID = "_id";
    public static final String COLUMN_SKILL_CATEGORY = "category";
    public static final String COLUMN_SKILL_SUBCATEGORY = "subcategory";
    public static final String COLUMN_SKILL_RATING = "rating";


    public static final String TABLE_LANGUAGE = "language";
    public static final String COLUMN_LANGUAGE_ID = "_id";
    public static final String COLUMN_LANGUAGE_NAME = "name";
    public static final String COLUMN_LANGUAGE_RATING = "rating";


    public static final String TABLE_CONNECT = "connect";
    public static final String COLUMN_CONNECT_ID = "_id";
    public static final String COLUMN_CONNECT_OPTION = "option";
    public static final String COLUMN_CONNECT_USER = "user";

    public static final String TABLE_GENERAL = "general";
    public static final String COLUMN_GENERAL_ID = "_id";
    public static final String COLUMN_GENERAL_AVATARPATH = "avatarpath";
    public static final String COLUMN_GENERAL_DATEFORMAT = "dateformat";
    public static final String COLUMN_GENERAL_TYPEFACE = "typeface";
    public static final String COLUMN_GENERAL_COLORTHEME = "colortheme";
    public static final String COLUMN_GENERAL_SHOWSTARTDIALOG = "showstartdialog";


    private static final String DATABASE_NAME = "mytable.db";
    private static final int DATABASE_VERSION = 1;

    // Database creation sql statement


    private static final String DATABASE_CREATE_BASICINFORMATION = "create table "
            + TABLE_BASICINFORMATION + "("
            + COLUMN_BASICINFORMATION_ID + " integer primary key, "
            + COLUMN_BASICINFORMATION_PROFESSION + " TEXT, "
            + COLUMN_BASICINFORMATION_NAME + " TEXT, "
            + COLUMN_BASICINFORMATION_BORN + " TEXT, "
            + COLUMN_BASICINFORMATION_ADDRESS + " TEXT, "
            + COLUMN_BASICINFORMATION_EMAIL + " TEXT, "
            + COLUMN_BASICINFORMATION_PHONE + " TEXT "
            + ")";


    private static final String DATABASE_CREATE_EDUCATION = "create table "
            + TABLE_EDUCATION + "("
            + COLUMN_EDUCATION_ID + " integer primary key autoincrement, "
            + COLUMN_EDUCATION_DATEBEGINN + " TEXT, "
            + COLUMN_EDUCATION_DATEEND + " TEXT, "
            + COLUMN_EDUCATION_INSTITUTION + " TEXT, "
            + COLUMN_EDUCATION_LOCATION + " TEXT, "
            + COLUMN_EDUCATION_DEGREE + " TEXT"
            + ")";


    private static final String DATABASE_CREATE_EXPERIENCE = "create table "
            + TABLE_EXPERIENCE + "("
            + COLUMN_EXPERIENCE_ID + " integer primary key autoincrement, "
            + COLUMN_EXPERIENCE_DATEBEGINN + " TEXT, "
            + COLUMN_EXPERIENCE_DATEEND + " TEXT, "
            + COLUMN_EXPERIENCE_SUBJECT + " TEXT, "
            + COLUMN_EXPERIENCE_LOCATION + " TEXT, "
            + COLUMN_EXPERIENCE_DESCRIPTION + " TEXT"
            + ")";

    private static final String DATABASE_CREATE_SKILL = "create table "
            + TABLE_SKILL + "("
            + COLUMN_SKILL_ID + " integer primary key autoincrement, "
            + COLUMN_SKILL_CATEGORY + " TEXT, "
            + COLUMN_SKILL_SUBCATEGORY + " TEXT, "
            + COLUMN_SKILL_RATING + " TEXT "
            + ")";


    private static final String DATABASE_CREATE_LANGUAGE = "create table "
            + TABLE_LANGUAGE + "("
            + COLUMN_LANGUAGE_ID + " integer primary key autoincrement, "
            + COLUMN_LANGUAGE_NAME + " TEXT, "
            + COLUMN_LANGUAGE_RATING + " TEXT "
            + ")";

    private static final String DATABASE_CREATE_CONNECT = "create table "
            + TABLE_CONNECT + "("
            + COLUMN_CONNECT_ID + " integer primary key autoincrement, "
            + COLUMN_CONNECT_OPTION + " TEXT, "
            + COLUMN_CONNECT_USER + " TEXT "
            + ")";

    private static final String DATABASE_CREATE_GENERAL = "create table "
            + TABLE_GENERAL + "("
            + COLUMN_GENERAL_ID + " integer primary key autoincrement, "
            + COLUMN_GENERAL_AVATARPATH + " TEXT, "
            + COLUMN_GENERAL_DATEFORMAT + " TEXT, "
            + COLUMN_GENERAL_TYPEFACE + " TEXT, "
            + COLUMN_GENERAL_COLORTHEME + " TEXT, "
            + COLUMN_GENERAL_SHOWSTARTDIALOG + " TEXT "
            + ")";


    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE_BASICINFORMATION);
        db.execSQL(DATABASE_CREATE_EDUCATION);
        db.execSQL(DATABASE_CREATE_EXPERIENCE);
        db.execSQL(DATABASE_CREATE_SKILL);
        db.execSQL(DATABASE_CREATE_LANGUAGE);
        db.execSQL(DATABASE_CREATE_CONNECT);
        db.execSQL(DATABASE_CREATE_GENERAL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(SQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BASICINFORMATION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EDUCATION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXPERIENCE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SKILL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LANGUAGE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONNECT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GENERAL);
        onCreate(db);
    }
}


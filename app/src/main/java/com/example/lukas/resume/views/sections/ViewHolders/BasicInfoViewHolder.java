package com.example.lukas.resume.views.sections.ViewHolders;


import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.lukas.resume.R;
import com.example.lukas.resume.datamanagement.Form_Entries_BasicInfo;
import com.example.lukas.resume.datamanagement.UserDataHub;
import com.example.lukas.resume.views.sections.ViewHolder;
import com.gc.materialdesign.views.ButtonFlat;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.example.lukas.resume.datamanagement.SQLiteHelper.COLUMN_BASICINFORMATION_ADDRESS;
import static com.example.lukas.resume.datamanagement.SQLiteHelper.COLUMN_BASICINFORMATION_EMAIL;
import static com.example.lukas.resume.datamanagement.SQLiteHelper.COLUMN_BASICINFORMATION_NAME;
import static com.example.lukas.resume.datamanagement.SQLiteHelper.COLUMN_BASICINFORMATION_PHONE;
import static com.example.lukas.resume.datamanagement.SQLiteHelper.COLUMN_BASICINFORMATION_PROFESSION;
import static com.example.lukas.resume.datamanagement.SQLiteHelper.TABLE_BASICINFORMATION;


public class BasicInfoViewHolder implements ViewHolder {

    private List<EditText> editableTexts;

    private EditText nameEditable;
    private EditText professionEditable;
    private EditText addressEditable;
    private EditText emailEditable;
    private EditText phoneEditable;
    private CircleImageView uploadImageCircleView;
    private ButtonFlat importContactButton;
    private DatePickerDialog dateOfBirthDialogPicker;
    private ProgressBar progressIndicator;

    private GlobalViewHolder global;
    private View subView;

    public BasicInfoViewHolder(GlobalViewHolder global) {
        this.global = global;
        this.global.setLayoutResource(R.layout.fragment_formgenerator_section_basicinfo);
        this.subView = global.getSubView();

        initViewHolder();
        setAvatar();
        read();
        setTextWatcherForEditables();

    }

    @Override
    public void initViewHolder() {

        this.nameEditable = (EditText) subView.findViewById(
                R.id.fragment_formgenerator_basicinformation_edittext_name);

        this.professionEditable = (EditText) subView.findViewById(
                R.id.fragment_formgenerator_seondstep_basicinformation_edittext_profession);

        this.addressEditable = (EditText) subView.findViewById(R.id
                .fragment_formgenerator_seondstep_basicinformation_edittext_address);

        this.emailEditable = (EditText) subView.findViewById(R.id
                .fragment_formgenerator_seondstep_basicinformation_edittext_email);

        this.phoneEditable = (EditText) subView.findViewById(R.id
                .fragment_formgenerator_seondstep_basicinformation_edittext_phone);

        this.editableTexts = new ArrayList<EditText>();
        this.editableTexts.add(nameEditable);
        this.editableTexts.add(professionEditable);
        this.editableTexts.add(addressEditable);
        this.editableTexts.add(emailEditable);
        this.editableTexts.add(phoneEditable);


        this.importContactButton = (ButtonFlat) subView.findViewById(
                R.id.fragment_formgenerator_basicinfo_importcontact);


        this.progressIndicator = (ProgressBar) subView.findViewById(
                R.id.fragment_formgenerator_secondstep_progressbar);

        this.uploadImageCircleView = (CircleImageView) subView.findViewById(
                R.id.fragment_formgenerator_secondstep_basicinformation_imageupload_personal);

        subView.findViewById(
                R.id.fragment_formgenerator_secondstep_basicinformation_imageupload_semicircle).getBackground().setLevel(3800);
        subView.findViewById(
                R.id.fragment_formgenerator_secondstep_basicinformation_imageupload_semicircle)
                .getBackground().setAlpha(150);


    }


    public void read() {
        Form_Entries_BasicInfo entry = UserDataHub.getBasicInfoData();
        this.nameEditable.setText(entry.getName());
        this.professionEditable.setText(entry.getProfession());
        this.addressEditable.setText(entry.getAddress());
        this.emailEditable.setText(entry.getEmail());
        this.phoneEditable.setText(entry.getPhone());
    }


    public void setTextForEditables(EditText editText, String text) {
        editText.setText(text);
    }

    public String getTextEditablesAsString(EditText editText) {
        Log.v("TextEditables", editText.getText().toString());

        return editText.getText().toString();
    }


    public EditText getNameEditable() {
        return nameEditable;
    }


    public EditText getProfessionEditable() {
        return professionEditable;
    }


    public EditText getAddressEditable() {
        return addressEditable;
    }


    public EditText getEmailEditable() {
        return emailEditable;
    }


    public EditText getPhoneEditable() {
        return phoneEditable;
    }


    public void setClickListenerForUploadImage(View.OnClickListener listener) {
        this.uploadImageCircleView.setOnClickListener(listener);
    }

    public void setAvatar(Bitmap bitmap) {
        setImageBitmapForUploadImage(bitmap);
    }


    public void setAvatar() {
        try {
            if (UserDataHub.getGeneralData().hasAvatarImage()) {
                Bitmap bitmap = BitmapFactory.decodeFile(UserDataHub.getGeneralData().getAvatarImage());
                if (null != bitmap) {
                    setImageBitmapForUploadImage(bitmap);
                }
            }

        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    private void setImageBitmapForUploadImage(Bitmap bitmap) {
        this.uploadImageCircleView.setImageBitmap(bitmap);
    }

    public void setClickListenerForImportContactButton(View.OnClickListener listener) {
        this.importContactButton.setOnClickListener(listener);
    }

    public ProgressBar getProgressIndicator() {
        return progressIndicator;
    }

    private TextWatcher createTextWatcher() {

        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                try {

                    ContentValues contentValues = new ContentValues();

                    contentValues.put(
                            COLUMN_BASICINFORMATION_NAME,
                            getTextEditablesAsString(nameEditable)
                    );

                    contentValues.put(COLUMN_BASICINFORMATION_PROFESSION,
                            getTextEditablesAsString(professionEditable)
                    );

                    //mContentValues.put(SQLiteHelper.COLUMN_BASICINFORMATION_BORN, basicInfoSectionView.textView_Born.getText().toCustomString());

                    contentValues.put(COLUMN_BASICINFORMATION_ADDRESS,
                            getTextEditablesAsString(addressEditable)
                    );
                    contentValues.put(COLUMN_BASICINFORMATION_EMAIL,
                            getTextEditablesAsString(emailEditable)
                    );

                    contentValues.put(COLUMN_BASICINFORMATION_PHONE,
                            getTextEditablesAsString(phoneEditable)
                    );

                    if (UserDataHub.isTableEmpty(TABLE_BASICINFORMATION)) {

                        UserDataHub.insert(TABLE_BASICINFORMATION, contentValues);

                    } else {

                        UserDataHub.update(TABLE_BASICINFORMATION, contentValues,
                                UserDataHub.getBasicInfoData().getId());
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };


    }


    public void setTextWatcherForEditables() {
        TextWatcher textWatcher = createTextWatcher();
        for (EditText editText : editableTexts) {
            editText.addTextChangedListener(textWatcher);
        }
    }

    public void setVisibilityOfProgressIndicator(int isVisibile) {
        this.professionEditable.setVisibility(isVisibile);
    }

    public void setImportContactButtonPressed(boolean isPressed) {
        this.importContactButton.setPressed(isPressed);
    }
}
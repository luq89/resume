package com.example.lukas.resume;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;

import com.example.lukas.resume.datamanagement.DataSource;
import com.example.lukas.resume.datamanagement.Form_Entries_BasicInfo;
import com.example.lukas.resume.datamanagement.Form_Entries_Connect;
import com.example.lukas.resume.datamanagement.Form_Entries_Education;
import com.example.lukas.resume.datamanagement.Form_Entries_Experience;
import com.example.lukas.resume.datamanagement.Form_Entries_General;
import com.example.lukas.resume.datamanagement.Form_Entries_Language;
import com.example.lukas.resume.datamanagement.Form_Entries_Skill;
import com.example.lukas.resume.helpers.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;


/**
 * Created by Lukas on 07.08.2015.
 */


public class ViewDrawForm {


    Context mContext;

    private class FormDimensions {
        int canvas_Width,
                canvas_Height,
                titleContainer_Width,
                titleContainer_Height,
                basicInformationContainer_Width,
                basicInformationContainer_Height,
                basicInformationContainer_Avatar_Width,
                basicInformationContainer_Avatar_Height,
                experienceContainer_Width,
                experienceContainer_Height,
                connectContainer_Width,
                connectContainer_Height,
                footerContainer_Width,
                footerContainer_Height,
                generalIcon_Width_Small,
                generalIcon_Height_Small,
                generalIcon_Width_Medium,
                generalIcon_Height_Medium,
                generalIcon_Width_Large,
                generalIcon_Height_Large,
                overlayIcon_EditMode_Offset,
                offsetSectionStart_y,
                offset_y,
                offsetBasicInformation_X,
                offsetBasicInformation_Y_Small,
                offsetBasicInformation_Y_Large,
                offsetBasicInformation_Y_Title;


        float canvasFontSize_Small,
                canvasFontSize_Medium,
                canvasFontSize_Large,
                canvasFontSize_Title;


        float centerVert;
        float gPositionY;
        float dottedLineSize_Medium;
        float dottedLineSize_Small;

        float dottedLineSpacing;

    }


    private Typeface mTypeface;

    private SimpleDateFormat mSimpleDateFormatSource;
    private SimpleDateFormat mSimpleDateFormatTarget;


    FormDimensions dim = new FormDimensions();


    private Form_Entries_BasicInfo mFormValue_BasicInformation;
    private List<Form_Entries_Education> mFormValues_Education;
    private List<Form_Entries_Experience> mFormValues_Experience;

    private List<Form_Entries_Skill> mFormValues_Skill;
    private List<Form_Entries_Language> mFormValues_Language;
    private List<Form_Entries_Connect> mFormValues_Connect;
    private Form_Entries_General mFormValue_General;


    String fallBackString = "";


    private int CANVAS_WIDTH = 1444;
    private int CANVAS_HEIGHT = 6000;


    private Utils mUtils;

    private int highlightColor = 0;
    private int fontColor = 0;
    private int backgroundColor = 0;


    public ViewDrawForm(Context context) {
        mContext = context;
        dim.canvas_Width = CANVAS_WIDTH;
        dim.canvas_Height = CANVAS_HEIGHT;

        dim.centerVert = dim.canvas_Width / 2;

        dim.titleContainer_Width = dim.canvas_Width;
        dim.titleContainer_Height = 600;

        dim.basicInformationContainer_Width = dim.canvas_Width;
        dim.basicInformationContainer_Height = 500;


        dim.basicInformationContainer_Avatar_Width = 200;
        dim.basicInformationContainer_Avatar_Height = 200;

        dim.experienceContainer_Width = dim.canvas_Width;
        dim.experienceContainer_Height = 5000;


        dim.connectContainer_Width = dim.canvas_Width;
        dim.connectContainer_Height = 5000;

        dim.footerContainer_Width = dim.canvas_Width;
        dim.footerContainer_Height = 100;


        dim.canvasFontSize_Small = mContext.getResources().getDimension(R.dimen.form_font_size_small);
        dim.canvasFontSize_Medium = mContext.getResources().getDimension(R.dimen.form_font_size_medium);
        dim.canvasFontSize_Large = mContext.getResources().getDimension(R.dimen.form_font_size_large);
        dim.canvasFontSize_Title = mContext.getResources().getDimension(R.dimen.form_font_size_title);


        dim.generalIcon_Width_Small = 28;
        dim.generalIcon_Height_Small = 28;


        dim.generalIcon_Width_Medium = 48;
        dim.generalIcon_Height_Medium = 48;

        dim.generalIcon_Width_Large = 96;
        dim.generalIcon_Height_Large = 96;

        dim.offset_y = 75;
        dim.offsetBasicInformation_X = 96;
        dim.offsetBasicInformation_Y_Small = 8;
        dim.offsetBasicInformation_Y_Large = 48;
        dim.offsetBasicInformation_Y_Title = 128;
        dim.offsetSectionStart_y = 150;
        dim.overlayIcon_EditMode_Offset = 30;

        initDataFields();
        fallBackString = mContext.getResources().getString(R.string.general_emptyinput);
        mUtils = Utils.newInstance(mContext);

        dim.dottedLineSize_Medium = 3;
        dim.dottedLineSize_Small = 2;
        dim.dottedLineSpacing = 16;

    }


    public void setFontColor(int fontColor) {
        this.fontColor = fontColor;
    }

    public int getFontColor() {
        return this.fontColor;
    }

    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public int getBackgroundColor() {
        return this.backgroundColor;
    }


    public void setHighlightColor(int highlightColor) {
        this.highlightColor = highlightColor;
    }

    public int getHighlightColor() {
        return this.highlightColor;
    }


    /*
    public void setColors(ArrayList<Integer> colors) {
        this.colors = colors;
    }
    */


    public void initDataFields() {

        DataSource mDataSource = new DataSource(mContext);
        mDataSource.open();

        mFormValue_BasicInformation = mDataSource.getBasicInformationEntry();
        mFormValues_Experience = mDataSource.getAllExperienceEntries();
        mFormValues_Education = mDataSource.getAllEducationEntries();
        mFormValues_Skill = mDataSource.getAllSkillEntries();
        mFormValues_Language = mDataSource.getAllLanguageEntries();

        mFormValues_Connect = mDataSource.getAllConnectEntries();
        mFormValue_General = mDataSource.getGeneralEntry();

        mSimpleDateFormatSource = new SimpleDateFormat(mContext.getResources().getString(R.string.default_dateformat));

        if (null != mFormValue_General.getDateFormat()) {
            mSimpleDateFormatTarget = new SimpleDateFormat(mFormValue_General.getDateFormat());
        } else {
            mSimpleDateFormatTarget = new SimpleDateFormat(mContext.getResources().getString(R.string.default_dateformat));
        }


        if (mFormValue_General.getTypeface().equals("") || mFormValue_General.getTypeface().equals("DEFAULT")) {
            mTypeface = Typeface.createFromAsset(mContext.getResources().getAssets(), "fonts/JosefinSansRegular.ttf");
        } else if (mFormValue_General.getTypeface().equals("JOSEFINSANS")) {
            mTypeface = Typeface.createFromAsset(mContext.getResources().getAssets(), "fonts/JosefinSansRegular.ttf");
        } else if (mFormValue_General.getTypeface().equals("WHITNEYBOOK")) {
            mTypeface = Typeface.createFromAsset(mContext.getResources().getAssets(), "fonts/WhitneyBook.ttf");
        } else if (mFormValue_General.getTypeface().equals("CORBEL")) {
            mTypeface = Typeface.createFromAsset(mContext.getResources().getAssets(), "fonts/Corbel.ttf");
        }

        if (!mFormValue_General.getColorTheme().equals("")) {
            setHighlightColor(mContext.getResources().getColor(R.color.Tint1));
            //setHighlightColor(Integer.parseInt(mFormValue_General.getColorTheme()));
            //setHighlightColor(Integer.parseInt(mFormValue_General.getColorTheme()));
        } else {
            setHighlightColor(mContext.getResources().getColor(R.color.Tint1));
        }

        setFontColor(mContext.getResources().getColor(R.color.Font_Dark));
        setBackgroundColor(mContext.getResources().getColor(R.color.White));


    }




    /*
    public int getSection(int pos) {
        if (pos < mSectionPos.length) {
            return mSectionPos[pos];
        } else return 0;
    }
    */


    protected Bitmap drawBitmap() {

        // Init Bitmap
        Bitmap result = Bitmap.createBitmap(dim.canvas_Width, dim.canvas_Height, Bitmap.Config.ARGB_8888);
        Canvas mCanvas = new Canvas(result);
        mCanvas.setDensity(mContext.getResources().getDisplayMetrics().densityDpi);

        dim.gPositionY = 0;


        Paint paint = new Paint();
        paint.setFlags(Paint.FILTER_BITMAP_FLAG | Paint.ANTI_ALIAS_FLAG | Paint.EMBEDDED_BITMAP_TEXT_FLAG | Paint.DITHER_FLAG | Paint.SUBPIXEL_TEXT_FLAG);

        Paint iconPaint = new Paint();
        iconPaint.setFlags(Paint.FILTER_BITMAP_FLAG | Paint.ANTI_ALIAS_FLAG);

        iconPaint.setColorFilter(new PorterDuffColorFilter(highlightColor, PorterDuff.Mode.SRC_ATOP));


        //Draw Title

        drawTitleSection(mCanvas);

        //Draw BasicInformation

        drawBasicInformationSection(mCanvas);

        //Draw Experience & Skill
        drawEducationExperienceSection(mCanvas);

        drawSkillSection(mCanvas);

        //Draw ConnectSection
        //drawConnectSection(mCanvas);
        //drawMeSomething(mCanvas);

        //Draw Footer
        //drawFooterSection(mCanvas);

        //Draw HelperLines

        drawOuttaBorder(mCanvas, 25);


        drawHelperLines(mCanvas);


        result.reconfigure(dim.canvas_Width, (int) dim.gPositionY, Bitmap.Config.ARGB_8888);
        //result.reconfigure(dim.canvas_Width, 4000, Bitmap.Config.ARGB_8888);


        return result;

    }


    private void drawHelperLines(Canvas mCanvas) {
        Paint pForm = getFormPaint(Color.RED);
        pForm.setStrokeWidth(2);

        //mCanvas.restore();
        mCanvas.drawLine(dim.canvas_Width * 0.33f, 0, dim.canvas_Width * 0.33f, 1000, pForm);
        mCanvas.drawLine(dim.canvas_Width * 0.66f, 0, dim.canvas_Width * 0.66f, 1000, pForm);
        mCanvas.drawLine(dim.canvas_Width * 0.90f, 0, dim.canvas_Width * 0.90f, 1000, pForm);


        mCanvas.drawLine(0, dim.gPositionY, dim.canvas_Width, dim.gPositionY, pForm);

    }


    private Paint getFormPaint(int color) {
        Paint p = new Paint();
        p.setFlags(Paint.FILTER_BITMAP_FLAG | Paint.ANTI_ALIAS_FLAG | Paint.EMBEDDED_BITMAP_TEXT_FLAG | Paint.DITHER_FLAG | Paint.SUBPIXEL_TEXT_FLAG);
        p.setColor(color);
        return p;
    }

    private TextPaint getFontPaint(int color, float textSize, TextPaint.Align align, Typeface typeface) {
        TextPaint tP = new TextPaint();
        tP.setFlags(Paint.FILTER_BITMAP_FLAG | Paint.ANTI_ALIAS_FLAG | Paint.EMBEDDED_BITMAP_TEXT_FLAG | Paint.DITHER_FLAG | Paint.SUBPIXEL_TEXT_FLAG);
        tP.setTextSize(textSize);
        tP.setTextAlign(align);
        tP.setColor(color);
        tP.setTypeface(typeface);
        return tP;
    }

    private Paint getIconPaint(int color) {
        Paint pI = new Paint();
        pI.setFlags(Paint.FILTER_BITMAP_FLAG | Paint.ANTI_ALIAS_FLAG);
        pI.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP));
        return pI;
    }

    private void translate(Canvas mCanvas, float x, float y, boolean translateCanvas, boolean increaseGlobalY) {
        if (translateCanvas) {
            mCanvas.translate(x, y);
        }
        if (increaseGlobalY) {
            dim.gPositionY += y;
        }


    }


    private void drawTitleSection(Canvas mCanvas) {


        //mCanvas.getContentValues();

        //Draw Title
        Paint pForm = getFormPaint(getBackgroundColor());
        mCanvas.drawRect(0, 0, dim.titleContainer_Width, dim.titleContainer_Height, pForm);
        String[] arrName = new String[]{"", ""};

        try {
            arrName = mFormValue_BasicInformation.getName().split(" ");
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        TextPaint pFont_l = getFontPaint(Color.BLACK, dim.canvasFontSize_Title, TextPaint.Align.RIGHT, mTypeface);
        mCanvas.drawText(arrName.length < 2 ? fallBackString : arrName[0],
                dim.canvas_Width * 0.33f, dim.titleContainer_Height / 2 + dim.canvasFontSize_Large / 3,
                pFont_l);


        TextPaint pFont_r = getFontPaint(Color.BLACK, dim.canvasFontSize_Title, TextPaint.Align.LEFT, mTypeface);
        mCanvas.drawText(arrName.length < 2 ? fallBackString : arrName[1],
                dim.canvas_Width * 0.66f, dim.titleContainer_Height / 2 + dim.canvasFontSize_Large / 3, pFont_r);


        Paint pPoly = getFormPaint(getFontColor());
        pPoly.setStyle(Paint.Style.STROKE);
        pPoly.setStrokeWidth(5);
        drawPolygon(mCanvas, dim.canvas_Width * 0.5f, dim.titleContainer_Height / 2, 150, 6, 30, false, pPoly);

        TextPaint pFont_c = getFontPaint(Color.BLACK, dim.canvasFontSize_Title, TextPaint.Align.CENTER, mTypeface);

        mCanvas.drawText("CV",
                dim.canvas_Width * 0.5f, dim.titleContainer_Height / 2 + dim.canvasFontSize_Large / 3, pFont_c);


        //mCanvas.restore();

    }


    private void drawBasicInformationSection(Canvas mCanvas) {

        mCanvas.save();

        translate(mCanvas, 0, dim.titleContainer_Height, true, true);

        //mCanvas.translate(0, dim.titleContainer_Height);

        //Init Paint Objects
        Paint pForm = getFormPaint(getBackgroundColor());
        Paint pIcon = getIconPaint(getFontColor());

        //dim.basicInformationContainer_Height = (int ) getBasicInformationContainerHeight(pFont, 300);

        mCanvas.drawRect(0, 0, dim.basicInformationContainer_Width, dim.basicInformationContainer_Height, pForm);

        //pForm.setColor(colors.get(3));
        //pForm.setStrokeWidth(2);

        Bitmap bm = Bitmap.createScaledBitmap(((BitmapDrawable) mContext.getResources().getDrawable(R.drawable.form_bird)).getBitmap(), dim.generalIcon_Width_Large, dim.generalIcon_Height_Large, false);
        mCanvas.drawBitmap(bm, dim.centerVert - dim.generalIcon_Width_Large / 2, 0, pIcon);


        translate(mCanvas, 0, dim.generalIcon_Height_Large + dim.offsetBasicInformation_Y_Large, true, true);
        //mCanvas.translate(0, dim.generalIcon_Height_Large + dim.offsetBasicInformation_Y_Large);

        Paint pDottedLine = getFormPaint(Color.BLACK);
        drawVertDottedLine(mCanvas,
                dim.centerVert,
                0,
                dim.basicInformationContainer_Avatar_Height,
                dim.dottedLineSpacing, dim.dottedLineSize_Medium, pDottedLine);

        drawAvatarImage(mCanvas,
                dim.centerVert + dim.offsetBasicInformation_X,
                0,
                dim.basicInformationContainer_Avatar_Width,
                dim.basicInformationContainer_Avatar_Height, 0, false);


        translate(mCanvas, 0, dim.canvasFontSize_Large / 2, true, true);

        //mCanvas.translate(0, dim.canvasFontSize_Large / 2);

        TextPaint pFont_l = getFontPaint(getFontColor(), dim.canvasFontSize_Large, TextPaint.Align.RIGHT, mTypeface);
        mCanvas.drawText(
                mContext.getResources().getString(R.string.fragment_form_hello_title).toUpperCase(),
                dim.centerVert - dim.offsetBasicInformation_X,
                0 + 4,
                pFont_l);

        pFont_l.setTextSize(dim.canvasFontSize_Small);

        try {

            mCanvas.save();

            /*
            mCanvas.translate(dim.centerVert - dim.offsetBasicInformation_X,
                    dim.offsetBasicInformation_Y_Large);
            */

            translate(mCanvas, dim.centerVert - dim.offsetBasicInformation_X,
                    dim.offsetBasicInformation_Y_Large, true, true);


            StaticLayout mTextLayout = new StaticLayout("I'm Lukas",
                    pFont_l, 700, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);

            mTextLayout.draw(mCanvas);


            mCanvas.translate(0, mTextLayout.getHeight() +
                    dim.offsetBasicInformation_Y_Small);

            mTextLayout = new StaticLayout(mFormValue_BasicInformation.getProfession().length() == 0 ? fallBackString : mFormValue_BasicInformation.getProfession(),
                    pFont_l, 700, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);

            mTextLayout.draw(mCanvas);


            /*
            mCanvas.translate(0, mTextLayout.getHeight() +
                    dim.offsetBasicInformation_Y_Large);
                    */


            translate(mCanvas, 0, mTextLayout.getHeight() +
                    dim.offsetBasicInformation_Y_Large, true, true);

            mTextLayout = new StaticLayout(mFormValue_BasicInformation.getAddress().length() == 0 ? fallBackString : mFormValue_BasicInformation.getAddress(),
                    pFont_l, 700, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);

            mTextLayout.draw(mCanvas);

            mCanvas.restore();


        } catch (IndexOutOfBoundsException e) {

        }


        TextPaint pFont_r = getFontPaint(getFontColor(), dim.canvasFontSize_Large, TextPaint.Align.LEFT, mTypeface);


        mCanvas.drawText(
                mContext.getResources().getString(R.string.fragment_form_contact_title).toUpperCase(),
                dim.centerVert + dim.offsetBasicInformation_X + dim.basicInformationContainer_Avatar_Width + dim.offsetBasicInformation_X / 2,
                0 + 4,
                pFont_r);

        pFont_r.setTextSize(dim.canvasFontSize_Small);

        try {

            mCanvas.save();
            mCanvas.translate(dim.centerVert + dim.offsetBasicInformation_X + dim.basicInformationContainer_Avatar_Width + dim.offsetBasicInformation_X / 2,
                    dim.offsetBasicInformation_Y_Large);

            StaticLayout mTextLayout = new StaticLayout(mFormValue_BasicInformation.getEmail().length() == 0 ? fallBackString : mFormValue_BasicInformation.getEmail(),
                    pFont_r, 700, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);

            mTextLayout.draw(mCanvas);


            mCanvas.translate(0, mTextLayout.getHeight() +
                    dim.offsetBasicInformation_Y_Small);

            mTextLayout = new StaticLayout(mFormValue_BasicInformation.getPhone().length() == 0 ? fallBackString : mFormValue_BasicInformation.getPhone(),
                    pFont_r, 700, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);


            mTextLayout.draw(mCanvas);

            mCanvas.translate(0,
                    dim.offsetBasicInformation_Y_Large);

            drawConnectSection(mCanvas, 0, 0);
            mCanvas.restore();
        } catch (IndexOutOfBoundsException e) {

        }


        translate(mCanvas, 0, dim.offset_y, false, true);

        mCanvas.restore();
    }


    private void drawEducationExperienceSection(Canvas mCanvas) {

        mCanvas.save();

        translate(mCanvas, 0, dim.gPositionY + dim.offset_y, true, false);

        //mCanvas.translate(0, dim.titleContainer_Height + dim.basicInformationContainer_Height);

        Paint pLine = getFormPaint(Color.RED);
        pLine.setStrokeWidth(2);

        //mCanvas.restore();
        mCanvas.drawLine(0, 0, dim.canvas_Width, 0, pLine);


        Paint pForm = getFormPaint(getBackgroundColor());
        Paint pIcon = getIconPaint(getFontColor());

        //mCanvas.drawRect(0, 0, dim.experienceContainer_Width, dim.experienceContainer_Height, pForm);
        mCanvas.drawRect(0, 0, dim.experienceContainer_Width, dim.experienceContainer_Height, pForm);


        Bitmap bm = Bitmap.createScaledBitmap(((BitmapDrawable) mContext.getResources().getDrawable(R.drawable.form_sheet)).getBitmap(), dim.generalIcon_Width_Large, dim.generalIcon_Height_Large, false);
        mCanvas.drawBitmap(bm, dim.centerVert - dim.generalIcon_Width_Large / 2, 0, pIcon);


        translate(mCanvas, 0, dim.generalIcon_Height_Large + dim.offsetBasicInformation_Y_Large, true, true);


        mCanvas.save();
        mCanvas.translate(0, dim.canvasFontSize_Large / 2);


        TextPaint pFont_l = getFontPaint(Color.BLACK, dim.canvasFontSize_Large, TextPaint.Align.RIGHT, mTypeface);
        mCanvas.drawText(
                mContext.getResources().getString(R.string.fragment_form_education_title).toUpperCase(),
                dim.centerVert - dim.offsetBasicInformation_X,
                0 + 4,
                pFont_l);


        mCanvas.translate(dim.centerVert - dim.offsetBasicInformation_X,
                0);
        pFont_l.setTextSize(dim.canvasFontSize_Small);

        int height_l = 0;
        for (Form_Entries_Education entry : mFormValues_Education) {

            height_l += dim.offsetBasicInformation_Y_Large + dim.canvasFontSize_Medium;
            mCanvas.translate(0,
                    dim.offsetBasicInformation_Y_Large + dim.canvasFontSize_Medium);
            java.util.Date dateBeginn = null;
            try {
                dateBeginn = mSimpleDateFormatSource.parse(entry.getDateBegin());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            StaticLayout mTextLayout = new StaticLayout(dateBeginn == null ? entry.getDateBegin() : mSimpleDateFormatTarget.format(dateBeginn),
                    pFont_l, dim.canvas_Width * 40 / 100, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            mTextLayout.draw(mCanvas);

            height_l += mTextLayout.getHeight() + dim.offsetBasicInformation_Y_Small;
            mCanvas.translate(0, mTextLayout.getHeight() + dim.offsetBasicInformation_Y_Small);
            mTextLayout = new StaticLayout(entry.getInstitution() + " - " + entry.getDegree(),
                    pFont_l, dim.canvas_Width * 40 / 100, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            mTextLayout.draw(mCanvas);

            height_l += mTextLayout.getHeight() + dim.offsetBasicInformation_Y_Small;
            mCanvas.translate(0, mTextLayout.getHeight() + dim.offsetBasicInformation_Y_Small);
            mTextLayout = new StaticLayout(entry.getLocation(),
                    pFont_l, dim.canvas_Width * 40 / 100, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            mTextLayout.draw(mCanvas);
        }

        mCanvas.restore();
        mCanvas.save();


        mCanvas.translate(dim.centerVert + dim.offsetBasicInformation_X,
                dim.canvasFontSize_Large / 2);


        TextPaint pFont_r = getFontPaint(Color.BLACK, dim.canvasFontSize_Large, TextPaint.Align.LEFT, mTypeface);


        mCanvas.drawText(
                mContext.getResources().getString(R.string.fragment_form_experience_title).toUpperCase(),
                0,
                0 + 4,
                pFont_r);

        pFont_r.setTextSize(dim.canvasFontSize_Small);


        int height_r = 0;
        for (Form_Entries_Experience entry : mFormValues_Experience) {


            height_r += dim.offsetBasicInformation_Y_Large + dim.canvasFontSize_Medium;
            mCanvas.translate(0,
                    dim.offsetBasicInformation_Y_Large + dim.canvasFontSize_Medium);
            java.util.Date dateBeginn = null;
            try {
                dateBeginn = mSimpleDateFormatSource.parse(entry.getDateBegin());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            StaticLayout mTextLayout = new StaticLayout(dateBeginn == null ? entry.getDateBegin() : mSimpleDateFormatTarget.format(dateBeginn),
                    pFont_r, 700, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            mTextLayout.draw(mCanvas);

            height_r += mTextLayout.getHeight() + dim.offsetBasicInformation_Y_Small;
            mCanvas.translate(0, mTextLayout.getHeight() + dim.offsetBasicInformation_Y_Small);
            mTextLayout = new StaticLayout(entry.getProfession() + " - " + entry.getDescription(),
                    pFont_r, dim.canvas_Width * 40 / 100, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            mTextLayout.draw(mCanvas);

            height_r += mTextLayout.getHeight() + dim.offsetBasicInformation_Y_Small;
            mCanvas.translate(0, mTextLayout.getHeight() + dim.offsetBasicInformation_Y_Small);
            mTextLayout = new StaticLayout(entry.getBorn(),
                    pFont_r, dim.canvas_Width * 40 / 100, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            mTextLayout.draw(mCanvas);

        }


        mCanvas.translate(0, dim.generalIcon_Height_Large + dim.offsetBasicInformation_Y_Large);
        height_r += dim.generalIcon_Height_Large + dim.offsetBasicInformation_Y_Large;
        //translate(mCanvas, 0, dim.generalIcon_Height_Large + dim.offsetBasicInformation_Y_Large);

        pFont_r.setTextSize(dim.canvasFontSize_Large);
        mCanvas.drawText(
                mContext.getResources().getString(R.string.fragment_form_language_title).toUpperCase(),
                0,
                0 + 4,
                pFont_r);

        pFont_r.setTextSize(dim.canvasFontSize_Small);


        for (Form_Entries_Language entry : mFormValues_Language) {


            String[] rating = {"+", "++", "+++", "++++", "+++++"};


            height_r += dim.offsetBasicInformation_Y_Small + dim.canvasFontSize_Medium;
            mCanvas.translate(0,
                    dim.offsetBasicInformation_Y_Small + dim.canvasFontSize_Medium);


            StaticLayout mTextLayout = new StaticLayout(entry.getLanguage(),
                    pFont_r, dim.canvas_Width * 33 / 100, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            mTextLayout.draw(mCanvas);

            mCanvas.save();
            //mCanvas.translate(pFont_r.measureText(entry.getLanguage()), 0);


            pFont_r.setFakeBoldText(true);
            mTextLayout = new StaticLayout(String.valueOf(rating[((Double) entry.getRating()).intValue() - 1]),
                    pFont_r, dim.canvas_Width * 33 / 100, Layout.Alignment.ALIGN_OPPOSITE, 1.0f, 0.0f, false);
            mTextLayout.draw(mCanvas);
            pFont_r.setFakeBoldText(false);


            Paint pDottedLine = getFormPaint(getHighlightColor());
            drawHoriDottedLine(mCanvas,
                    pFont_r.measureText(entry.getLanguage()) + 16,
                    dim.canvasFontSize_Small - 10,
                    mTextLayout.getWidth() - (pFont_r.measureText(String.valueOf(rating[((Double) entry.getRating()).intValue() - 1]) + 16)),
                    dim.dottedLineSpacing, dim.dottedLineSize_Small, pDottedLine);

            mCanvas.restore();

            /*

            height_r +=  mTextLayout.getHeight() + dim.offsetBasicInformation_Y_Small;
            mCanvas.translate(0, mTextLayout.getHeight() + dim.offsetBasicInformation_Y_Small);
            mTextLayout = new StaticLayout(entry.getProfession() + " - " + entry.getDescription(),
                    pFont_r, dim.canvas_Width * 40 / 100, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            mTextLayout.draw(mCanvas);

            height_r +=  mTextLayout.getHeight() + dim.offsetBasicInformation_Y_Small;
            mCanvas.translate(0, mTextLayout.getHeight() + dim.offsetBasicInformation_Y_Small);
            mTextLayout = new StaticLayout(entry.getBorn(),
                    pFont_r, dim.canvas_Width * 40 / 100, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            mTextLayout.draw(mCanvas);
            */
        }

        mCanvas.restore();


        Paint pDottedLine = getFormPaint(getFontColor());
        drawVertDottedLine(mCanvas,
                dim.centerVert,
                0,
                (height_l > height_r) ? (height_l + dim.offsetBasicInformation_Y_Large) : (height_r + dim.offsetBasicInformation_Y_Large),
                dim.dottedLineSpacing, dim.dottedLineSize_Medium, pDottedLine);


        translate(mCanvas, 0, (height_l > height_r) ? (height_l + dim.offsetBasicInformation_Y_Large + dim.offset_y) : (height_r + dim.offsetBasicInformation_Y_Large + dim.offset_y), false, true);

        mCanvas.restore();


    }


    private void drawSkillSection(Canvas mCanvas) {

        mCanvas.save();

        translate(mCanvas, 0, dim.gPositionY + dim.offset_y, true, false);

        //mCanvas.translate(0, dim.gPositionY + dim.offsetBasicInformation_Y_Large );

        Paint pLine = getFormPaint(Color.RED);
        pLine.setStrokeWidth(2);

        //mCanvas.restore();
        mCanvas.drawLine(0, 0, dim.canvas_Width, 0, pLine);


        Paint pForm = getFormPaint(getBackgroundColor());
        Paint pIcon = getIconPaint(getFontColor());

        mCanvas.drawRect(0, 0, dim.experienceContainer_Width, dim.experienceContainer_Height, pForm);


        Bitmap bm = Bitmap.createScaledBitmap(((BitmapDrawable) mContext.getResources().getDrawable(R.drawable.ic_general_profession)).getBitmap(), dim.generalIcon_Width_Large, dim.generalIcon_Height_Large, false);
        mCanvas.drawBitmap(bm, dim.centerVert - dim.generalIcon_Width_Large / 2, 0, pIcon);


        translate(mCanvas, 0, dim.generalIcon_Height_Large + dim.offsetBasicInformation_Y_Large, true, true);

        mCanvas.save();
        mCanvas.translate(0, dim.canvasFontSize_Large / 2);


        TextPaint pFont_l = getFontPaint(getFontColor(), dim.canvasFontSize_Large, TextPaint.Align.RIGHT, mTypeface);
        mCanvas.drawText(
                mContext.getResources().getString(R.string.fragment_form_skills_title).toUpperCase(),
                dim.centerVert - dim.offsetBasicInformation_X,
                0 + 4,
                pFont_l);


        translate(mCanvas, dim.centerVert - dim.offsetBasicInformation_X,
                dim.offsetBasicInformation_Y_Large, true, true);

        pFont_l.setTextSize(dim.canvasFontSize_Small);

        mCanvas.translate(0,
                dim.offsetBasicInformation_Y_Large + dim.canvasFontSize_Medium);

        int height_l = 0;
        for (Form_Entries_Skill entry : mFormValues_Skill) {

            //mCanvas.drawBitmap(mUtils.optionToBitmap(entry.getSubCategory(), dim.generalIcon_Width_Medium, dim.generalIcon_Height_Medium), 0, -dim.generalIcon_Height_Medium, pIcon);

            pForm.setColor(getHighlightColor());
            mCanvas.drawRect(0, -dim.generalIcon_Height_Small, (int) (entry.getRating()) * -50, 0, pForm);

            pFont_l.setTextSize(dim.canvasFontSize_Small);
            //
            pFont_l.setColor(getFontColor());
            mCanvas.drawText(entry.getSubCategory(),
                    (float) entry.getRating() * -50.0f - dim.canvasFontSize_Small, -dim.generalIcon_Height_Small / 2 + dim.canvasFontSize_Small / 3, pFont_l);
            //titleSubcategory = entry.getLanguage();
            mCanvas.translate(0, dim.offsetBasicInformation_Y_Small + dim.canvasFontSize_Small);
            //dim.gPositionY += dim.offset_y;
            height_l += dim.offset_y;
        }

        mCanvas.restore();
        mCanvas.save();


        mCanvas.translate(dim.centerVert + dim.offsetBasicInformation_X,
                dim.canvasFontSize_Large / 2);

        TextPaint pFont_r = getFontPaint(getFontColor(), dim.canvasFontSize_Large, TextPaint.Align.LEFT, mTypeface);

        mCanvas.drawText(
                mContext.getResources().getString(R.string.fragment_form_skills_title).toUpperCase(),
                0,
                0 + 4,
                pFont_r);

        pFont_r.setTextSize(dim.canvasFontSize_Small);


        int height_r = 0;

        for (Form_Entries_Language entry : mFormValues_Language) {


            height_r += dim.offsetBasicInformation_Y_Large + dim.canvasFontSize_Medium;
            mCanvas.translate(0,
                    dim.offsetBasicInformation_Y_Large + dim.canvasFontSize_Medium);

            StaticLayout mTextLayout = new StaticLayout(entry.getLanguage(),
                    pFont_r, dim.canvas_Width * 33 / 100, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            mTextLayout.draw(mCanvas);


            mCanvas.save();
            mTextLayout = new StaticLayout(String.valueOf(entry.getRating()),
                    pFont_r, dim.canvas_Width * 33 / 100, Layout.Alignment.ALIGN_OPPOSITE, 1.0f, 0.0f, false);
            mTextLayout.draw(mCanvas);


            mCanvas.restore();


        }

        mCanvas.restore();

        Paint pDottedLine = getFormPaint(getFontColor());
        drawVertDottedLine(mCanvas,
                dim.centerVert,
                0,
                (height_l > height_r) ? (height_l + dim.offsetBasicInformation_Y_Large) : (height_r + dim.offsetBasicInformation_Y_Large),
                dim.dottedLineSpacing, dim.dottedLineSize_Medium, pDottedLine);


        translate(mCanvas, 0, (height_l > height_r) ? (height_l + dim.offsetBasicInformation_Y_Large + dim.offset_y * 2) : (height_r + dim.offsetBasicInformation_Y_Large + dim.offset_y * 2), false, true);
        mCanvas.restore();

    }


    public void drawOuttaBorder(Canvas mCanvas, int width) {

        Paint pBorder = getFormPaint(getHighlightColor());
        //top
        mCanvas.drawRect(0, 0, dim.canvas_Width, width, pBorder);

        //left
        mCanvas.drawRect(0, 0, width, dim.gPositionY, pBorder);

        //right
        mCanvas.drawRect(dim.canvas_Width - width, 0, dim.canvas_Width, dim.gPositionY, pBorder);

        //bottom
        mCanvas.drawRect(0, dim.gPositionY - width, dim.canvas_Width, dim.gPositionY, pBorder);


    }

    public void drawConnectSection(Canvas mCanvas, float startX, float startY) {


        mCanvas.save();
        mCanvas.translate(startX, startY);


        Paint pIcon = getIconPaint(getHighlightColor());
        pIcon.setColorFilter(null);


        int i = 0;

        for (Form_Entries_Connect entry : mFormValues_Connect) {
            mCanvas.drawBitmap(Utils.optionToBitmap(entry.getOption(), dim.generalIcon_Width_Medium, dim.generalIcon_Height_Medium), dim.generalIcon_Height_Medium * i, 0, pIcon);
            i++;
        }

        mCanvas.restore();


    }


    private void drawFooterSection(Canvas mCanvas) {

        Paint pForm = new Paint();
        pForm.setFlags(Paint.FILTER_BITMAP_FLAG | Paint.ANTI_ALIAS_FLAG | Paint.EMBEDDED_BITMAP_TEXT_FLAG | Paint.DITHER_FLAG | Paint.SUBPIXEL_TEXT_FLAG);
        pForm.setColor(highlightColor);
        mCanvas.translate(-dim.footerContainer_Width * 0.5f, dim.offsetSectionStart_y);
        //dim.gPositionY += dim.offset_y + dim.footerContainer_Height;


        mCanvas.drawRect(0, 0, dim.footerContainer_Width, dim.footerContainer_Height, pForm);
    }


    private float getBasicInformationContainerHeight(TextPaint p, int yMax) {

        float res = dim.offsetSectionStart_y * 1.5f;
        //float res = 0;

        try {
            String[] arr = {mFormValue_BasicInformation.getName(),
                    mFormValue_BasicInformation.getProfession(),
                    mFormValue_BasicInformation.getBorn(),
                    mFormValue_BasicInformation.getAddress(),
                    mFormValue_BasicInformation.getEmail(),
                    mFormValue_BasicInformation.getPhone()};

            for (int i = 0; i < arr.length; i++) {

                StaticLayout st = new StaticLayout(arr[i],
                        p, yMax, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);

                res += st.getHeight() + dim.offsetBasicInformation_X * 1.2;

            }

            Log.v("RESULT", String.valueOf(res));

        } catch (IndexOutOfBoundsException e) {

        }


        return res;
    }


    private float getMaxTextSize(TextPaint p, int yMax) {


        float res = p.measureText(fallBackString);

        try {
            String[] arr = {mFormValue_BasicInformation.getName(),
                    mFormValue_BasicInformation.getProfession(),
                    mFormValue_BasicInformation.getBorn(),
                    mFormValue_BasicInformation.getAddress(),
                    mFormValue_BasicInformation.getEmail(),
                    mFormValue_BasicInformation.getPhone()};

            for (int i = 0; i < arr.length; i++) {

                StaticLayout st = new StaticLayout(arr[i],
                        p, yMax, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);

                if (res < p.measureText(arr[i])) {
                    res = st.getWidth();
                }
            }

            Log.v("RESULT", String.valueOf(res));

        } catch (IndexOutOfBoundsException e) {

        }


        return res;
    }

    private void drawPolygon(Canvas mCanvas, float x, float y, float radius, float sides, float startAngle, boolean anticlockwise, Paint paint) {

        if (sides < 3) {
            return;
        }

        float a = ((float) Math.PI * 2) / sides * (anticlockwise ? -1 : 1);
        mCanvas.save();
        mCanvas.translate(x, y);
        mCanvas.rotate(startAngle);
        Path path = new Path();
        path.moveTo(radius, 0);
        for (int i = 1; i < sides; i++) {
            path.lineTo(radius * (float) Math.cos(a * i), radius * (float) Math.sin(a * i));
        }
        path.close();
        mCanvas.drawPath(path, paint);
        mCanvas.restore();
    }

    private void drawHoriDottedLine(Canvas mCanvas, float startX, float startY, float endX, float spacing, float radius, Paint paint) {

        mCanvas.save();
        mCanvas.translate(startX + radius, startY);
        int temp = 0;
        while (temp < (endX - startX)) {
            mCanvas.drawCircle(temp, 0, radius, paint);
            temp += spacing;
        }
        mCanvas.restore();

    }


    private void drawVertDottedLine(Canvas mCanvas, float startX, float startY, float endY, float spacing, float radius, Paint paint) {

        mCanvas.save();
        mCanvas.translate(startX, startY + radius);
        int temp = 0;
        while (temp < (endY - startY)) {
            mCanvas.drawCircle(0, temp, radius, paint);
            temp += spacing;
        }
        mCanvas.restore();

    }

    public void drawAvatarImage(Canvas mCanvas, float startX, float startY, int height, int width, int strokeWidth, boolean drawStroke) {
        // Draw Avatar Image
        if (null != mFormValue_General.getAvatarImage()) {

            mCanvas.save();
            //mCanvas.translate(dim.basicInformationContainer_Width * 0.25f - dim.basicInformationContainer_Avatar_Width / 2, dim.titleContainer_Height + dim.offsetSectionStart_y -  dim.canvasFontSize_Small);
            mCanvas.translate(startX, startY);

            Bitmap bmp = BitmapFactory.decodeFile(mFormValue_General.getAvatarImage());

            if (null == bmp) {
                bmp = ((BitmapDrawable) mContext.getResources().getDrawable(R.drawable.img_avatar_placeholder)).getBitmap();
            }

            Paint avatarPaint = new Paint();
            avatarPaint.setFlags(Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG);
            BitmapShader shader = new BitmapShader(bmp, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            avatarPaint.setShader(shader);

            Rect mBorderRect = new Rect();
            mBorderRect.set(0, 0, height, width);
            Rect mDrawableRect = new Rect();
            mDrawableRect.set(strokeWidth, strokeWidth, mBorderRect.width() - strokeWidth, mBorderRect.height() - strokeWidth);
            float mDrawableRadius = Math.min(mDrawableRect.height() / 2.0F, mDrawableRect.width() / 2.0F);

            float dx = 0.0F;
            float dy = 0.0F;

            Matrix mShaderMatrix = new Matrix();
            mShaderMatrix.set(null);
            float scale;

            if ((float) bmp.getWidth() * mDrawableRect.height() > mDrawableRect.width() * (float) bmp.getHeight()) {
                scale = mDrawableRect.height() / (float) bmp.getHeight();
                dx = (mDrawableRect.width() - (float) bmp.getWidth() * scale) * 0.5F;
            } else {
                scale = mDrawableRect.width() / (float) bmp.getWidth();
                dy = (mDrawableRect.height() - (float) bmp.getHeight() * scale) * 0.5F;
            }

            mShaderMatrix.setScale(scale, scale);
            mShaderMatrix.postTranslate((float) ((int) (dx + 0.5F) + strokeWidth), (float) ((int) (dy + 0.5F) + strokeWidth));
            shader.setLocalMatrix(mShaderMatrix);

            mCanvas.drawCircle(mDrawableRect.width() / 2, mDrawableRect.height() / 2, mDrawableRadius, avatarPaint);

            if (drawStroke) {
                avatarPaint.reset();
                avatarPaint.setFlags(Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG);
                avatarPaint.setColor(highlightColor);
                avatarPaint.setStyle(Paint.Style.STROKE);
                avatarPaint.setStrokeWidth(strokeWidth);
                mCanvas.drawCircle(mDrawableRadius, mDrawableRadius, mDrawableRadius, avatarPaint);
            }

            mCanvas.restore();
        }

    }


}



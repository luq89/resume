package com.example.lukas.resume.helpers.Enums;

/**
 * Created by luq89 on 17-Mar-17.
 */

public enum ColorThemes {

    THEME_1 {
        private String themePattern = "#104E5B;#ffffff;#6396A3;#348292;#CCD91A";

        public String getTheme() {
            return themePattern;
        }

        @Override
        public boolean equals(String typeface) {
            return this.themePattern.equals(typeface);
        }
    },

    THEME_2 {
        private String themePattern = "#348292;#33312E;#99672F;#C7A048;#348292;#C7A048";

        public String getTheme() {
            return themePattern;
        }

        @Override
        public boolean equals(String typeface) {
            return this.themePattern.equals(typeface);
        }
    },

    THEME_3 {
        private String themePattern = "#CCD91A;#33312E;#668C4D;#99B333;#336666;#CCD91A";

        public String getTheme() {
            return themePattern;
        }

        @Override
        public boolean equals(String typeface) {
            return this.themePattern.equals(typeface);
        }
    },;

    public abstract String getTheme();

    public abstract boolean equals(String typeface);


}






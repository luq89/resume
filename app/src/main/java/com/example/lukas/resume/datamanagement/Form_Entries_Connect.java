package com.example.lukas.resume.datamanagement;

/**
 * Created by Lukas on 06.05.2015.
 */

public class Form_Entries_Connect implements UserEntry {


    private long id;

    private String connect_Option;
    private String connect_User;

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }


    public String getOption() {
        return connect_Option;
    }

    public void setOption(String option) {
        this.connect_Option = option;
    }

    public String getUser() {
        return connect_User;
    }

    public void setUser(String user) {
        this.connect_User = user;
    }

    @Override
    public String toCustomString() {
        return
                "ID" + id + " ; "
                        + "Option: " + connect_Option + " ; "
                        + "User: " + connect_User + " ; "
                ;
    }

}

package com.example.lukas.resume.views.dialogs;

import android.content.ContentValues;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.lukas.resume.R;
import com.example.lukas.resume.adapter.CustomPagerAdapterSkill;
import com.example.lukas.resume.datamanagement.Form_Entries_Skill;
import com.example.lukas.resume.datamanagement.SQLiteHelper;
import com.example.lukas.resume.datamanagement.UserDataHub;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.util.Calendar;
import java.util.List;


public class SkillsetDialog implements ICustomDialog {

    private TextView saveTextView;
    private TextView titleTextView;
    private LinearLayout dismissLinearLayout;
    private LinearLayout errorMessageContainerLinearLayout;
    private GridLayout titleContainerGridLayout;
    private LinearLayout pagerContainerLinearLayout;
    private ViewPager pager;
    private DiscreteSeekBar seekBar;

    String Categories[] = {"General", "Development", "Design"};
    String Category = Categories[0];
    String SubCategory = getSubCategory(0, 0);

    private Calendar calendar;

    private View rootView;
    private Context context;

    public SkillsetDialog(final View rootView, final Context context) {
        this.rootView = rootView;
        this.context = context;
        this.calendar = Calendar.getInstance();

        initDialog();
    }

    @Override
    public ContentValues getContentValues() {

        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLiteHelper.COLUMN_SKILL_CATEGORY, getSelectedCategory());
        contentValues.put(SQLiteHelper.COLUMN_SKILL_SUBCATEGORY, getSelectedSubCategory());
        contentValues.put(SQLiteHelper.COLUMN_SKILL_RATING, seekBar.getProgress());


        return null;
    }

    @Override
    public ContentValues getContentValues(long mId) {
        return null;
    }

    @Override
    public void initDialog() {

        this.titleTextView = (TextView) rootView.findViewById(R.id
                .fragment_formgenerator_dialog_skill_title);

        this.saveTextView = (TextView) rootView.findViewById(R.id
                .fragment_formgenerator_dialog_skills_save);


        this.dismissLinearLayout = (LinearLayout) rootView.findViewById(
                R.id.fragment_formgenerator_dialog_skill_dismiss);

        this.errorMessageContainerLinearLayout = (LinearLayout) rootView.findViewById(R.id
                .fragment_formgenerator_dialog_skill_errormessage);

        this.seekBar = (DiscreteSeekBar) rootView.findViewById(
                R.id.fragment_formgenerator_dialog_skill_seekbar);


        this.pager = (ViewPager) rootView.findViewById(
                R.id.fragment_formgenerator_dialog_skill_pager);

        this.pager.setAdapter(new CustomPagerAdapterSkill(
                context,
                context.getResources().getStringArray(
                        R.array.fragment_formgenerator_dialog_skills_array_category))
        );

        this.titleContainerGridLayout = (GridLayout) rootView.findViewById(R.id.fragment_formgenerator_dialog_gridlayout);

        this.pagerContainerLinearLayout = (LinearLayout) rootView.findViewById(R.id
                .fragment_formgenerator_dialog_skill_pager_container);

        this.pagerContainerLinearLayout.setVisibility(View.GONE);


    }

    @Override
    public void initOnClickListeners() {

    }

    @Override
    public void readEntry() {

    }

    @Override
    public void readEntry(long entryId) {

        List<Form_Entries_Skill> skillList = UserDataHub.getSkillData();
        Form_Entries_Skill entry = skillList.get(Long.valueOf(entryId).intValue());

        if (null != entry) {


            this.pagerContainerLinearLayout.setVisibility(View.GONE);

            this.titleTextView.setText("Edit: " + entry.getCategory() + " - " + entry
                    .getSubCategory());

            this.seekBar.setProgress((int) entry.getRating());


        }


    }

    @Override
    public void setOnClickListenerForSaveAction(View.OnClickListener listener) {

    }

    @Override
    public void setOnClickListenerForDismissAction(View.OnClickListener listener) {

    }

    public String getSelectedCategory() {
        int category = ((CustomPagerAdapterSkill) pager.getAdapter())
                .getSelectedCategory();

        return this.Categories[category];
    }

    public String getSelectedSubCategory() {

        int category = ((CustomPagerAdapterSkill) pager.getAdapter())
                .getSelectedCategory();

        int subcategory = ((CustomPagerAdapterSkill) pager.getAdapter())
                .getSelectedSubCategory();

        return getSubCategory(category, subcategory);
    }


    private String getSubCategory(int cat, int subcat) {

        String[] arr = {};

        switch (cat) {

            case 0:
                arr = context.getResources().getStringArray(R.array
                        .fragment_formgenerator_dialog_skills_array_subcategory_general);
                break;

            case 1:
                arr = context.getResources().getStringArray(R.array
                        .fragment_formgenerator_dialog_skills_array_subcategory_development);
                break;
            case 2:
                arr = context.getResources().getStringArray(R.array
                        .fragment_formgenerator_dialog_skills_array_subcategory_design);
                break;

            default:

        }
        return arr[subcat];
    }


    @Override
    public void onError(String msg) {

        errorMessageContainerLinearLayout.setBackgroundColor(
                context.getResources().getColor(R.color.Tint4));

        ((TextView) ((RelativeLayout) errorMessageContainerLinearLayout
                .getChildAt(1))
                .getChildAt(1))
                .setText(msg);

        errorMessageContainerLinearLayout.setVisibility(View.VISIBLE);

    }


}
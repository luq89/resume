package com.example.lukas.resume.adapter;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lukas.resume.ContentFragment;
import com.example.lukas.resume.MainActivity;
import com.example.lukas.resume.R;
import com.example.lukas.resume.datamanagement.Form_Entries_Education;
import com.example.lukas.resume.datamanagement.SQLiteHelper;
import com.example.lukas.resume.datamanagement.UserDataHub;
import com.example.lukas.resume.views.dialogs.InputDialogs;

import java.util.List;

import javax.security.auth.callback.Callback;

/**
 * Created by Lukas on 14.05.2015.
 */
public class CustomRecylerViewAdapterExpEdu extends RecyclerView.Adapter<CustomRecylerViewAdapterExpEdu.AdapterViewHolder> {


    private Callback mCallback;
    private List<Form_Entries_Education> mDataset;
    private ContentFragment mContentFragment;

    public class AdapterViewHolder extends RecyclerView.ViewHolder {

        private CardView cardView;

        public AdapterViewHolder(CardView v) {
            super(v);
            cardView = v;
        }

        public CardView getCardView() {
            return cardView;
        }

        public View findViewById(int res) {
            return cardView.findViewById(res);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CustomRecylerViewAdapterExpEdu(ContentFragment contentFragment, List<Form_Entries_Education> myDataset) {
        mContentFragment = contentFragment;
        mDataset = myDataset;

    }

    // Create new views (invoked by the layout manager)
    @Override
    public AdapterViewHolder onCreateViewHolder(ViewGroup parent,
                                                int viewType) {

        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview, parent, false);
        AdapterViewHolder vh = new AdapterViewHolder((CardView) v);


        return vh;

    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(AdapterViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        if (position + 1 == getItemCount()) {

            holder.getCardView().setCardBackgroundColor(mContentFragment.getResources()
                    .getColor(R.color.Tint4));
            holder.findViewById(R.id.fragment_cardview_container).setVisibility(View.GONE);
            holder.findViewById(R.id.fragment_cardview_container_addentry).setVisibility(View.VISIBLE);


            holder.getCardView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mContentFragment.showDialog();
                }
            });
        } else {


            TextView tV_title = (TextView) holder.cardView.findViewById(R.id.info_title);


            tV_title.setText(mDataset.get(position).getInstitution());


            TextView tv_degree = (TextView) holder.cardView.findViewById(R.id.info_degree);
            tv_degree.setText(mDataset.get(position).getDegree());

            TextView tv_desc = (TextView) holder.cardView.findViewById(R.id.info_description);
            tv_desc.setText(mDataset.get(position).getLocation());

            TextView tv_time = (TextView) holder.cardView.findViewById(R.id.info_time);
            tv_time.setText(mDataset.get(position).getDateBegin() + " - " + mDataset.get(position).getDateEnd());
            ImageView iV_del = (ImageView) holder.cardView.findViewById(R.id.fragment_cardview_deleteentry);
            iV_del.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    new AlertDialog.Builder(mContentFragment.getContext())

                            .setMessage("Are you sure you want to delete this entry?")
                            .setIcon(0)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    deleteEntry(mDataset.get(position).getId());
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                }
            });

            ImageView iV_edit = (ImageView) holder.cardView.findViewById(R.id.fragment_cardview_editentry);
            iV_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editEntry(mDataset.get(position).getId());
                }
            });


        }


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size() + 1;
    }


    public void deleteEntry(long id) {
        UserDataHub.delete(SQLiteHelper.TABLE_SKILL, id);
        notifyDataSetChanged();
        mContentFragment.updateEducationAdapter();
    }

    public void editEntry(long id) {


        FragmentTransaction ft = ((MainActivity) mContentFragment.getContext()).getSupportFragmentManager().beginTransaction();
        Fragment dialog = ((MainActivity) mContentFragment.getContext()).getSupportFragmentManager().findFragmentByTag("dialog");

        if (dialog != null) {
            ft.remove(dialog);
        }
        ft.addToBackStack(null);

        //Create and show dialog.
        InputDialogs newDialog = InputDialogs.newInstance(id, true, 0);
        newDialog.show(ft, "dialog");

    }

}

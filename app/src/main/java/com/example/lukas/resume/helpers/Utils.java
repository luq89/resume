package com.example.lukas.resume.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;

import com.example.lukas.resume.R;
import com.example.lukas.resume.datamanagement.SQLiteHelper;

/**
 * Created by Lukas on 21.04.2016.
 */
public class Utils {

    private static Context context;

    private Utils(Context context) {
        Utils.context = context;
    }


    public static Utils newInstance(Context context) {
        return new Utils(context);
    }

    public static int optionToResId(String option) {

        int resId;

        switch (option) {

            //Icons Social Connect

            case "Facebook":
                resId = R.drawable.ic_social_facebook;
                break;
            case "Twitter":
                resId = R.drawable.ic_social_twitter;
                break;
            case "Printerest":
                resId = R.drawable.ic_social_printerest;
                break;
            case "Youtube":
                resId = R.drawable.ic_social_youtube;
                break;
            case "Google+":
                resId = R.drawable.ic_social_google;
                break;
            case "LinkedIn":
                resId = R.drawable.ic_social_linkedin;
                break;
            case "Wordpress":
                resId = R.drawable.ic_social_wordpress;
                break;
            case "Email":
                resId = R.drawable.ic_social_email;
                break;

            case "StackExchange":
                resId = R.drawable.ic_social_stackexchange;
                break;

            case "GitHub":
                resId = R.drawable.ic_social_github;
                break;


            //Icons Skill General

            case "Microsoft Office":
                resId = R.drawable.ic_general_msoffice;
                break;

            //Icons Skill Development

            case "C/C++":
                resId = R.drawable.ic_skill_development_cpp;
                break;
            case "CSharp":
                resId = R.drawable.ic_skill_development_csharp;
                break;

            case "HTML/CSS":
                resId = R.drawable.ic_skill_development_html;
                break;

            case "HTML 5":
                resId = R.drawable.ic_skill_development_html5;
                break;

            case "JavaScript":
                resId = R.drawable.ic_skill_development_javascript;
                break;

            case "PHP":
                resId = R.drawable.ic_skill_development_php;
                break;

            case "Phyton":
                resId = R.drawable.ic_skill_development_phyton;
                break;

            case "Ruby":
                resId = R.drawable.ic_skill_development_ruby;
                break;

            case "SQL":
                resId = R.drawable.ic_skill_development_sql;
                break;

            case "XML":
                resId = R.drawable.ic_skill_development_xml;
                break;


            //Icons Skill Design

            case "Blender":
                resId = R.drawable.ic_design_blender;
                break;

            case "InDesign":
                resId = R.drawable.ic_design_id;
                break;

            case "Fireworks":
                resId = R.drawable.ic_design_fw;
                break;

            case "Photoshop":
                resId = R.drawable.ic_design_ps;
                break;

            case "Illustrator":
                resId = R.drawable.ic_design_ai;
                break;

            case "AfterEffects":
                resId = R.drawable.ic_design_ae;
                break;

            default:
                resId = R.drawable.ic_social_facebook;
                break;
        }

        return resId;
    }


    public static Bitmap optionToBitmap(String option, int iconWidth, int iconHeight) {

        int resId;

        switch (option) {

            //Icons Social Connect

            case "Facebook":
                resId = R.drawable.ic_social_facebook;
                break;
            case "Twitter":
                resId = R.drawable.ic_social_twitter;
                break;
            case "Printerest":
                resId = R.drawable.ic_social_printerest;
                break;
            case "Youtube":
                resId = R.drawable.ic_social_youtube;
                break;
            case "Google+":
                resId = R.drawable.ic_social_google;
                break;
            case "LinkedIn":
                resId = R.drawable.ic_social_linkedin;
                break;
            case "Wordpress":
                resId = R.drawable.ic_social_wordpress;
                break;
            case "Email":
                resId = R.drawable.ic_social_email;
                break;

            case "StackExchange":
                resId = R.drawable.ic_social_stackexchange;
                break;

            case "GitHub":
                resId = R.drawable.ic_social_github;
                break;


            //Icons Skill General

            case "Microsoft Office":
                resId = R.drawable.ic_general_msoffice;
                break;

            //Icons Skill Development

            case "C/C++":
                resId = R.drawable.ic_skill_development_cpp;
                break;
            case "CSharp":
                resId = R.drawable.ic_skill_development_csharp;
                break;

            case "HTML/CSS":
                resId = R.drawable.ic_skill_development_html;
                break;

            case "HTML 5":
                resId = R.drawable.ic_skill_development_html5;
                break;

            case "JavaScript":
                resId = R.drawable.ic_skill_development_javascript;
                break;

            case "PHP":
                resId = R.drawable.ic_skill_development_php;
                break;

            case "Phyton":
                resId = R.drawable.ic_skill_development_phyton;
                break;

            case "Ruby":
                resId = R.drawable.ic_skill_development_ruby;
                break;

            case "SQL":
                resId = R.drawable.ic_skill_development_sql;
                break;

            case "XML":
                resId = R.drawable.ic_skill_development_xml;
                break;

            //Icons Skill Design

            case "Blender":
                resId = R.drawable.ic_design_blender;
                break;

            case "InDesign":
                resId = R.drawable.ic_design_id;
                break;

            case "Fireworks":
                resId = R.drawable.ic_design_fw;
                break;

            case "Photoshop":
                resId = R.drawable.ic_design_ps;
                break;

            case "Illustrator":
                resId = R.drawable.ic_design_ai;
                break;

            case "AfterEffects":
                resId = R.drawable.ic_design_ae;
                break;

            default:
                resId = R.drawable.ic_social_facebook;
                break;
        }
        return Bitmap.createScaledBitmap(((BitmapDrawable) context.getResources().getDrawable(resId)).getBitmap(), iconWidth, iconHeight, false);
    }


    public static String getDefaultStringByTableColumnName(String name) {

        String defaultString = "";

        switch (name) {

            case SQLiteHelper.COLUMN_GENERAL_AVATARPATH:
                defaultString = context.getString(R.string.default_avatarpath);
                break;

            case SQLiteHelper.COLUMN_GENERAL_COLORTHEME:
                defaultString = context.getString(R.string.default_colortheme);
                break;


            case SQLiteHelper.COLUMN_GENERAL_DATEFORMAT:
                defaultString = context.getString(R.string.default_dateformat);
                break;


            case SQLiteHelper.COLUMN_GENERAL_SHOWSTARTDIALOG:
                defaultString = context.getString(R.string.default_showStartDialog);
                break;

            case SQLiteHelper.COLUMN_GENERAL_TYPEFACE:
                defaultString = context.getString(R.string.default_typeface);


            default:
                break;
        }

        return defaultString;
    }


}

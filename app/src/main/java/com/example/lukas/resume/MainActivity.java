package com.example.lukas.resume;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.lukas.resume.adapter.CustomFragmentPagerAdapter;
import com.example.lukas.resume.datamanagement.DataSource;
import com.example.lukas.resume.datamanagement.SQLiteHelper;
import com.example.lukas.resume.datamanagement.UserDataHub;
import com.example.lukas.resume.helpers.Contact;
import com.example.lukas.resume.helpers.Enums.SectionTitles;
import com.example.lukas.resume.pageritems.SamplePagerItem;
import com.example.lukas.resume.views.dialogs.InputDialogs;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends AppCompatActivity implements InputDialogs
        .OnFragmentInteractionListener {


    public ViewPager mViewPager;

    private SmartTabLayout mSmartTabLayout;
    private List<SamplePagerItem> mTabs = new ArrayList<>();
    private FloatingActionButton fab_preview;
    private ContentFragment mContentFragment;


    private UserDataHub userDataHub;


    private static final int PICK_IMAGE = 1;
    private static final int PICK_CONTACT = 2015;


    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_formgenerator);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().hide();

        DataSource dataSource = new DataSource(this);
        userDataHub = UserDataHub.newInstance(dataSource, getApplicationContext());
        UserDataHub.refreshUserData();

        addTabsToList();


        mViewPager = (ViewPager) findViewById(R.id.activity_formgenerator_viewpager);
        //ViewPager.setAdapter(new TempPagerAdapter(getSupportFragmentManager(), mTabs));
        mViewPager.setAdapter(new
                CustomFragmentPagerAdapter(getSupportFragmentManager(), mTabs));
        mSmartTabLayout = (SmartTabLayout) findViewById(R.id.activity_formgenerator_slidingtabs);
        mSmartTabLayout.setViewPager(mViewPager);

        mContentFragment = new ContentFragment();
        fab_preview = (FloatingActionButton) findViewById(R.id.fab_preview);
        fab_preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPreviewFragment();
            }
        });

    }

    private void addTabsToList() {
        mTabs.add(new SamplePagerItem(SectionTitles.BASICINFORMATION));
        mTabs.add(new SamplePagerItem(SectionTitles.EDUCATION));
        mTabs.add(new SamplePagerItem(SectionTitles.SKILLSET));
        mTabs.add(new SamplePagerItem(SectionTitles.CONNECT));
        mTabs.add(new SamplePagerItem(SectionTitles.FINISH));
    }

    private void openPreviewFragment() {

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment preview = getSupportFragmentManager().findFragmentByTag("dialog");

        if (preview != null) {
            ft.remove(preview);
        }
        ft.addToBackStack(null);
        //Create and show dialog.
        FormFragment newPreview = FormFragment.newInstance();
        newPreview.setTargetFragment(mContentFragment, 0);
        newPreview.show(ft, "preview");

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PICK_CONTACT && resultCode == RESULT_OK) {
            collectContactInformationFromIntent(data);

        } else {
            cancelContactPick();
        }

        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            loadAndSetImageFromIntent(data);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void loadAndSetImageFromIntent(Intent data) {

        try {

            Bitmap bitmap = null;
            // recyle unused bitmaps
            if (bitmap != null) {
                bitmap.recycle();
            }
            InputStream inputStream = getContentResolver().openInputStream(
                    data.getData());
            bitmap = BitmapFactory.decodeStream(inputStream);
            inputStream.close();

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
            byte[] bArray = bos.toByteArray();


            File cachePath = new File(getCacheDir(), "images");

            if (!cachePath.exists()) {
                cachePath.mkdirs();
            }
            FileOutputStream outputStream = new FileOutputStream(cachePath + "/avatarimg.png"); // overwrites image every time
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);


            if (bArray.length > 0) {
                updateAvatarImage(cachePath, bArray);
            }

        } catch (FileNotFoundException e) {
            Toast.makeText(this, R.string.errormessage_general, Toast.LENGTH_LONG).show();
            onException();

        } catch (IOException e) {
            Toast.makeText(this, R.string.errormessage_general, Toast.LENGTH_LONG).show();
            onException();

        }
    }

    private void updateAvatarImage(File cachePath, byte[] bArray) {

        ContentValues mContentValues = new ContentValues();
        mContentValues.put(SQLiteHelper.COLUMN_GENERAL_AVATARPATH, cachePath + "/avatarimg.png");

        if (UserDataHub.isTableEmpty(SQLiteHelper.TABLE_GENERAL)) {
            UserDataHub.insert(SQLiteHelper.TABLE_GENERAL, mContentValues);
        } else {
            UserDataHub.update(SQLiteHelper.TABLE_GENERAL, mContentValues);
        }


        Fragment contactsPage = getActiveFragmentPage();
        if (mViewPager.getCurrentItem() == 0 && contactsPage != null) {
            ((ContentFragment) contactsPage).updateAvatarImage(bArray);
        }

    }


    private void collectContactInformationFromIntent(Intent intentData) {

        try {

            Map<String, String> contactDataMap = new HashMap<String, String>();
            Uri contactData = intentData.getData();
            Cursor cursor = getContentResolver().query(contactData, null, null, null, null);
            cursor.moveToFirst();

            addNameToContactMap(cursor, contactDataMap);
            addPhoneNumberToContactMap(cursor, contactDataMap);
            addEmailToContactMap(cursor, contactDataMap);
            addAddressToContactMap(cursor, contactDataMap);
            setBirthday(cursor, contactDataMap);


            cursor.close();

            Log.d("activity result", "onActivityResult - got contact: "
                    + contactDataMap.get(Contact.NAME) + "; "
                    + contactDataMap.get(Contact.MAIL) + "; "
                    + contactDataMap.get(Contact.PHONE) + "; "
                    + contactDataMap.get(Contact.BIRTHDAY) + "; "
                    + contactDataMap.get(Contact.ADDRESS));


            Fragment contactsPage = getActiveFragmentPage();

            if (mViewPager.getCurrentItem() == 0 && contactsPage != null) {
                ((ContentFragment) contactsPage).updateContact(contactDataMap);
            }


        } catch (Exception e) {
            Toast.makeText(this, R.string.errormessage_general, Toast.LENGTH_LONG).show();
            e.printStackTrace();
            onException();
        }
    }

    private void cancelContactPick() {

        Fragment contactsPage = getActiveFragmentPage();
        if (mViewPager.getCurrentItem() == 0 && contactsPage != null) {
            ((ContentFragment) contactsPage).resetImportContactFab();
        }
    }


    private void setBirthday(Cursor cursor, Map<String, String> contactDataMap) {

        String contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

        String[] projection = new String[]{
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Event.CONTACT_ID,
                ContactsContract.CommonDataKinds.Event.START_DATE
        };


        String where = ContactsContract.CommonDataKinds.Event.TYPE
                + "=" + ContactsContract.CommonDataKinds.Event.TYPE_BIRTHDAY +
                " and " + ContactsContract.CommonDataKinds.Event.MIMETYPE + " = '" + ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE
                + "' and " + ContactsContract.Data.CONTACT_ID + " = " + contactId;


        String[] selectionArgs = new String[]{
                ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE
        };


        Cursor birthdayCur = getContentResolver().query(
                ContactsContract.Data.CONTENT_URI,
                projection,
                where,
                null, ContactsContract.Contacts.DISPLAY_NAME);

        while (birthdayCur.moveToNext()) {
            String birthday = birthdayCur.getString(
                    birthdayCur.getColumnIndex(ContactsContract.CommonDataKinds.Event.START_DATE));
            contactDataMap.put(Contact.BIRTHDAY, (birthday != null) ? birthday : "");
            break;
        }

        birthdayCur.close();

    }

    private void addNameToContactMap(Cursor cursor, Map<String, String> contactDataMap) {

        String contactName = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));
        contactDataMap.put(Contact.NAME, (contactName != null) ? contactName : "");
    }

    private void addPhoneNumberToContactMap(Cursor cursor, Map<String, String> contactDataMap) {

        String contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

        if (Integer.parseInt(cursor.getString(
                cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
            Cursor pCur = getContentResolver().query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                    new String[]{contactId},
                    null);

            while (pCur.moveToNext()) {
                String number = pCur.getString(pCur.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER));
                contactDataMap.put(Contact.PHONE, (number != null) ? number : "");
                break;
            }
            pCur.close();
        }
    }

    private void addEmailToContactMap(Cursor cursor, Map<String, String> contactDataMap) {

        String contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

        Cursor emailCur = getContentResolver().query(
                ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                new String[]{contactId}, null);

        while (emailCur.moveToNext()) {
            String email = emailCur.getString(
                    emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));

            contactDataMap.put(Contact.MAIL, (email != null) ? email : "");
            break;
        }
        emailCur.close();
    }

    private void addAddressToContactMap(Cursor cursor, Map<String, String> contactDataMap) {

        String contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

        Cursor addressCur = getContentResolver().query(
                ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.StructuredPostal.CONTACT_ID + " = ?",
                new String[]{contactId}, null);

        while (addressCur.moveToNext()) {
            String address = addressCur.getString(
                    addressCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.FORMATTED_ADDRESS));

            contactDataMap.put(Contact.ADDRESS, (address != null) ? address : "");
            break;
        }
        addressCur.close();
    }


    private Fragment getActiveFragmentPage() {

        String fragmentPageTag = "android:switcher:" + R.id
                .activity_formgenerator_viewpager + ":" + mViewPager.getCurrentItem();

        return getSupportFragmentManager().findFragmentByTag(fragmentPageTag);
    }


    private void onException() {

        Fragment page = getActiveFragmentPage();
        if (mViewPager.getCurrentItem() == 1 && page != null) {
            ((ContentFragment) page).basicInfoSectionView.getProgressIndicator().setVisibility(View
                    .INVISIBLE);
        }
    }

    @Override
    public void onUpdateEducationAdapter() {
        Fragment page = getActiveFragmentPage();
        if (page != null) {
            ((ContentFragment) page).updateEducationAdapter();
        }
    }


    @Override
    public void onUpdateExperienceAdapter() {

        Fragment page = getActiveFragmentPage();
        if (page != null) {
            ((ContentFragment) page).updateExperienceAdapter();
        }

    }

    @Override
    public void onUpdateSkillAdapter() {
        Fragment page = getActiveFragmentPage();
        if (page != null) {
            ((ContentFragment) page).updateSkillAdapter();
        }
    }

    @Override
    public void onUpdateLanguageAdapter() {
        Fragment page = getActiveFragmentPage();
        if (page != null) {
            ((ContentFragment) page).updateLanguageAdapter();
        }
    }

    @Override
    public void onUpdateConnectAdapter() {

        Fragment page = getActiveFragmentPage();
        if (page != null) {
            ((ContentFragment) page).updateConnectAdapter();
        }
    }


}





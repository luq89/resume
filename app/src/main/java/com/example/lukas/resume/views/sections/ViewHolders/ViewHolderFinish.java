package com.example.lukas.resume.views.sections.ViewHolders;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.example.lukas.resume.ContentFragment;
import com.example.lukas.resume.R;
import com.getbase.floatingactionbutton.FloatingActionButton;

/**
 * Created by luq89 on 08-Mar-17.
 */


public class ViewHolderFinish extends GlobalViewHolder {
    public FloatingActionButton exportFab;
    public TextView showImressumTextView;
    public TextView resetProgressTextView;


    public ViewHolderFinish(Context context, ContentFragment parent) {
        super(context, parent);

        this.exportFab = (FloatingActionButton) super.findViewWithinSubviewById(R.id
                .fragment_formgenerator_finish_fab_export);

        this.showImressumTextView = (TextView) super.findViewWithinSubviewById(R.id
                .fragment_formgenerator_finish_impressum);

        this.resetProgressTextView = (TextView) super.findViewWithinSubviewById(R.id
                .fragment_formgenerator_finish_reset);
    }

    public void setOnClickListenerForExportFab(View.OnClickListener listener) {
        this.exportFab.setOnClickListener(listener);
    }

    public void setOnClickListenerForShowImpressumTextView(View.OnClickListener listener) {
        this.exportFab.setOnClickListener(listener);
    }

    public void setOnClickListenerForShowResetProgress(View.OnClickListener listener) {
        this.resetProgressTextView.setOnClickListener(listener);
    }


}


package com.example.lukas.resume.views.sections.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.LinearLayout;

import com.example.lukas.resume.R;
import com.example.lukas.resume.adapter.CustomRecylerViewAdapterExpEdu;
import com.example.lukas.resume.datamanagement.UserDataHub;
import com.example.lukas.resume.views.sections.ViewHolder;

/**
 * Created by luq89 on 08-Mar-17.
 */


public class EducationViewHolder implements ViewHolder {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private GlobalViewHolder global;
    private View subView;


    public EducationViewHolder(GlobalViewHolder global) {
        this.global = global;
        this.global.setLayoutResource(R.layout.fragment_formgenerator_section_connect_temp);
        this.subView = global.getSubView();
        initViewHolder();

    }

    public RecyclerView.Adapter getAdapter() {
        return adapter;
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        this.adapter = adapter;
    }

    public void updateAdapter() {
        this.recyclerView.setAdapter(new CustomRecylerViewAdapterExpEdu(
                global.getParent(), UserDataHub.getEducationData()));
        this.adapter.notifyDataSetChanged();
    }

    @Override
    public void initViewHolder() {
        this.recyclerView = (RecyclerView) subView.findViewById(R.id.my_recycler_view);
        this.recyclerView.setHasFixedSize(true);
        this.layoutManager = new StaggeredGridLayoutManager(2, LinearLayout.VERTICAL);
        this.recyclerView.setLayoutManager(layoutManager);
        this.recyclerView.setAdapter(new CustomRecylerViewAdapterExpEdu(
                global.getParent(), UserDataHub.getEducationData()));
    }
}
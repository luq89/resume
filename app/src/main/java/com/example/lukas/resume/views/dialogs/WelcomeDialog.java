package com.example.lukas.resume.views.dialogs;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.lukas.resume.R;
import com.example.lukas.resume.views.dialogs.ViewHolders.WelcomeDialogHolder;

/**
 * Created by Lukas on 19.11.2015.
 */
public class WelcomeDialog extends DialogFragment {

    public static WelcomeDialog newInstance(Context context) {

        WelcomeDialog f = new WelcomeDialog();
        f.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        return f;
    }

    public WelcomeDialog() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_formgenerator_dialog_start, container, false);

        final WelcomeDialogHolder welcomeDialogHolder = new WelcomeDialogHolder(getContext(), rootView);


        welcomeDialogHolder.setOnClickListenerForSaveAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                welcomeDialogHolder.onSave();
                dismiss();
            }
        });

        welcomeDialogHolder.setOnClickListenerForDismissAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        return rootView;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        super.show(manager, tag);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void dismiss() {
        super.dismiss();

    }

    @Override
    public void onStop() {
        super.onStop();
    }
}

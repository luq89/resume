package com.example.lukas.resume.datamanagement;

/**
 * Created by Lukas on 06.05.2015.
 */

public class Form_Entries_Skill implements UserEntry {


    private long id;

    private String skill_Category;
    private String skill_SubCategory;
    private double skill_Rating;


    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }


    public String getSubCategory() {
        return skill_SubCategory;
    }

    public void setSubCategory(String subCategory) {
        this.skill_SubCategory = subCategory;
    }


    public String getCategory() {
        return skill_Category;
    }

    public void setCategory(String category) {
        this.skill_Category = category;
    }


    public double getRating() {
        return skill_Rating;
    }

    public void setRating(double rating) {
        this.skill_Rating = rating;
    }


    @Override
    public String toCustomString() {
        return
                "ID" + id + " ; "
                        + "Categroy: " + skill_Category + " ; "
                        + "SubCategory: " + skill_SubCategory + " ; "
                        + "Rating: " + skill_Rating + " ; "
                ;
    }

}

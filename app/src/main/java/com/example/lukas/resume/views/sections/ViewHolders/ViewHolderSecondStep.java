package com.example.lukas.resume.views.sections.ViewHolders;

import android.widget.ExpandableListView;

import com.example.lukas.resume.adapter.CustomExpandableListViewAdapterEducation;
import com.example.lukas.resume.adapter.CustomExpandableListViewAdapterExperience;
import com.example.lukas.resume.views.sections.ViewHolder;
import com.getbase.floatingactionbutton.FloatingActionButton;

/**
 * Created by luq89 on 08-Mar-17.
 */


public class ViewHolderSecondStep implements ViewHolder {
    private FloatingActionButton addEntryFab;
    private FloatingActionButton addEductationFab;
    private ExpandableListView educationExpandableListView;
    private CustomExpandableListViewAdapterEducation educationAdapter;
    private ExpandableListView experienceExpandableListView;
    private CustomExpandableListViewAdapterExperience experienceAdapter;

    public FloatingActionButton getAddEntryFab() {
        return addEntryFab;
    }

    public void setAddEntryFab(FloatingActionButton addEntryFab) {
        this.addEntryFab = addEntryFab;
    }

    public FloatingActionButton getAddEductationFab() {
        return addEductationFab;
    }

    public void setAddEductationFab(FloatingActionButton addEductationFab) {
        this.addEductationFab = addEductationFab;
    }

    public ExpandableListView getEducationExpandableListView() {
        return educationExpandableListView;
    }

    public void setEducationExpandableListView(ExpandableListView educationExpandableListView) {
        this.educationExpandableListView = educationExpandableListView;
    }

    public CustomExpandableListViewAdapterEducation getEducationAdapter() {
        return educationAdapter;
    }

    public void setEducationAdapter(CustomExpandableListViewAdapterEducation educationAdapter) {
        this.educationAdapter = educationAdapter;
    }

    public ExpandableListView getExperienceExpandableListView() {
        return experienceExpandableListView;
    }

    public void setExperienceExpandableListView(ExpandableListView experienceExpandableListView) {
        this.experienceExpandableListView = experienceExpandableListView;
    }

    public CustomExpandableListViewAdapterExperience getExperienceAdapter() {
        return experienceAdapter;
    }

    public void setExperienceAdapter(CustomExpandableListViewAdapterExperience experienceAdapter) {
        this.experienceAdapter = experienceAdapter;
    }

    @Override
    public void initViewHolder() {

    }
}
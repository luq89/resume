package com.example.lukas.resume.helpers.Enums;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by luq89 on 17-Mar-17.
 */

public enum Fonts {

    JOSEFINSANS {
        private String typeface = "Josefin Sans";


        public String getTypefaceName() {
            return typeface;
        }

        @Override
        public boolean equals(String typeface) {
            return this.typeface.equals(typeface);
        }


        public Typeface getTypeface(Context context) {
            return Typeface.createFromAsset(context.getResources().getAssets(),
                    "fonts/JosefinSansRegular.ttf");
        }


    },

    WHITNEYBOOK {
        private String typeface = "Whitney Book";

        @Override
        public String getTypefaceName() {
            return typeface;
        }

        @Override
        public boolean equals(String typeface) {
            return this.typeface.equals(typeface);
        }


        public Typeface getTypeface(Context context) {
            return Typeface.createFromAsset(context.getResources().getAssets(),
                    "fonts/WhitneyBook" +
                            ".ttf");

        }
    },

    CORBEL {
        private String typeface = "Corbel";

        public String getTypefaceName() {
            return typeface;
        }

        @Override
        public boolean equals(String typeface) {
            return this.typeface.equals(typeface);
        }


        public Typeface getTypeface(Context context) {
            return Typeface.createFromAsset(context.getResources().getAssets(), "fonts/Corbel" +
                    ".ttf");
        }
    };

    public abstract String getTypefaceName();

    public abstract Typeface getTypeface(Context context);

    public abstract boolean equals(String typeface);


}






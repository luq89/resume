package com.example.lukas.resume;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * Created by Lukas on 19.11.2015.
 */
public class FormGeneratorFragment_Dialog_ColorTheme extends DialogFragment {

    static ArrayList<String> mColorThemes;
    OnFragmentInteractionListener mCallback;
    int mSelectedTheme = -1;


    public static FormGeneratorFragment_Dialog_ColorTheme newInstance(ArrayList<String> colorThemes) {
        FormGeneratorFragment_Dialog_ColorTheme f = new FormGeneratorFragment_Dialog_ColorTheme();
        mColorThemes = colorThemes;
        f.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        return f;
    }

    public FormGeneratorFragment_Dialog_ColorTheme() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_formgenerator_dialog_colortheme, container,
                false);


        TextView textView_Apply = (TextView) rootView.findViewById(R.id.fragment_form_dialog_colortheme_apply);
        textView_Apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (null != mCallback && mSelectedTheme != -1) {
                    //mCallback.onThemeSelected(mSelectedTheme);
                }

                dismiss();


            }
        });

        LinearLayout linearLayout_dismiss = (LinearLayout) rootView.findViewById(R.id.fragment_form_dialog_colortheme_dismiss);
        linearLayout_dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        GridLayout gl = (GridLayout) rootView.findViewById(R.id.fragment_form_dialog_colortheme_container);
        RadioGroup rg = new RadioGroup(getActivity());

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mSelectedTheme = group.indexOfChild(group.findViewById(checkedId));
            }
        });


        for (int i = 0; i < mColorThemes.size(); i++) {


            String[] val_splittet = mColorThemes.get(i).split(";");
            for (int k = 0; k < val_splittet.length; k++) {

                GridLayout.LayoutParams llparams = new GridLayout.LayoutParams();
                LinearLayout ll = new LinearLayout(getActivity());
                ll.setBackgroundColor(Color.parseColor(val_splittet[k]));
                llparams.height = 100;
                llparams.width = 100;
                llparams.bottomMargin = 25;
                llparams.setGravity(Gravity.CENTER);
                llparams.rightMargin = k == 3 ? 20 : 0;
                llparams.columnSpec = GridLayout.spec(k + 1);
                llparams.rowSpec = GridLayout.spec(i + 1);
                gl.addView(ll, llparams);

            }

        }


        RadioButton[] rb = new RadioButton[mColorThemes.size()];

        for (int j = 0; j < rb.length; j++) {


            rb[j] = new RadioButton(getActivity());
            GridLayout.LayoutParams rbParams = new GridLayout.LayoutParams();
            rbParams.rightMargin = 25;
            rbParams.bottomMargin = 25;
            rbParams.height = 125;

            rb[j].setGravity(Gravity.CENTER_VERTICAL);
            rb[j].setBottom(50);
            rb[j] = new RadioButton(getActivity());
            rb[j].setButtonTintList(new ColorStateList(
                    new int[][]{

                            new int[]{-android.R.attr.state_enabled}, //disabled
                            new int[]{android.R.attr.state_enabled} //enabled
                    },
                    new int[]{

                            getActivity().getResources().getColor(R.color.White), //disabled
                            getActivity().getResources().getColor(R.color.Main)  //enabled
                    }
            ));
            rg.addView(rb[j], rbParams);
        }

        GridLayout.LayoutParams rgParams = new GridLayout.LayoutParams();
        rgParams.columnSpec = GridLayout.spec(0);
        rgParams.rowSpec = GridLayout.spec(1, mColorThemes.size());
        gl.addView(rg, rgParams);

        return rootView;
    }


    private Drawable changeColorOfDrawable(int color) {

        Drawable drawable = getActivity().getResources().getDrawable(R.drawable.helper_roundish_square_small);
        drawable.setTint(color);
        return drawable;
    }


    @Override
    public void show(FragmentManager manager, String tag) {
        super.show(manager, tag);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        //mCallback.onThemeDissmiss();
    }

    @Override
    public void onStop() {
        super.onStop();
        //mCallback.onThemeDissmiss();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {

            mCallback = (OnFragmentInteractionListener) getTargetFragment();

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public interface OnFragmentInteractionListener {
        //void onThemeSelected(int selectedTheme);
        //void onThemeDissmiss();
    }
}

package com.example.lukas.resume.views.dialogs;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.andexert.expandablelayout.library.ExpandableLayout;
import com.example.lukas.resume.R;
import com.example.lukas.resume.datamanagement.Form_Entries_Language;
import com.example.lukas.resume.datamanagement.SQLiteHelper;
import com.example.lukas.resume.datamanagement.UserDataHub;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.util.Calendar;


public class LanguageDialog implements ICustomDialog {
    TextView saveTextView;
    TextView titleTextView;
    LinearLayout dismissLinearLayout;
    LinearLayout errormessageContainerLinearLayout;
    GridLayout titleContainerGridLayout;
    LinearLayout pagerContainerLinearLayout;
    EditText languageEditText;

    ExpandableLayout expandableLayout;
    ScrollView languageOptionsScrollView;
    LinearLayout connectOptionsContainerLiniearLayout;

    private String[] rating = {"+", "++", "+++", "++++", "+++++"};

    public TextView connectOptionTitleTextView;

    DiscreteSeekBar seekBar;

    private Calendar calendar;
    private View rootView;
    private Context context;

    private boolean isDialogInEditMode;

    public LanguageDialog(View rootView, Context context, boolean isDialogInEditMode) {
        this.rootView = rootView;
        this.context = context;
        this.isDialogInEditMode = isDialogInEditMode;
        this.calendar = Calendar.getInstance();


        initDialog();


    }

    @Override
    public ContentValues getContentValues() {
        return null;
    }

    @Override
    public ContentValues getContentValues(long mId) {
        ContentValues contentValues = new ContentValues();

        Form_Entries_Language entry = (Form_Entries_Language) UserDataHub.getEntry
                (SQLiteHelper
                        .TABLE_LANGUAGE, SQLiteHelper.COLUMN_LANGUAGE_ID, mId);


        contentValues.put(SQLiteHelper.COLUMN_LANGUAGE_NAME, entry.getLanguage());
        contentValues.put(SQLiteHelper.COLUMN_LANGUAGE_RATING, seekBar
                .getProgress());

        return contentValues;


    }

    @Override
    public void initDialog() {

        this.saveTextView = (TextView) rootView.findViewById(R.id
                .fragment_formgenerator_dialog_language_title);

        this.saveTextView = (TextView) rootView.findViewById(R.id
                .fragment_formgenerator_dialog_language_save);

        this.dismissLinearLayout = (LinearLayout) rootView.findViewById(
                R.id.fragment_formgenerator_dialog_language_dismiss);

        this.errormessageContainerLinearLayout = (LinearLayout) rootView.findViewById(
                R.id.fragment_formgenerator_dialog_language_errormessage);

        this.expandableLayout = (ExpandableLayout) rootView.findViewById(
                R.id.fragment_formgenerator_dialog_language_expandablelayout_languageoption);

        this.languageOptionsScrollView = (ScrollView) expandableLayout.getContentLayout()
                .findViewById(R.id.view_dateformat_content_scollview);

        this.seekBar = (DiscreteSeekBar) rootView.findViewById(
                R.id.fragment_formgenerator_dialog_language_seekbar);

        this.titleContainerGridLayout = (GridLayout) rootView.findViewById(
                R.id.fragment_formgenerator_dialog_gridlayout);

        this.pagerContainerLinearLayout = (LinearLayout) rootView.findViewById(R.id
                .fragment_formgenerator_dialog_language_pager_container);

        this.pagerContainerLinearLayout.setVisibility(View.GONE);

        this.connectOptionsContainerLiniearLayout.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (expandableLayout.isOpened()) {
                    ((ImageView) v.findViewById(R.id.view_dateformat_header_icon))
                            .setImageDrawable(context.getResources().getDrawable(R.drawable
                                    .ic_expand_up));

                } else {
                    ((ImageView) v.findViewById(R.id.view_dateformat_header_icon))
                            .setImageDrawable(context.getResources().getDrawable(R.drawable
                                    .ic_expand_down));
                }
            }
        });


        this.connectOptionTitleTextView = ((TextView) expandableLayout.getHeaderLayout
                ().findViewById(R.id
                .view_dateformat_header_title));

        initLanguageOptions();
        initOnClickListeners();

    }

    private void initLanguageOptions() {

        this.connectOptionsContainerLiniearLayout = new LinearLayout(context);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        connectOptionsContainerLiniearLayout.setOrientation(LinearLayout.VERTICAL);

        String[] optionsArr = context.getApplicationContext().getResources().getStringArray(R.array
                .fragment_formgenerator_dialog_language_array);
        this.connectOptionTitleTextView.setText(optionsArr[0]);


        this.connectOptionTitleTextView.setTextAppearance(context.getApplicationContext(), R.style
                .TextAppearance_AppCompat_Medium);

        this.connectOptionTitleTextView.setTextColor(context.getResources().getColor(R.color
                .Font_Dark));


        int count = 0;
        for (String s : optionsArr) {

            RelativeLayout ll_row = new RelativeLayout(context);
            RelativeLayout.LayoutParams lp_row = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            ll_row.setPadding(50, 25, 25, 25);


            LinearLayout ll_textContainer = new LinearLayout(context);
            LinearLayout.LayoutParams lp_llContainer = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            TextView tV = new TextView(context);
            tV.setTextAppearance(context.getApplicationContext(), R.style.TextAppearance_AppCompat_Medium);
            LinearLayout.LayoutParams lP_tV = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            tV.setTextColor(context.getResources().getColor(R.color.Font_Dark));
            tV.setText(s);
            lP_tV.leftMargin = 0;


            TextView tV_rating = new TextView(context);
            tV_rating.setTextAppearance(context.getApplicationContext(), R.style.TextAppearance_AppCompat_Medium);
            LinearLayout.LayoutParams lP_tV_rating = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            tV_rating.setTextColor(context.getResources().getColor(R.color.Font_Dark));
            tV_rating.setText(rating[count]);


            lP_tV_rating.leftMargin = 15;
            lP_tV_rating.rightMargin = 80;
            lP_tV_rating.gravity = Gravity.RIGHT;

            ImageView iV_check = new ImageView(context);
            iV_check.setImageBitmap(Bitmap.createScaledBitmap(((BitmapDrawable) context.getResources().getDrawable(R.drawable.ic_tick)).getBitmap(), context.getResources().getDimensionPixelOffset(R.dimen.fab_icon_size), context.getResources().getDimensionPixelOffset(R.dimen.fab_icon_size), false));
            iV_check.setColorFilter(context.getResources().getColor(R.color.White), PorterDuff.Mode.SRC_ATOP);
            iV_check.setVisibility(View.INVISIBLE);

            RelativeLayout.LayoutParams lp_iVcheck = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            lp_iVcheck.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);

            ll_row.setBackgroundColor(
                    count % 2 == 0 ? context.getResources().getColor(R.color.Tint5) : context.getResources().getColor(R.color.Tint6));


            ll_textContainer.addView(tV, lP_tV);
            ll_textContainer.addView(tV_rating, lP_tV_rating);

            ll_row.addView(ll_textContainer);
            ll_row.addView(iV_check, lp_iVcheck);
            connectOptionsContainerLiniearLayout.addView(ll_row, lp_row);
            count++;


            ll_row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    LinearLayout lparent = (LinearLayout) v.getParent();
                    int count = lparent.getChildCount();

                    for (int i = 0; i < count; i++) {

                        lparent.getChildAt(i).setBackgroundColor(i % 2 == 0 ? context.getResources().getColor(R.color.Tint5) : context.getResources().getColor(R.color.Tint6));
                        ((TextView) ((LinearLayout) ((RelativeLayout) lparent.getChildAt(i)).getChildAt(0)).getChildAt(0)).setTextColor(context.getResources().getColor(R.color.Font_Dark));
                        ((TextView) ((LinearLayout) ((RelativeLayout) lparent.getChildAt(i)).getChildAt(0)).getChildAt(1)).setTextColor(context.getResources().getColor(R.color.Font_Dark));
                        (((RelativeLayout) lparent.getChildAt(i)).getChildAt(1)).setVisibility(View.GONE);
                    }
                    connectOptionTitleTextView.setText(((TextView) ((LinearLayout) ((RelativeLayout) v).getChildAt(0)).getChildAt(0)).getText());
                    ((TextView) ((LinearLayout) ((RelativeLayout) v).getChildAt(0)).getChildAt(0)).setTextColor(context.getResources().getColor(R.color.Font_Light));
                    ((TextView) ((LinearLayout) ((RelativeLayout) v).getChildAt(0)).getChildAt(1)).setTextColor(context.getResources().getColor(R.color.Font_Light));
                    ((RelativeLayout) v).getChildAt(1).setVisibility(View.VISIBLE);
                    v.setBackgroundColor(context.getResources().getColor(R.color.Tint7));
                }
            });
        }

        languageOptionsScrollView.addView(connectOptionsContainerLiniearLayout, lp);

    }

    @Override
    public void initOnClickListeners() {

    }

    @Override
    public void readEntry() {

    }

    @Override
    public void readEntry(long entryId) {

        Form_Entries_Language entry = (Form_Entries_Language) UserDataHub.getEntry
                (SQLiteHelper
                        .TABLE_LANGUAGE, SQLiteHelper.COLUMN_LANGUAGE_ID, entryId);

        if (null != entry) {
            this.titleTextView.setText("Edit: " + entry.getLanguage());
            this.seekBar.setProgress((int) entry.getRating());
        }


    }

    @Override
    public void setOnClickListenerForSaveAction(View.OnClickListener listener) {
        this.saveTextView.setOnClickListener(listener);

    }

    @Override
    public void setOnClickListenerForDismissAction(View.OnClickListener listener) {

    }

    @Override
    public void onError(String message) {

        errormessageContainerLinearLayout.setBackgroundColor(
                context.getResources().getColor(R.color.Tint4));

        ((TextView) ((RelativeLayout) errormessageContainerLinearLayout
                .getChildAt(1))
                .getChildAt(1))
                .setText(message);


        errormessageContainerLinearLayout.setVisibility(View.VISIBLE);

    }
}
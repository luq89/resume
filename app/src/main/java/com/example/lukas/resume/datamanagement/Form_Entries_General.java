package com.example.lukas.resume.datamanagement;

import com.example.lukas.resume.helpers.Enums.ColorThemes;
import com.example.lukas.resume.helpers.Enums.Fonts;

/**
 * Created by Lukas on 06.05.2015.
 */

public class Form_Entries_General implements UserEntry {


    private long id;

    private Fonts currentFont;
    private String general_DateFormat;
    private String general_AvatarImage;
    private String general_ColorTheme;
    private String general_ShowStartDialog;


    @Override
    public long getId() {
        return id;
    }

    //Typeface
    public String getTypeface() {
        return getFont().getTypefaceName();
    }

    public Fonts getFont() {
        if (null == currentFont) {
            currentFont = Fonts.JOSEFINSANS;
        }
        return currentFont;
    }


    @Override
    public void setId(long id) {
        this.id = id;
    }

    public void setTypeface(String typeface) {

        if (Fonts.JOSEFINSANS.equals(typeface)) {
            currentFont = Fonts.JOSEFINSANS;
        }

        if (Fonts.WHITNEYBOOK.equals(typeface)) {
            currentFont = Fonts.WHITNEYBOOK;
        }

        if (Fonts.CORBEL.equals(typeface)) {
            currentFont = Fonts.CORBEL;
        }
    }

    public void setTypeface(Fonts font) {
        this.currentFont = font;
    }

    public String getDateFormat() {
        return general_DateFormat;
    }

    public void setDateFormat(String dateformat) {
        this.general_DateFormat = dateformat;
    }


    public String getAvatarImage() {
        return general_AvatarImage;
    }

    public boolean hasAvatarImage() {
        boolean hasImage = false;

        if (null != general_AvatarImage) {
            hasImage = general_AvatarImage.equals("") == false;
        }
        return hasImage;
    }


    public void setAvatarImage(String path) {
        this.general_AvatarImage = path;
    }


    public String getColorTheme() {
        if (null == general_ColorTheme) {
            general_ColorTheme = ColorThemes.THEME_1.getTheme();
        }
        return general_ColorTheme;
    }

    public void setColorTheme(String theme) {
        this.general_ColorTheme = theme;
    }


    public String getShowStartDialog() {
        return general_ShowStartDialog;
    }

    public boolean getShowStartDialogAsBoolean() {
        boolean showDialog = true;

        if (null != general_ShowStartDialog) {
            showDialog = general_ShowStartDialog.equals("true");
        }
        return showDialog;

    }

    public void setShowStartDialog(String option) {
        this.general_ShowStartDialog = option;
    }


    @Override
    public String toCustomString() {
        return "ID: " + String.valueOf(id) + " ; "
                + "Typeface: " + currentFont.name() + " ; "
                + "DateFormat: " + general_DateFormat.toString() + " ; "
                + "DateFormat: " + String.valueOf(general_ColorTheme) + " ; "
                ;
    }


}

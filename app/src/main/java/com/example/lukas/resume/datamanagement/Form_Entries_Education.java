package com.example.lukas.resume.datamanagement;

/**
 * Created by Lukas on 06.05.2015.
 */

public class Form_Entries_Education implements UserEntry {


    private long id;

    private String education_DateBeginn;
    private String education_DateEnd;
    private String education_Institution;
    private String education_Location;
    private String education_Degree;


    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }


    //Date Begin
    public String getDateBegin() {
        return education_DateBeginn;
    }

    public void setDateBeginn(String dateBegin) {
        this.education_DateBeginn = dateBegin;
    }

    //Date End
    public String getDateEnd() {
        return education_DateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.education_DateEnd = dateEnd;
    }

    //Profession
    public String getInstitution() {
        return education_Institution;
    }

    public void setInstitution(String institution) {
        this.education_Institution = institution;
    }


    //Born
    public String getLocation() {
        return education_Location;
    }

    public void setLocation(String location) {
        this.education_Location = location;
    }


    //Born
    public String getDegree() {
        return education_Degree;
    }

    public void setDegree(String degree) {
        this.education_Degree = degree;
    }

    @Override
    public String toCustomString() {
        return
                "ID" + id + " ; "
                        + "DateBegin: " + education_DateBeginn + " ; "
                        + "DateEnd: " + education_DateEnd + " ; "
                        + "What: " + education_Institution + " ; "
                        + "Where: " + education_Location + " ; "
                ;
    }

}

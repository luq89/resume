package com.example.lukas.resume.views.sections.ViewHolders;

import android.content.Context;
import android.view.View;
import android.widget.ExpandableListView;

import com.example.lukas.resume.ContentFragment;
import com.example.lukas.resume.R;
import com.example.lukas.resume.adapter.CustomExpandableListViewAdapterConnect;
import com.example.lukas.resume.datamanagement.UserDataHub;
import com.getbase.floatingactionbutton.FloatingActionButton;

/**
 * Created by luq89 on 08-Mar-17.
 */


public class ConnectViewHolder extends GlobalViewHolder {
    private FloatingActionButton addEntryFab;
    private ExpandableListView expandableListViewConnect;
    private CustomExpandableListViewAdapterConnect adapterConnect;

    public ConnectViewHolder(Context context, ContentFragment parent) {
        super(context, parent);

        this.expandableListViewConnect = (ExpandableListView) super.findViewWithinSubviewById(R.id
                .fragment_formgenerator_fifthstep_expandablelistview);

        this.adapterConnect = new CustomExpandableListViewAdapterConnect(super.getContext(),
                super.getParent(), UserDataHub.getConnectData());

        this.expandableListViewConnect.setAdapter(adapterConnect);

        this.addEntryFab = (FloatingActionButton) super.findViewWithinSubviewById(R.id
                .fragment_formgenerator_fifthstep_connect_fab_addentry);

    }

    public void setOnClickListenerForAddSkillFab(View.OnClickListener listener) {
        this.addEntryFab.setOnClickListener(listener);
    }


    public void updateAdapter() {
        this.expandableListViewConnect.setAdapter(new CustomExpandableListViewAdapterConnect(
                super.getContext(),
                super.getParent(),
                UserDataHub.getConnectData()
        ));
    }
}

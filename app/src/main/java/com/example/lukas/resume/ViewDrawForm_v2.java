package com.example.lukas.resume;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;

import com.example.lukas.resume.datamanagement.DataSource;
import com.example.lukas.resume.datamanagement.Form_Entries_BasicInfo;
import com.example.lukas.resume.datamanagement.Form_Entries_Connect;
import com.example.lukas.resume.datamanagement.Form_Entries_Experience;
import com.example.lukas.resume.datamanagement.Form_Entries_General;
import com.example.lukas.resume.datamanagement.Form_Entries_Skill;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Lukas on 07.08.2015.
 */


public class ViewDrawForm_v2 {


    Context mContext;

    private class FormDimensions {
        int canvas_Width,
                canvas_Height,
                titleContainer_Width,
                titleContainer_Height,
                basicInformationContainer_Width,
                basicInformationContainer_Height,
                basicInformationContainer_Avatar_Width,
                basicInformationContainer_Avatar_Height,
                experienceContainer_Width,
                experienceContainer_Height,
                connectContainer_Width,
                connectContainer_Height,
                footerContainer_Width,
                footerContainer_Height,
                generalIcon_Width_Small,
                generalIcon_Height_Small,
                generalIcon_Width_Medium,
                generalIcon_Height_Medium,
                generalIcon_Width_Large,
                generalIcon_Height_Large,
                overlayIcon_EditMode_Offset,
                offsetSectionStart_y,
                offset_y,
                offsetBasicInformation_y;


        float canvasFontSize_Small,
                canvasFontSize_Medium,
                canvasFontSize_Large;


        float centerVert;
        float gPositionY;
    }


    public ArrayList<Integer> colors;

    public int[] mSectionPos;


    private Typeface mTypeface;

    private SimpleDateFormat mSimpleDateFormatSource;
    private SimpleDateFormat mSimpleDateFormatTarget;


    FormDimensions dim = new FormDimensions();


    private Form_Entries_BasicInfo mFormValue_BasicInformation;
    private List<Form_Entries_Experience> mFormValues_Experience;
    private List<Form_Entries_Skill> mFormValues_Skill;
    private List<Form_Entries_Connect> mFormValues_Connect;
    private Form_Entries_General mFormValue_General;


    String fallBackString = "";


    private int CANVAS_WIDTH = 1444;
    private int CANVAS_HEIGHT = 6000;


    public ViewDrawForm_v2(Context context) {
        mContext = context;
        dim.canvas_Width = CANVAS_WIDTH;
        dim.canvas_Height = CANVAS_HEIGHT;

        dim.centerVert = dim.canvas_Width / 2;

        dim.titleContainer_Width = dim.canvas_Width;
        dim.titleContainer_Height = 100;

        dim.basicInformationContainer_Width = dim.canvas_Width;
        dim.basicInformationContainer_Height = 800;


        dim.basicInformationContainer_Avatar_Width = 450;
        dim.basicInformationContainer_Avatar_Height = 450;

        dim.experienceContainer_Width = dim.canvas_Width;
        dim.experienceContainer_Height = 5000;


        dim.connectContainer_Width = dim.canvas_Width;
        dim.connectContainer_Height = 5000;

        dim.footerContainer_Width = dim.canvas_Width;
        dim.footerContainer_Height = 100;


        dim.canvasFontSize_Small = mContext.getResources().getDimension(R.dimen.form_font_size_small);
        dim.canvasFontSize_Medium = mContext.getResources().getDimension(R.dimen.form_font_size_medium);
        dim.canvasFontSize_Large = mContext.getResources().getDimension(R.dimen.form_font_size_large);


        dim.generalIcon_Width_Small = 28;
        dim.generalIcon_Height_Small = 28;


        dim.generalIcon_Width_Medium = 36;
        dim.generalIcon_Height_Medium = 36;

        dim.generalIcon_Width_Large = 56;
        dim.generalIcon_Height_Large = 56;

        dim.offset_y = 75;
        dim.offsetBasicInformation_y = 40;
        dim.offsetSectionStart_y = 150;
        dim.overlayIcon_EditMode_Offset = 30;

        dim.gPositionY = 0;
        initDataFields();
        fallBackString = mContext.getResources().getString(R.string.general_emptyinput);

        mSectionPos = new int[]{10, 0, 0, 0, 0};

    }


    public void setColors(ArrayList<Integer> colors) {
        this.colors = colors;
    }

    public int[] getSectionsPos() {
        return mSectionPos;
    }

    public void initDataFields() {

        DataSource mDataSource = new DataSource(mContext);
        mDataSource.open();

        mFormValue_BasicInformation = mDataSource.getBasicInformationEntry();
        mFormValues_Experience = mDataSource.getAllExperienceEntries();
        mFormValues_Skill = mDataSource.getAllSkillEntries();
        mFormValues_Connect = mDataSource.getAllConnectEntries();
        mFormValue_General = mDataSource.getGeneralEntry();

        mSimpleDateFormatSource = new SimpleDateFormat(mContext.getResources().getString(R.string.default_dateformat));

        if (null != mFormValue_General.getDateFormat()) {
            mSimpleDateFormatTarget = new SimpleDateFormat(mFormValue_General.getDateFormat());
        } else {
            mSimpleDateFormatTarget = new SimpleDateFormat(mContext.getResources().getString(R.string.default_dateformat));
        }


        if (mFormValue_General.getTypeface().equals("") || mFormValue_General.getTypeface().equals("DEFAULT")) {
            mTypeface = Typeface.createFromAsset(mContext.getResources().getAssets(), "fonts/JosefinSansRegular.ttf");
        } else if (mFormValue_General.getTypeface().equals("JOSEFINSANS")) {
            mTypeface = Typeface.createFromAsset(mContext.getResources().getAssets(), "fonts/JosefinSansRegular.ttf");
        } else if (mFormValue_General.getTypeface().equals("WHITNEYBOOK")) {
            mTypeface = Typeface.createFromAsset(mContext.getResources().getAssets(), "fonts/WhitneyBook.ttf");
        } else if (mFormValue_General.getTypeface().equals("CORBEL")) {
            mTypeface = Typeface.createFromAsset(mContext.getResources().getAssets(), "fonts/Corbel.ttf");
        }
    }


    public int getSection(int pos) {
        if (pos < mSectionPos.length) {
            return mSectionPos[pos];
        } else return 0;
    }


    protected Bitmap drawBitmap() {

        // Init Bitmap
        Bitmap result = Bitmap.createBitmap(dim.canvas_Width, dim.canvas_Height, Bitmap.Config.ARGB_8888);
        Canvas mCanvas = new Canvas(result);
        mCanvas.setDensity(mContext.getResources().getDisplayMetrics().densityDpi);


        Paint paint = new Paint();
        paint.setFlags(Paint.FILTER_BITMAP_FLAG | Paint.ANTI_ALIAS_FLAG | Paint.EMBEDDED_BITMAP_TEXT_FLAG | Paint.DITHER_FLAG | Paint.SUBPIXEL_TEXT_FLAG);

        Paint iconPaint = new Paint();
        iconPaint.setFlags(Paint.FILTER_BITMAP_FLAG | Paint.ANTI_ALIAS_FLAG);

        iconPaint.setColorFilter(new PorterDuffColorFilter(colors.get(3), PorterDuff.Mode.SRC_ATOP));

        //Draw Title

        drawTitleSection(mCanvas);

        //Draw BasicInformation

        drawBasicInformationSection(mCanvas);

        //Draw Experience & Skill
        drawExperienceSkillSection(mCanvas);

        //Draw ConnectSection
        drawConnectSection(mCanvas);
        //drawMeSomething(mCanvas);

        //Draw Footer
        drawFooterSection(mCanvas);

        result.reconfigure(dim.canvas_Width, (int) dim.gPositionY, Bitmap.Config.ARGB_8888);


        return result;

    }


    private void drawTitleSection(Canvas mCanvas) {

        //Draw Title

        Paint pForm = getFormPaint(colors.get(0));
        TextPaint pFont = getFontPaint(colors.get(1), dim.canvasFontSize_Large, TextPaint.Align.CENTER, mTypeface);

        mCanvas.drawRect(0, 0, dim.titleContainer_Width, dim.titleContainer_Height, pForm);


        try {
            mCanvas.drawText(mFormValue_BasicInformation.getName().length() == 0 ? fallBackString : mFormValue_BasicInformation.getName().toUpperCase(),
                    dim.titleContainer_Width / 2, dim.titleContainer_Height / 2 + dim.canvasFontSize_Large / 3, pFont);
        } catch (IndexOutOfBoundsException e) {
            mCanvas.drawText(fallBackString.toUpperCase(),
                    dim.titleContainer_Width / 2, dim.titleContainer_Height / 2 + dim.canvasFontSize_Large / 3, pFont);
            e.printStackTrace();
        }

        mSectionPos[0] = dim.titleContainer_Height / 2 - dim.overlayIcon_EditMode_Offset;

    }


    private Paint getFormPaint(int color) {
        Paint p = new Paint();
        p.setFlags(Paint.FILTER_BITMAP_FLAG | Paint.ANTI_ALIAS_FLAG | Paint.EMBEDDED_BITMAP_TEXT_FLAG | Paint.DITHER_FLAG | Paint.SUBPIXEL_TEXT_FLAG);
        p.setColor(color);
        return p;
    }

    private TextPaint getFontPaint(int color, float textSize, TextPaint.Align align, Typeface typeface) {
        TextPaint tP = new TextPaint();
        tP.setFlags(Paint.FILTER_BITMAP_FLAG | Paint.ANTI_ALIAS_FLAG | Paint.EMBEDDED_BITMAP_TEXT_FLAG | Paint.DITHER_FLAG | Paint.SUBPIXEL_TEXT_FLAG);
        tP.setTextSize(textSize);
        tP.setTextAlign(align);
        tP.setColor(color);
        tP.setTypeface(typeface);
        return tP;
    }

    private Paint getIconPaint(int color) {
        Paint pI = new Paint();
        pI.setFlags(Paint.FILTER_BITMAP_FLAG | Paint.ANTI_ALIAS_FLAG);
        pI.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP));
        return pI;
    }


    private void drawBasicInformationSection(Canvas mCanvas) {

        mSectionPos[1] = dim.titleContainer_Height + dim.basicInformationContainer_Height / 2 - dim.offsetBasicInformation_y;


        //Init Paint Objects
        Paint pForm = getFormPaint(colors.get(2));
        Paint pIcon = getIconPaint(colors.get(3));
        TextPaint pFont = getFontPaint(colors.get(3), dim.canvasFontSize_Medium, TextPaint.Align.LEFT, mTypeface);

        dim.basicInformationContainer_Height = (int) getBasicInformationContainerHeight(pFont, 300);

        mCanvas.drawRect(0, dim.titleContainer_Height, dim.basicInformationContainer_Width, dim.basicInformationContainer_Height, pForm);

        pForm.setColor(colors.get(3));
        pForm.setStrokeWidth(2);


        // Draw Avatar Image
        if (null != mFormValue_General.getAvatarImage()) {

            mCanvas.save();
            mCanvas.translate(dim.basicInformationContainer_Width * 0.25f - dim.basicInformationContainer_Avatar_Width / 2, dim.titleContainer_Height + dim.offsetSectionStart_y - dim.canvasFontSize_Small);

            Bitmap bmp = BitmapFactory.decodeFile(mFormValue_General.getAvatarImage());

            if (null == bmp) {
                bmp = ((BitmapDrawable) mContext.getResources().getDrawable(R.drawable.img_avatar_placeholder)).getBitmap();
            }

            Paint avatarPaint = new Paint();
            avatarPaint.setFlags(Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG);
            BitmapShader shader = new BitmapShader(bmp, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            avatarPaint.setShader(shader);

            int mBorderWidth = 10;

            int mBitmapHeight = dim.basicInformationContainer_Avatar_Width;
            int mBitmapWidth = dim.basicInformationContainer_Avatar_Height;

            Rect mBorderRect = new Rect();
            mBorderRect.set(0, 0, mBitmapWidth, mBitmapHeight);
            Rect mDrawableRect = new Rect();
            mDrawableRect.set(mBorderWidth, mBorderWidth, mBorderRect.width() - mBorderWidth, mBorderRect.height() - mBorderWidth);
            float mDrawableRadius = Math.min(mDrawableRect.height() / 2.0F, mDrawableRect.width() / 2.0F);

            float dx = 0.0F;
            float dy = 0.0F;

            Matrix mShaderMatrix = new Matrix();
            mShaderMatrix.set(null);
            float scale;

            if ((float) bmp.getWidth() * mDrawableRect.height() > mDrawableRect.width() * (float) bmp.getHeight()) {
                scale = mDrawableRect.height() / (float) bmp.getHeight();
                dx = (mDrawableRect.width() - (float) bmp.getWidth() * scale) * 0.5F;
            } else {
                scale = mDrawableRect.width() / (float) bmp.getWidth();
                dy = (mDrawableRect.height() - (float) bmp.getHeight() * scale) * 0.5F;
            }

            mShaderMatrix.setScale(scale, scale);
            mShaderMatrix.postTranslate((float) ((int) (dx + 0.5F) + mBorderWidth), (float) ((int) (dy + 0.5F) + mBorderWidth));
            shader.setLocalMatrix(mShaderMatrix);

            mCanvas.drawCircle(mDrawableRect.width() / 2, mDrawableRect.height() / 2, mDrawableRadius, avatarPaint);

            avatarPaint.reset();
            avatarPaint.setFlags(Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG);
            avatarPaint.setColor(colors.get(3));
            avatarPaint.setStyle(Paint.Style.STROKE);
            avatarPaint.setStrokeWidth(mBorderWidth);
            mCanvas.drawCircle(mDrawableRadius, mDrawableRadius, mDrawableRadius, avatarPaint);
            mCanvas.restore();
        }


        dim.gPositionY = dim.titleContainer_Height + dim.offsetSectionStart_y;
        //dim.gPositionY = dim.titleContainer_Height;

        StaticLayout mTextLayout = new StaticLayout("", pFont, 0, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);

        float lineEnd_x = getMaxTextSize(pFont, 300) + dim.centerVert * 1.5f;
        float line_offset_y = dim.canvasFontSize_Small / 2;
        float textPos_x = dim.centerVert + dim.generalIcon_Width_Small + 20;
        float iconpos_x = dim.centerVert;
        float iconpos_y = dim.gPositionY - dim.canvasFontSize_Small / 3 - dim.generalIcon_Height_Small / 2;


        Bitmap bm = Bitmap.createScaledBitmap(((BitmapDrawable) mContext.getResources().getDrawable(R.drawable.ic_general_name)).getBitmap(), dim.generalIcon_Width_Small, dim.generalIcon_Height_Small, false);
        mCanvas.drawBitmap(bm, iconpos_x, iconpos_y, pIcon);
        mCanvas.drawText(mContext.getResources().getString(R.string.fragment_formgenerator_basicinformation_name).toUpperCase(),
                textPos_x, dim.gPositionY, pFont);

        try {

            mCanvas.save();
            mCanvas.translate(dim.centerVert * 1.5f, iconpos_y);

            mTextLayout = new StaticLayout(mFormValue_BasicInformation.getName().length() == 0 ? fallBackString : mFormValue_BasicInformation.getName(),
                    pFont, 300, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            mTextLayout.draw(mCanvas);
            mCanvas.restore();

            mCanvas.drawLine(dim.centerVert,
                    dim.gPositionY + mTextLayout.getHeight() - line_offset_y,
                    lineEnd_x,
                    dim.gPositionY + mTextLayout.getHeight() - line_offset_y,
                    pForm);


        } catch (IndexOutOfBoundsException e) {
            mCanvas.drawText(fallBackString,
                    dim.centerVert * 1.5f, dim.gPositionY, pFont);
        }


        dim.gPositionY += dim.offsetBasicInformation_y + mTextLayout.getHeight();

        iconpos_y = dim.gPositionY - dim.canvasFontSize_Small / 3 - dim.generalIcon_Height_Small / 2;
        bm = Bitmap.createScaledBitmap(((BitmapDrawable) mContext.getResources().getDrawable(R.drawable.ic_general_profession)).getBitmap(), dim.generalIcon_Width_Small, dim.generalIcon_Height_Small, false);
        mCanvas.drawBitmap(bm, iconpos_x, iconpos_y, pIcon);
        mCanvas.drawText(mContext.getResources().getString(R.string.fragment_formgenerator_basicinformation_profession).toUpperCase(),
                textPos_x, dim.gPositionY, pFont);

        try {

            mCanvas.save();
            mCanvas.translate(dim.centerVert * 1.5f, iconpos_y);

            mTextLayout = new StaticLayout(mFormValue_BasicInformation.getProfession().length() == 0 ? fallBackString : mFormValue_BasicInformation.getProfession(),
                    pFont, 300, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            mTextLayout.draw(mCanvas);
            mCanvas.restore();

            mCanvas.drawLine(dim.centerVert,
                    dim.gPositionY + mTextLayout.getHeight() - line_offset_y,
                    lineEnd_x,
                    dim.gPositionY + mTextLayout.getHeight() - line_offset_y,
                    pForm);


        } catch (IndexOutOfBoundsException e) {
            mCanvas.drawText(fallBackString,
                    dim.centerVert * 1.5f, dim.gPositionY, pFont);
        }


        dim.gPositionY += dim.offsetBasicInformation_y + mTextLayout.getHeight();
        iconpos_y = dim.gPositionY - dim.canvasFontSize_Small / 3 - dim.generalIcon_Height_Small / 2;
        bm = Bitmap.createScaledBitmap(((BitmapDrawable) mContext.getResources().getDrawable(R.drawable.ic_general_born)).getBitmap(), dim.generalIcon_Width_Small, dim.generalIcon_Height_Small, false);
        mCanvas.drawBitmap(bm, iconpos_x, iconpos_y, pIcon);
        mCanvas.drawText(mContext.getResources().getString(R.string.fragment_formgenerator_basicinformation_born).toUpperCase(),
                textPos_x, dim.gPositionY, pFont);

        try {

            mCanvas.save();
            mCanvas.translate(dim.centerVert * 1.5f, iconpos_y);

            java.util.Date dateBorn = mSimpleDateFormatSource.parse(mFormValue_BasicInformation.getBorn().toString());
            mTextLayout = new StaticLayout(dateBorn == null ? fallBackString : mSimpleDateFormatTarget.format(dateBorn),
                    pFont, 300, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            mTextLayout.draw(mCanvas);
            mCanvas.restore();

            mCanvas.drawLine(dim.centerVert,
                    dim.gPositionY + mTextLayout.getHeight() - line_offset_y,
                    lineEnd_x,
                    dim.gPositionY + mTextLayout.getHeight() - line_offset_y,
                    pForm);


        } catch (IndexOutOfBoundsException e) {
            mCanvas.drawText(fallBackString,
                    dim.centerVert * 1.5f, dim.gPositionY, pFont);
        } catch (ParseException e) {
            mCanvas.drawText(mFormValue_BasicInformation.getBorn().length() == 0 ? fallBackString : mFormValue_BasicInformation.getBorn(),
                    dim.centerVert * 1.5f, dim.gPositionY, pFont);
        }


        dim.gPositionY += dim.offsetBasicInformation_y + mTextLayout.getHeight();

        iconpos_y = dim.gPositionY - dim.canvasFontSize_Small / 3 - dim.generalIcon_Height_Small / 2;
        bm = Bitmap.createScaledBitmap(((BitmapDrawable) mContext.getResources().getDrawable(R.drawable.ic_general_address)).getBitmap(), dim.generalIcon_Width_Small, dim.generalIcon_Height_Small, false);
        mCanvas.drawBitmap(bm, iconpos_x, iconpos_y, pIcon);

        mCanvas.drawText(mContext.getResources().getString(R.string.fragment_formgenerator_basicinformation_country).toUpperCase(),
                textPos_x, dim.gPositionY, pFont);

        try {

            mCanvas.save();
            mCanvas.translate(dim.centerVert * 1.5f, iconpos_y);

            mTextLayout = new StaticLayout(mFormValue_BasicInformation.getAddress().length() == 0 ? fallBackString : mFormValue_BasicInformation.getAddress(),
                    pFont, 300, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            mTextLayout.draw(mCanvas);
            mCanvas.restore();

            mCanvas.drawLine(dim.centerVert,
                    dim.gPositionY + mTextLayout.getHeight() - line_offset_y,
                    lineEnd_x,
                    dim.gPositionY + mTextLayout.getHeight() - line_offset_y,
                    pForm);
        } catch (IndexOutOfBoundsException e) {
            mCanvas.drawText(fallBackString,
                    dim.centerVert * 1.5f, dim.gPositionY, pFont);
        }


        dim.gPositionY += dim.offsetBasicInformation_y + mTextLayout.getHeight();
        iconpos_y = dim.gPositionY - dim.canvasFontSize_Small / 3 - dim.generalIcon_Height_Small / 2;
        bm = Bitmap.createScaledBitmap(((BitmapDrawable) mContext.getResources().getDrawable(R.drawable.ic_general_email)).getBitmap(), dim.generalIcon_Width_Small, dim.generalIcon_Height_Small, false);
        mCanvas.drawBitmap(bm, iconpos_x, iconpos_y, pIcon);
        mCanvas.drawText(mContext.getResources().getString(R.string.fragment_formgenerator_basicinformation_email).toUpperCase(),
                textPos_x, dim.gPositionY, pFont);

        try {

            mCanvas.save();
            mCanvas.translate(dim.centerVert * 1.5f, iconpos_y);

            mTextLayout = new StaticLayout(mFormValue_BasicInformation.getEmail().length() == 0 ? fallBackString : mFormValue_BasicInformation.getEmail(),
                    pFont, 300, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            mTextLayout.draw(mCanvas);
            mCanvas.restore();

            mCanvas.drawLine(dim.centerVert,
                    dim.gPositionY + mTextLayout.getHeight() - line_offset_y,
                    lineEnd_x,
                    dim.gPositionY + mTextLayout.getHeight() - line_offset_y,
                    pForm);
        } catch (IndexOutOfBoundsException e) {
            mCanvas.drawText(fallBackString,
                    dim.centerVert * 1.5f, dim.gPositionY, pFont);
        }


        dim.gPositionY += dim.offsetBasicInformation_y + mTextLayout.getHeight();
        iconpos_y = dim.gPositionY - dim.canvasFontSize_Small / 3 - dim.generalIcon_Height_Small / 2;
        bm = Bitmap.createScaledBitmap(((BitmapDrawable) mContext.getResources().getDrawable(R.drawable.ic_general_phone)).getBitmap(), dim.generalIcon_Width_Small, dim.generalIcon_Height_Small, false);
        mCanvas.drawBitmap(bm, iconpos_x, iconpos_y, pIcon);
        mCanvas.drawText(mContext.getResources().getString(R.string.fragment_formgenerator_basicinformation_phone).toUpperCase(),
                textPos_x, dim.gPositionY, pFont);

        try {

            mCanvas.save();
            mCanvas.translate(dim.centerVert * 1.5f, iconpos_y);

            mTextLayout = new StaticLayout(mFormValue_BasicInformation.getPhone().length() == 0 ? fallBackString : mFormValue_BasicInformation.getPhone(),
                    pFont, 300, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            mTextLayout.draw(mCanvas);
            mCanvas.restore();

            mCanvas.drawLine(dim.centerVert,
                    dim.gPositionY + mTextLayout.getHeight() - line_offset_y,
                    lineEnd_x,
                    dim.gPositionY + mTextLayout.getHeight() - line_offset_y,
                    pForm);
        } catch (IndexOutOfBoundsException e) {
            mCanvas.drawText(fallBackString,
                    dim.centerVert * 1.5f, dim.gPositionY, pFont);
        }
    }


    private void drawExperienceSkillSection(Canvas mCanvas) {

        int lHeight = 0;


        Paint pForm = getFormPaint(colors.get(4));
        Paint pIcon = getIconPaint(colors.get(5));
        TextPaint pFont = getFontPaint(colors.get(5), dim.canvasFontSize_Small, TextPaint.Align.LEFT, mTypeface);


        mCanvas.drawRect(0, dim.basicInformationContainer_Height, dim.experienceContainer_Width, dim.experienceContainer_Height, pForm);

        dim.gPositionY = dim.basicInformationContainer_Height + dim.offsetSectionStart_y;


        float textPos_x = dim.experienceContainer_Width * 0.1f;


        pFont.setTextSize(dim.canvasFontSize_Large);

        mCanvas.drawText(mContext.getResources().getString(R.string.fragment_form_experience_title).toUpperCase(),
                textPos_x, dim.gPositionY, pFont);
        textPos_x = dim.experienceContainer_Width * 0.15f;

        int offset_textPos_x = 300;
        mCanvas.save();

        mCanvas.translate(textPos_x, dim.gPositionY + dim.offset_y);

        dim.gPositionY += dim.offset_y;
        lHeight += dim.offset_y;

        int temp = 0;
        StaticLayout mTextLayout;
        pFont.setTextSize(dim.canvasFontSize_Medium);

        for (Form_Entries_Experience entry : mFormValues_Experience) {

            mCanvas.translate(0, temp);


            java.util.Date dateBeginn = null;
            try {
                dateBeginn = mSimpleDateFormatSource.parse(entry.getDateBegin());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            //java.util.Date dateBeginn = mSimpleDateFormatSource.parse(entry.getDateBegin());


            mTextLayout = new StaticLayout(dateBeginn == null ? entry.getDateBegin() : mSimpleDateFormatTarget.format(dateBeginn), pFont, dim.experienceContainer_Width * 80 / 100, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            mTextLayout.draw(mCanvas);

            mCanvas.translate(offset_textPos_x, 0);
            mTextLayout = new StaticLayout(entry.getProfession() + " / " + entry.getBorn(), pFont, dim.experienceContainer_Width * 70 / 100 - offset_textPos_x, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            mTextLayout.draw(mCanvas);

            if (!entry.getDescription().equals("")) {

                mCanvas.translate(0, mTextLayout.getHeight() + 25);
                dim.gPositionY += mTextLayout.getHeight() + 25;
                mTextLayout = new StaticLayout(entry.getDescription(), pFont, dim.experienceContainer_Width * 70 / 100 - offset_textPos_x, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
                mTextLayout.draw(mCanvas);

                temp = mTextLayout.getHeight() + dim.offset_y;
                dim.gPositionY += 25 + mTextLayout.getHeight() + dim.offset_y;
                lHeight += 25 + mTextLayout.getHeight() + dim.offset_y;


            } else {
                temp = mTextLayout.getHeight() + dim.offset_y;
                dim.gPositionY += temp;
                lHeight += temp;
            }

            mCanvas.translate(-offset_textPos_x, 0);
        }

        mCanvas.restore();

        mCanvas.save();

        dim.gPositionY += dim.offset_y * 2;
        lHeight += dim.offset_y * 2;


        pFont.setTextSize(dim.canvasFontSize_Large);
        mCanvas.translate(dim.experienceContainer_Width * 0.1f, dim.gPositionY);
        mCanvas.drawText(mContext.getResources().getString(R.string.fragment_form_skills_title).toUpperCase(),
                0, 0, pFont);


        pFont.setTextSize(dim.canvasFontSize_Medium);

        mCanvas.translate(dim.experienceContainer_Width * 0.15f - dim.experienceContainer_Width * 0.1f, dim.offset_y);

        dim.gPositionY += dim.offset_y;
        lHeight += dim.offset_y;

        String titleSubcategory = "";
        //pIcon.setColorFilter(new PorterDuffColorFilter(colors.get(5), PorterDuff.Mode.SRC_ATOP));


        for (Form_Entries_Skill entry : mFormValues_Skill) {


            if (!titleSubcategory.equals(entry.getCategory())) {

                pFont.setColor(colors.get(5));
                pFont.setTextSize(dim.canvasFontSize_Medium);
                mCanvas.translate(0, dim.offset_y / 2);
                mCanvas.drawText(entry.getCategory(),
                        0, 0, pFont);
                mCanvas.translate(0, dim.offset_y);
                dim.gPositionY += dim.offset_y * 1.5f;
                lHeight += dim.offset_y * 1.5f;

            }
            mCanvas.drawBitmap(optionToBitmap(entry.getSubCategory(), dim.generalIcon_Width_Medium, dim.generalIcon_Height_Medium), 0, -dim.generalIcon_Height_Medium, pIcon);

            pForm.setColor(colors.get(5));
            mCanvas.drawRect(dim.generalIcon_Width_Medium * 2, -dim.generalIcon_Height_Medium, (int) (entry.getRating()) * 200, 0, pForm);

            pFont.setTextSize(dim.canvasFontSize_Small);
            //
            pFont.setColor(colors.get(4));
            mCanvas.drawText(entry.getSubCategory(),
                    dim.generalIcon_Width_Medium * 2.5f, -dim.generalIcon_Height_Medium / 2 + dim.canvasFontSize_Small / 3, pFont);
            titleSubcategory = entry.getCategory();
            mCanvas.translate(0, dim.offset_y);
            dim.gPositionY += dim.offset_y;
            lHeight += dim.offset_y;
        }

        mSectionPos[2] = (int) dim.gPositionY - lHeight / 2 - dim.overlayIcon_EditMode_Offset;
    }


    public void drawConnectSection(Canvas mCanvas) {

        int lHeight = 0;

        Paint pForm = getFormPaint(colors.get(6));
        Paint pIcon = getIconPaint(colors.get(7));
        pIcon.setColorFilter(null);
        TextPaint pFont = getFontPaint(colors.get(7), dim.canvasFontSize_Medium, TextPaint.Align.LEFT, mTypeface);


        mCanvas.drawRect(0, dim.titleContainer_Height, dim.basicInformationContainer_Width, dim.basicInformationContainer_Height, pForm);
        mCanvas.translate(-dim.connectContainer_Width * 0.15f, dim.offset_y);

        dim.gPositionY += dim.offset_y;
        lHeight += dim.offset_y;

        mCanvas.drawRect(0, 0, dim.connectContainer_Width, dim.connectContainer_Height, pForm);
        mCanvas.translate(dim.connectContainer_Width * 0.1f, dim.offsetSectionStart_y);
        dim.gPositionY += dim.offsetSectionStart_y;


        pFont.setTextSize(dim.canvasFontSize_Medium);

        mCanvas.drawText(mContext.getResources().getString(R.string.fragment_form_connect_title).toUpperCase(),
                0, 0, pFont);

        mCanvas.translate(dim.connectContainer_Width * 0.4f, dim.offsetSectionStart_y / 2);
        dim.gPositionY += dim.offsetSectionStart_y;

        int i = 0;

        for (Form_Entries_Connect entry : mFormValues_Connect) {


            if (i % 2 == 0) {

                mCanvas.save();
                mCanvas.translate(-dim.connectContainer_Width * 0.25f - dim.generalIcon_Width_Large * 1.5f, 0);
                mCanvas.drawBitmap(optionToBitmap(entry.getOption(), dim.generalIcon_Width_Large, dim.generalIcon_Height_Large), 0, -dim.generalIcon_Height_Large, pIcon);
                mCanvas.restore();
                pFont.setTextAlign(Paint.Align.LEFT);
                mCanvas.drawText(entry.getUser(),
                        -dim.connectContainer_Width * 0.25f, -dim.generalIcon_Height_Large / 2 + dim.canvasFontSize_Medium / 3, pFont);
            } else {

                mCanvas.save();
                mCanvas.translate(dim.connectContainer_Width * 0.25f + dim.generalIcon_Width_Large * 0.5f, 0);
                mCanvas.drawBitmap(optionToBitmap(entry.getOption(), dim.generalIcon_Width_Large, dim.generalIcon_Height_Large), 0, -dim.generalIcon_Height_Large, pIcon);
                mCanvas.restore();
                pFont.setTextAlign(Paint.Align.RIGHT);
                mCanvas.drawText(entry.getUser(),
                        dim.connectContainer_Width * 0.25f, -dim.generalIcon_Height_Large / 2 + dim.canvasFontSize_Medium / 3, pFont);

                mCanvas.translate(0, dim.offset_y);
                dim.gPositionY += dim.offset_y;
                lHeight += dim.offset_y;
            }

            i++;
        }


        mSectionPos[3] = (int) dim.gPositionY - lHeight / 2 - dim.overlayIcon_EditMode_Offset;


    }


    private void drawFooterSection(Canvas mCanvas) {

        Paint pForm = new Paint();
        pForm.setFlags(Paint.FILTER_BITMAP_FLAG | Paint.ANTI_ALIAS_FLAG | Paint.EMBEDDED_BITMAP_TEXT_FLAG | Paint.DITHER_FLAG | Paint.SUBPIXEL_TEXT_FLAG);
        pForm.setColor(colors.get(8));
        mCanvas.translate(-dim.footerContainer_Width * 0.5f, dim.offsetSectionStart_y);
        dim.gPositionY += dim.offset_y + dim.footerContainer_Height;

        mSectionPos[4] = (int) dim.gPositionY - dim.offset_y + dim.footerContainer_Height / 2;

        mCanvas.drawRect(0, 0, dim.footerContainer_Width, dim.footerContainer_Height, pForm);
    }


    private float getBasicInformationContainerHeight(TextPaint p, int yMax) {

        float res = dim.offsetSectionStart_y * 1.5f;
        //float res = 0;

        try {
            String[] arr = {mFormValue_BasicInformation.getName(),
                    mFormValue_BasicInformation.getProfession(),
                    mFormValue_BasicInformation.getBorn(),
                    mFormValue_BasicInformation.getAddress(),
                    mFormValue_BasicInformation.getEmail(),
                    mFormValue_BasicInformation.getPhone()};

            for (int i = 0; i < arr.length; i++) {

                StaticLayout st = new StaticLayout(arr[i],
                        p, yMax, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);

                res += st.getHeight() + dim.offsetBasicInformation_y * 1.2;

            }

            Log.v("RESULT", String.valueOf(res));

        } catch (IndexOutOfBoundsException e) {

        }


        return res;
    }


    private float getMaxTextSize(TextPaint p, int yMax) {


        float res = p.measureText(fallBackString);

        try {
            String[] arr = {mFormValue_BasicInformation.getName(),
                    mFormValue_BasicInformation.getProfession(),
                    mFormValue_BasicInformation.getBorn(),
                    mFormValue_BasicInformation.getAddress(),
                    mFormValue_BasicInformation.getEmail(),
                    mFormValue_BasicInformation.getPhone()};

            for (int i = 0; i < arr.length; i++) {

                StaticLayout st = new StaticLayout(arr[i],
                        p, yMax, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);

                if (res < p.measureText(arr[i])) {
                    res = st.getWidth();
                }
            }

            Log.v("RESULT", String.valueOf(res));

        } catch (IndexOutOfBoundsException e) {

        }


        return res;
    }


    private Bitmap optionToBitmap(String option, int iconWidth, int iconHeight) {
        int resId;

        switch (option) {

            //Icons Social Connect

            case "Facebook":
                resId = R.drawable.ic_social_facebook;
                break;
            case "Twitter":
                resId = R.drawable.ic_social_twitter;
                break;
            case "Printerest":
                resId = R.drawable.ic_social_printerest;
                break;
            case "Youtube":
                resId = R.drawable.ic_social_youtube;
                break;
            case "Google+":
                resId = R.drawable.ic_social_google;
                break;
            case "LinkedIn":
                resId = R.drawable.ic_social_linkedin;
                break;
            case "Wordpress":
                resId = R.drawable.ic_social_wordpress;
                break;
            case "Email":
                resId = R.drawable.ic_social_email;
                break;

            case "StackExchange":
                resId = R.drawable.ic_social_stackexchange;
                break;

            case "GitHub":
                resId = R.drawable.ic_social_github;
                break;


            //Icons Skill General

            case "Microsoft Office":
                resId = R.drawable.ic_general_msoffice;
                break;


            //Icons Skill Development

            case "C/C++":
                resId = R.drawable.ic_skill_development_cpp;
                break;
            case "CSharp":
                resId = R.drawable.ic_skill_development_csharp;
                break;

            case "HTML/CSS":
                resId = R.drawable.ic_skill_development_html;
                break;

            case "HTML 5":
                resId = R.drawable.ic_skill_development_html5;
                break;

            case "JavaScript":
                resId = R.drawable.ic_skill_development_javascript;
                break;

            case "PHP":
                resId = R.drawable.ic_skill_development_php;
                break;

            case "Phyton":
                resId = R.drawable.ic_skill_development_phyton;
                break;

            case "Ruby":
                resId = R.drawable.ic_skill_development_ruby;
                break;

            case "SQL":
                resId = R.drawable.ic_skill_development_sql;
                break;

            case "XML":
                resId = R.drawable.ic_skill_development_xml;
                break;

            //Icons Skill Design

            case "Blender":
                resId = R.drawable.ic_design_blender;
                break;

            case "InDesign":
                resId = R.drawable.ic_design_id;
                break;

            case "Fireworks":
                resId = R.drawable.ic_design_fw;
                break;

            case "Photoshop":
                resId = R.drawable.ic_design_ps;
                break;

            case "Illustrator":
                resId = R.drawable.ic_design_ai;
                break;

            case "AfterEffects":
                resId = R.drawable.ic_design_ae;
                break;

            default:
                resId = R.drawable.ic_social_facebook;
                break;
        }


        //Bitmap.createScaledBitmap(((BitmapDrawable) context.getResources().getDrawable(resId)).drawBitmap(), dim.generalIcon_Width_Small, dim.generalIcon_Height_Small, false);

        return Bitmap.createScaledBitmap(((BitmapDrawable) mContext.getResources().getDrawable(resId)).getBitmap(), iconWidth, iconHeight, false);
    }


}



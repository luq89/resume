package com.example.lukas.resume;


import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.andexert.expandablelayout.library.ExpandableLayout;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.example.lukas.resume.adapter.CustomPagerAdapter;
import com.example.lukas.resume.datamanagement.Form_Entries_General;
import com.example.lukas.resume.datamanagement.SQLiteHelper;
import com.example.lukas.resume.datamanagement.UserDataHub;
import com.example.lukas.resume.floatingactionbutton.FloatingActionButton;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import me.relex.circleindicator.CircleIndicator;


public class FormFragment extends android.support.v4.app.DialogFragment implements FormGeneratorFragment_Dialog_ColorPicker.OnFragmentInteractionListener {


    @Override
    public void onFormColorSelected(int color) {


        /*

        mViewHolderForm.mColorFields.get(id * 2 + target).setBackground(changeColorOfDrawable(color));
        mViewHolderForm.mColorFields.get(id * 2 + target).setTag(new ColorTag(id, target, color));
        ImageView iv = (ImageView) mViewHolderForm.mColorFields.get(id * 2 + target).getChildAt(0);
        iv.setColorFilter(getContrastColor(color), PorterDuff.Mode.SRC_ATOP);

        //mViewHolderForm.mViewDrawForm.setFormColor(id, getResources().getColor(R.color.Tint4));
        mViewHolderForm.mViewDrawForm.colors.set(id * 2 + target, color);
        setBitmap(mViewHolderForm.mViewDrawForm.drawBitmap());
        mViewHolderForm.mInitialColors.set(id * 2 + target, color);
        */
        mViewHolderForm.fab_highlightColor.setBackgroundColor(color);
        mViewHolderForm.mViewDrawForm.setHighlightColor(color);
        updateColorHistory(color);
        updateDatabase(color);
        setBitmap(mViewHolderForm.mViewDrawForm.drawBitmap());
        mViewHolderForm.relativeLayout_editPanel.setVisibility(View.GONE);


    }

    private void updateColorHistory(int color) {

        // Update Color History
        if (!mViewHolderForm.mColorHistory.contains(color)) {

            for (int j = mViewHolderForm.mColorHistory.size() - 1; j > 0; j--) {
                mViewHolderForm.mColorHistory.add(j, mViewHolderForm.mColorHistory.get(j - 1));
                mViewHolderForm.mColorHistory.remove(j - 1);
            }
            mViewHolderForm.mColorHistory.add(0, color);
        }

        if (mViewHolderForm.mColorHistory.size() > COLOR_HISTORY_STACK_SIZE) {
            while (mViewHolderForm.mColorHistory.size() > COLOR_HISTORY_STACK_SIZE) {
                mViewHolderForm.mColorHistory.remove(mViewHolderForm.mColorHistory.size() - 1);
            }
        }
    }

    private void updateDatabase(int color) {



        /*

        // Update Database
        String colorTheme = "";

        for (int k = 0; k < COLOR_SECTIONS_COUNT * 2; k++) {
            colorTheme += (String.format("#%06X", (0xFFFFFF & mViewHolderForm.mInitialColors.get(k))) + ";");
        }

        */

        ContentValues mContentValues = new ContentValues();
        mContentValues.put(SQLiteHelper.COLUMN_GENERAL_COLORTHEME, String.valueOf(color));
        UserDataHub.update(SQLiteHelper.TABLE_GENERAL, mContentValues);

    }


    private class ViewHolderForm {

        TextView textView_Dialog_title;
        LinearLayout linearLayout_containerImageViewExport;
        LinearLayout linearLayout_containerImageViewDismiss;
        FloatingActionButton fab_Export;

        LinearLayout linearLayout_container_EditMode;
        FloatingActionButton fab_EditMode;
        FloatingActionButton fab_Settings;
        //FloatingActionButton fab_ColorTheme;
        ScrollView mScrollview;
        SlidingUpPanelLayout mSlidingUpPanelLayout;


        int[] mPagerResources;
        ViewPager mPager;
        CircleIndicator mCircleIndicator;

        ExpandableLayout mExpandableLayout_fontStyle;
        LinearLayout mExpandableLayout_fontStyleLlContent;
        ExpandableLayout mExpandableLayout_dateFormat;
        ScrollView mExpandableLayout_dateFormatScrollView;
        LinearLayout mExpandableLayout_dateFormatLlContent;


        RelativeLayout relativeLayout_editPanel;
        FloatingActionButton fab_highlightColor;
        String settings_DateFormat;

        String settings_FontStyle;


        SubsamplingScaleImageView ssImageView;
        TextView textView_Title;

        private ArrayList<Integer> mColorHistory;
        private ArrayList<Integer> mInitialColors;
        private ArrayList<RelativeLayout> mColorFields;
        FormGeneratorFragment_Dialog_ColorPicker mDialog_ColorPicker;
        //FormGeneratorFragment_Dialog_ColorTheme mDialog_ColorTheme;
        ViewDrawForm mViewDrawForm;
        private boolean inEditMode = false;

    }

    ViewHolderForm mViewHolderForm;
    private Form_Entries_General mFormValue_General;

    OnFragmentInteractionListener mCallback;

    //private int COLOR_SECTIONS_COUNT = 5;
    private int COLOR_HISTORY_STACK_SIZE = 5;
    //private int COLOR_SECTIONS_COUNT = 5;

    public static FormFragment newInstance() {

        FormFragment fragment = new FormFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        return fragment;
    }


    public FormFragment() {

    }

    String string_CachePath = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {


            Bundle bundle = getArguments();
            if (bundle.containsKey("CachePath")) {
                string_CachePath = bundle.getString("CachePath");
            }
        }
        mViewHolderForm = new ViewHolderForm();
        mViewHolderForm.mColorHistory = new ArrayList<>();
        mViewHolderForm.mColorFields = new ArrayList<>();
        mViewHolderForm.mInitialColors = new ArrayList<>();


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment


        View rootView = inflater.inflate(R.layout.fragment_form_preview, container, false);

        mViewHolderForm.textView_Title = (TextView) rootView.findViewById(R.id.fragment_form_dialog_title);

        mViewHolderForm.linearLayout_containerImageViewDismiss = (LinearLayout) rootView.findViewById(R.id.fragment_form_container_imageview_dismiss);

        mViewHolderForm.linearLayout_containerImageViewDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        mViewHolderForm.linearLayout_containerImageViewExport = (LinearLayout) rootView.findViewById(R.id.fragment_form_container_linearlayout_export);


        if (!string_CachePath.equals("")) {

            mViewHolderForm.textView_Dialog_title = (TextView) rootView.findViewById(R.id.fragment_form_dialog_title);
            mViewHolderForm.textView_Dialog_title.setText("Export");

            mViewHolderForm.fab_Export = (FloatingActionButton) rootView.findViewById(R.id.fragment_form_fab_export);
            mViewHolderForm.fab_Export.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        export();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            mViewHolderForm.linearLayout_containerImageViewExport.setVisibility(View.GONE);
        }


        mViewHolderForm.mViewDrawForm = new ViewDrawForm(getActivity());

        mFormValue_General = UserDataHub.getGeneralData();

        /*

        String sColors = mFormValue_General.getColorTheme();

        String[] sColorsArr = sColors.split(";");
        for (int k = 0; k < sColorsArr.length; k++) {
            mViewHolderForm.mInitialColors.add(Color.parseColor(sColorsArr[k]));

            if (!mViewHolderForm.mColorHistory.contains(Color.parseColor(sColorsArr[k]))) {
                mViewHolderForm.mColorHistory.add(Color.parseColor(sColorsArr[k]));
            }
        }
        */


        //mViewHolderForm.mViewDrawForm.setColors(mViewHolderForm.mInitialColors);
        mViewHolderForm.relativeLayout_editPanel = (RelativeLayout) rootView.findViewById(R.id.fragment_form_container_editpanel);
        mViewHolderForm.fab_highlightColor = (FloatingActionButton) rootView.findViewById(R.id.fragment_form_fab_highlightcolor);


        mViewHolderForm.mSlidingUpPanelLayout = (SlidingUpPanelLayout) rootView.findViewById(R.id.fragment_form_slidinglayout);
        mViewHolderForm.mScrollview = (ScrollView) rootView.findViewById(R.id.fragment_form_scrollView);

        mViewHolderForm.mScrollview.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                updateEditPanel(mViewHolderForm.mScrollview.getScrollY());
            }
        });


        mViewHolderForm.mSlidingUpPanelLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View view, float v) {
                mViewHolderForm.fab_EditMode.setVisibility(View.GONE);
            }

            @Override
            public void onPanelCollapsed(View view) {
                mViewHolderForm.fab_Settings.setChecked(false);
                mViewHolderForm.ssImageView.invalidate();
                mViewHolderForm.fab_EditMode.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPanelExpanded(View view) {

            }

            @Override
            public void onPanelAnchored(View view) {

            }

            @Override
            public void onPanelHidden(View view) {

            }
        });


        //initEditMode(mViewHolderForm.mScrollview.getScrollY());
        //mViewHolderForm.relativeLayout_editPanel.setVisibility(View.GONE);
        //mViewHolderForm.linearLayout_container_EditMode = (LinearLayout) rootView.findViewById(R.id.fragment_form_linearlayout_colortheme);

        mViewHolderForm.fab_EditMode = (FloatingActionButton) rootView.findViewById(R.id.fragment_form_fab_editmode);

        mViewHolderForm.fab_EditMode.setOnCheckedChangeListener(new FloatingActionButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(FloatingActionButton fabView, boolean isChecked) {

                if (isChecked) {
                    //mViewHolderForm.fab_ColorTheme.setVisibility(View.VISIBLE);
                    mViewHolderForm.fab_Settings.setChecked(false);
                    mViewHolderForm.inEditMode = true;
                    mViewHolderForm.ssImageView.resetScaleAndCenter();
                    mViewHolderForm.ssImageView.setPanEnabled(false);
                    mViewHolderForm.ssImageView.setEnabled(false);
                    mViewHolderForm.ssImageView.setZoomEnabled(false);
                    initEditMode(mViewHolderForm.mScrollview.getScrollY());
                } else {
                    //mViewHolderForm.fab_ColorTheme.setVisibility(View.GONE);
                    mViewHolderForm.inEditMode = false;
                    mViewHolderForm.ssImageView.setPanEnabled(true);
                    mViewHolderForm.ssImageView.setPanEnabled(true);
                    mViewHolderForm.ssImageView.setEnabled(true);
                    mViewHolderForm.ssImageView.setZoomEnabled(true);
                    //mViewHolderForm.relativeLayout_editPanel.removeAllViews();
                }
            }
        });


        mViewHolderForm.fab_Settings = (FloatingActionButton) rootView.findViewById(R.id.fragment_form_fab_settings);
        mViewHolderForm.fab_Settings.setOnCheckedChangeListener(new FloatingActionButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(FloatingActionButton fabView, boolean isChecked) {

                if (isChecked) {
                    mViewHolderForm.fab_EditMode.setChecked(false);
                    mViewHolderForm.relativeLayout_editPanel.removeAllViews();
                    mViewHolderForm.mSlidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                } else {
                    mViewHolderForm.mSlidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
            }
        });



        /*

        mViewHolderForm.fab_ColorTheme = (FloatingActionButton) rootView.findViewById(R.id.fragment_form_fab_colortheme);
        mViewHolderForm.fab_ColorTheme.setVisibility(View.GONE);
        mViewHolderForm.fab_ColorTheme.setOnClickListenerForSaveAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mViewHolderForm.mDialog_ColorTheme = FormGeneratorFragment_Dialog_ColorTheme.newInstance(new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.color_theme_array))));
                mViewHolderForm.mDialog_ColorTheme.setTargetFragment(FormFragment.this, 0);
                //mViewHolderForm.mDialog_ColorTheme.setTargetFragment(ContentFragment, 0);
                mViewHolderForm.mDialog_ColorTheme.show(getFragmentManager(), "dialog");


            }
        });
        */
        mViewHolderForm.ssImageView = (SubsamplingScaleImageView) rootView.findViewById(R.id.fragment_form_container_subsamplingscaleimageview);


        mViewHolderForm.mPagerResources = new int[]{
                R.drawable.ic_social_wordpress,
                R.drawable.ic_social_linkedin,
                R.drawable.ic_social_youtube
        };


        mViewHolderForm.mPager = (ViewPager) rootView.findViewById(R.id.fragment_form_pager);
        mViewHolderForm.mPager.setAdapter(new CustomPagerAdapter(getContext(), mViewHolderForm.mPagerResources));
        mViewHolderForm.mCircleIndicator = (CircleIndicator) rootView.findViewById(R.id.fragment_form_circleIndicator);
        mViewHolderForm.mCircleIndicator.setViewPager(mViewHolderForm.mPager);


        mViewHolderForm.mExpandableLayout_fontStyle = (ExpandableLayout) rootView.findViewById(R.id.fragment_form_expandablelayout_fontstyle);
        mViewHolderForm.mExpandableLayout_fontStyleLlContent = (LinearLayout) mViewHolderForm.mExpandableLayout_fontStyle.getContentLayout().findViewById(R.id.welcomeDialog_fonstylecontentcontainer);
        initFontstyle();

        mViewHolderForm.mExpandableLayout_dateFormat = (ExpandableLayout) rootView.findViewById(R.id.fragment_form_expandablelayout_dateformat);
        mViewHolderForm.mExpandableLayout_dateFormatScrollView = (ScrollView) mViewHolderForm.mExpandableLayout_dateFormat.getContentLayout().findViewById(R.id.view_dateformat_content_scollview);
        initDateFormat();
        setBitmap(mViewHolderForm.mViewDrawForm.drawBitmap());


        return rootView;
    }


    private void initEditMode(int offset) {


        //mViewHolderForm.relativeLayout_editPanel.removeAllViews();

        mViewHolderForm.relativeLayout_editPanel.setVisibility(View.VISIBLE);
        mViewHolderForm.relativeLayout_editPanel.setBackgroundColor(getResources().getColor(R.color.Tint1));
        mViewHolderForm.relativeLayout_editPanel.getBackground().setAlpha(100);
        mViewHolderForm.relativeLayout_editPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewHolderForm.relativeLayout_editPanel.setVisibility(View.GONE);
            }
        });


        mViewHolderForm.fab_highlightColor.setBackgroundColor(mViewHolderForm.mViewDrawForm.getHighlightColor());


        mViewHolderForm.fab_highlightColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mViewHolderForm.mDialog_ColorPicker = FormGeneratorFragment_Dialog_ColorPicker.newInstance(mViewHolderForm.mViewDrawForm.getHighlightColor(), mViewHolderForm.mColorHistory);
                mViewHolderForm.mDialog_ColorPicker.setTargetFragment(FormFragment.this, 0);
                mViewHolderForm.mDialog_ColorPicker.show(getFragmentManager(), "dialog");
            }
        });



        /*

        mViewHolderForm.relativeLayout_editPanel.removeAllViews();
        mViewHolderForm.mColorFields.clear();


        RelativeLayout rl_background = new RelativeLayout(getActivity());
        RelativeLayout.LayoutParams lp_background = new RelativeLayout.LayoutParams(
                200, RelativeLayout.LayoutParams.MATCH_PARENT);
        //rl_background.setElevation(getResources().getDimension(R.dimen.fab_elevation_small));

        rl_background.setBackgroundColor(getResources().getColor(R.color.Tint1));
        rl_background.getBackground().setAlpha(200);

        //rl_background.setElevation(getResources().getDimension(R.dimen.fab_elevation));
        mViewHolderForm.relativeLayout_editPanel.addView(rl_background, lp_background);


        //int[] aPos = mViewHolderForm.mViewDrawForm.getSectionsPos();


        for (int i = 0; i < COLOR_SECTIONS_COUNT; i++) {
            //for (int i = 0; i < 2; i++) {

            RelativeLayout rl_form = new RelativeLayout(getActivity());
            RelativeLayout rl_font = new RelativeLayout(getActivity());


            //rl_form.setElevation(getResources().getDimension(R.dimen.fab_elevation));
            //rl_font.setElevation(getResources().getDimension(R.dimen.fab_elevation));


            RelativeLayout.LayoutParams lp_form = new RelativeLayout.LayoutParams(
                    60, 60);

            //lp_form.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
            //lp_form.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);


            RelativeLayout.LayoutParams lp_font = new RelativeLayout.LayoutParams(
                    60, 60);


            Drawable d_form = getActivity().getResources().getDrawable(R.drawable.helper_roundish_square_small);
            d_form.setTint(mViewHolderForm.mInitialColors.get(i * 2));
            ImageView iv_form = new ImageView(getContext());
            ViewGroup.LayoutParams lp_iv = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


            iv_form.setColorFilter(getContrastColor(mViewHolderForm.mInitialColors.get(i * 2)), PorterDuff.Mode.SRC_ATOP);
            Drawable d_font = getActivity().getResources().getDrawable(R.drawable.helper_roundish_square_small);
            d_font.setTint(mViewHolderForm.mInitialColors.get(i * 2 + 1));

            iv_form.setImageDrawable(getResources().getDrawable(R.drawable.ic_colorbucket));
            rl_form.setBackground(d_form);
            rl_form.addView(iv_form);

            ImageView iv_font = new ImageView(getContext());
            iv_font.setImageDrawable(getResources().getDrawable(R.drawable.ic_fontstyle));
            iv_font.setColorFilter(getContrastColor(mViewHolderForm.mInitialColors.get(i * 2 + 1)), PorterDuff.Mode.SRC_ATOP);
            rl_font.setBackground(d_font);
            rl_font.addView(iv_font, lp_iv);

            lp_form.topMargin = aPos[i] - offset;
            lp_form.leftMargin = 25;

            lp_font.topMargin = aPos[i] - offset;
            lp_font.leftMargin = 110;

            rl_form.setTag(new ColorTag(i, 0, mViewHolderForm.mInitialColors.get(i * 2)));

            //mViewHolderForm.linearLayout_wrapper_colorsForm.addView(rl_form, lp_form);

            rl_font.setTag(new ColorTag(i, 1, mViewHolderForm.mInitialColors.get(i * 2 + 1)));
            //mViewHolderForm.linearLayout_wrapper_colorsFont.addView(rl_font, lp_font);


            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ColorTag tag = (ColorTag) v.getTag();
                    Log.v("LISTENER", String.valueOf(tag.id) + ", " + String.valueOf(tag.target) + ", " + String.valueOf(tag.initialColor));

                    mViewHolderForm.mDialog_ColorPicker = FormGeneratorFragment_Dialog_ColorPicker.newInstance(tag.id, tag.target, tag.initialColor, mViewHolderForm.mColorHistory);
                    mViewHolderForm.mDialog_ColorPicker.setTargetFragment(FormFragment.this, 0);
                    mViewHolderForm.mDialog_ColorPicker.show(getFragmentManager(), "dialog");
                }
            };

            rl_form.setOnClickListenerForSaveAction(listener);
            rl_font.setOnClickListenerForSaveAction(listener);

            mViewHolderForm.mColorFields.add(rl_form);
            mViewHolderForm.mColorFields.add(rl_font);

            mViewHolderForm.relativeLayout_editPanel.addView(rl_form, lp_form);
            mViewHolderForm.relativeLayout_editPanel.addView(rl_font, lp_font);

        }

        */
    }


    public static int getComplementaryColor(int colorToInvert) {
        float[] hsv = new float[3];
        Color.RGBToHSV(Color.red(colorToInvert), Color.green(colorToInvert),
                Color.blue(colorToInvert), hsv);
        hsv[0] = (hsv[0] + 180) % 360;
        return Color.HSVToColor(hsv);
    }


    public int getContrastColor(int color) {
        int d = 0;

        // Counting the perceptive luminance - human eye favors green color...
        double a = 1 - (0.299 * Color.red(color) + 0.587 * Color.green(color) + 0.114 * Color.blue(color)) / 255;

        if (a < 0.5) {
            d = getResources().getColor(R.color.Font_Dark);
        } else {
            d = getResources().getColor(R.color.Font_Light);
        }

        return d;
    }


    private void updateEditPanel(int offset) {


    }


    private void setBitmap(Bitmap bmp) {
        mViewHolderForm.ssImageView.setPanLimit(SubsamplingScaleImageView.PAN_LIMIT_INSIDE);
        mViewHolderForm.ssImageView.setMinimumScaleType(SubsamplingScaleImageView.SCALE_TYPE_CENTER_INSIDE);
        mViewHolderForm.ssImageView.setImage(ImageSource.bitmap(bmp));

    }


    private int categoryToColor(String cat) {

        int color;
        Log.v("CAT", cat);

        switch (cat) {
            case "General":
                color = getActivity().getResources().getColor(R.color.chart_color_1);
                break;
            case "Development":
                color = getActivity().getResources().getColor(R.color.chart_color_2);
                break;
            case "Design":
                color = getActivity().getResources().getColor(R.color.chart_color_3);
                break;

            default:
                color = Color.WHITE;
        }

        return color;
    }


    public void export() throws IOException {

        try {


            Bitmap result = mViewHolderForm.mViewDrawForm.drawBitmap();


            File cachePath = new File(string_CachePath, "images");
            if (!cachePath.exists()) {
                cachePath.mkdirs(); // don't forget to make the directory android:textSize="@dimen/abc_text_size_medium_material"
            }

            String fname = "img_" + (new Date()).getTime() + ".png";


            FileOutputStream stream = new FileOutputStream(cachePath + "/" + fname); // overwrites this image every time
            result.compress(Bitmap.CompressFormat.PNG, 100, stream);
            stream.close();

            File newFile = new File(cachePath.getPath(), fname);

            Log.v("sadfaf", newFile.getPath());
            Uri contentUri = FileProvider.getUriForFile(getActivity(), "com.example.lukas.masterthesis.fileprovider", newFile);

            if (contentUri != null) {

                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); // showDialog permission for receiving app to read this file
                shareIntent.setDataAndType(contentUri, getActivity().getContentResolver().getType(contentUri));
                shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
                startActivity(Intent.createChooser(shareIntent, "Choose an app"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public int show(FragmentTransaction transaction, String tag) {
        return super.show(transaction, tag);
    }


    public class ColorTag {

        int initialColor;
        int id;
        int target;

        public ColorTag(int i, int tar, int col) {
            id = i;
            initialColor = col;
            target = tar;
        }
    }

    private Drawable changeColorOfDrawable(int color) {
        Drawable drawable = getActivity().getResources().getDrawable(R.drawable.helper_roundish_square_small);
        drawable.setTint(color);
        return drawable;
    }

    public interface OnFragmentInteractionListener {
        void onDismissPreview();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        mViewHolderForm.ssImageView.recycle();
    }

    @Override
    public void dismiss() {
        super.dismiss();
        mViewHolderForm.ssImageView.recycle();

    }


    public void onDateFormatSelected(String dateFormat) {


        ContentValues mContentValues = new ContentValues();
        mContentValues.clear();
        mContentValues.put(SQLiteHelper.COLUMN_GENERAL_DATEFORMAT, dateFormat);
        UserDataHub.update(SQLiteHelper.TABLE_GENERAL, mContentValues);

        mViewHolderForm.mViewDrawForm.initDataFields();
        setBitmap(mViewHolderForm.mViewDrawForm.drawBitmap());
    }


    public void onFontStyleSelected(String fontStyle) {

        ContentValues mContentValues = new ContentValues();
        mContentValues.clear();

        if (null != fontStyle && "" != fontStyle) {
            mContentValues.put(SQLiteHelper.COLUMN_GENERAL_TYPEFACE, fontStyle);
        }

        if (mContentValues.size() > 0) {
            UserDataHub.update(SQLiteHelper.TABLE_GENERAL, mContentValues);
            UserDataHub.replace(SQLiteHelper.TABLE_GENERAL, SQLiteHelper
                    .COLUMN_GENERAL_TYPEFACE, mContentValues);
        }

        mViewHolderForm.mViewDrawForm.initDataFields();
        setBitmap(mViewHolderForm.mViewDrawForm.drawBitmap());
    }


    private void initDateFormat() {

        mViewHolderForm.mExpandableLayout_dateFormatLlContent = new LinearLayout(getContext());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mViewHolderForm.mExpandableLayout_dateFormatLlContent.setOrientation(LinearLayout.VERTICAL);

        mViewHolderForm.mExpandableLayout_dateFormat.getHeaderLayout().addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (mViewHolderForm.mExpandableLayout_dateFormat.isOpened()) {
                    ((ImageView) v.findViewById(R.id.view_dateformat_header_icon)).setImageDrawable(getResources().getDrawable(R.drawable.ic_expand_up));

                } else {
                    ((ImageView) v.findViewById(R.id.view_dateformat_header_icon)).setImageDrawable(getResources().getDrawable(R.drawable.ic_expand_down));
                }
            }
        });


        final TextView dateFormatTitle = ((TextView) mViewHolderForm.mExpandableLayout_dateFormat.getHeaderLayout().findViewById(R.id.view_dateformat_header_title));
        dateFormatTitle.setText(mFormValue_General.getDateFormat());

        int count = 0;
        for (String s : getResources().getStringArray(R.array.settings_array_dateformat)) {

            RelativeLayout ll_row = new RelativeLayout(getContext());
            RelativeLayout.LayoutParams lp_row = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            ll_row.setPadding(50, 25, 25, 25);


            LinearLayout ll_textContainer = new LinearLayout(getContext());
            LinearLayout.LayoutParams lp_llContainer = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);


            TextView tV = new TextView(getContext());
            tV.setTextAppearance(getContext().getApplicationContext(), R.style.TextAppearance_AppCompat_Medium);
            LinearLayout.LayoutParams lp_tv = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);


            tV.setTextColor(s.equals(mFormValue_General.getDateFormat()) ?
                    getContext().getResources().getColor(R.color.Font_Light) : getContext().getResources().getColor(R.color.Font_Dark));
            tV.setText(s);


            TextView tVPreview = new TextView(getContext());
            tVPreview.setTextAppearance(getContext().getApplicationContext(), R.style.TextAppearance_AppCompat_Small);
            LinearLayout.LayoutParams lp_tvPreview = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lp_tvPreview.leftMargin = 50;


            Calendar date = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat(s, Locale.GERMAN);


            tVPreview.setTextColor(s.equals(mFormValue_General.getDateFormat()) ?
                    getContext().getResources().getColor(R.color.Font_Light) : getContext().getResources().getColor(R.color.Font_Tint));
            tVPreview.setText("// " + df.format(date.getTime()));


            ImageView iV_check = new ImageView(getContext());
            iV_check.setImageBitmap(Bitmap.createScaledBitmap(((BitmapDrawable) getContext().getResources().getDrawable(R.drawable.ic_tick)).getBitmap(), getContext().getResources().getDimensionPixelOffset(R.dimen.fab_icon_size), getContext().getResources().getDimensionPixelOffset(R.dimen.fab_icon_size), false));
            iV_check.setColorFilter(getContext().getResources().getColor(R.color.White), PorterDuff.Mode.SRC_ATOP);
            iV_check.setVisibility(s.equals(mFormValue_General.getDateFormat()) ? View.VISIBLE : View.INVISIBLE);
            RelativeLayout.LayoutParams lp_iVcheck = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            lp_iVcheck.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);

            ll_row.setBackgroundColor(s.equals(mFormValue_General.getDateFormat()) ?
                    getContext().getResources().getColor(R.color.Tint7) :
                    count % 2 == 0 ? getContext().getResources().getColor(R.color.Tint5) : getContext().getResources().getColor(R.color.Tint6));

            ll_textContainer.addView(tV, lp_tv);
            ll_textContainer.addView(tVPreview, lp_tvPreview);


            ll_row.addView(ll_textContainer);
            ll_row.addView(iV_check, lp_iVcheck);
            mViewHolderForm.mExpandableLayout_dateFormatLlContent.addView(ll_row, lp_row);
            count++;


            ll_row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    LinearLayout lparent = (LinearLayout) v.getParent();
                    int count = lparent.getChildCount();

                    for (int i = 0; i < count; i++) {

                        lparent.getChildAt(i).setBackgroundColor(i % 2 == 0 ? getContext().getResources().getColor(R.color.Tint5) : getContext().getResources().getColor(R.color.Tint6));
                        ((TextView) ((LinearLayout) ((RelativeLayout) lparent.getChildAt(i)).getChildAt(0)).getChildAt(0)).setTextColor(getContext().getResources().getColor(R.color.Font_Dark));
                        ((TextView) ((LinearLayout) ((RelativeLayout) lparent.getChildAt(i)).getChildAt(0)).getChildAt(1)).setTextColor(getContext().getResources().getColor(R.color.Font_Tint));
                        (((RelativeLayout) lparent.getChildAt(i)).getChildAt(1)).setVisibility(View.GONE);
                    }

                    String textOption = (String) ((TextView) ((LinearLayout) ((RelativeLayout) v).getChildAt(0)).getChildAt(0)).getText();


                    //settings_DateFormat = textOption;
                    dateFormatTitle.setText(textOption);


                    ((TextView) ((LinearLayout) ((RelativeLayout) v).getChildAt(0)).getChildAt(0)).setTextColor(getContext().getResources().getColor(R.color.Font_Light));
                    ((TextView) ((LinearLayout) ((RelativeLayout) v).getChildAt(0)).getChildAt(1)).setTextColor(getContext().getResources().getColor(R.color.Font_Light));
                    ((RelativeLayout) v).getChildAt(1).setVisibility(View.VISIBLE);
                    v.setBackgroundColor(getContext().getResources().getColor(R.color.Tint7));


                    String oldDateFormat = mViewHolderForm.settings_DateFormat;
                    mViewHolderForm.settings_DateFormat = textOption;

                    if (oldDateFormat != mViewHolderForm.settings_DateFormat) {
                        onDateFormatSelected(mViewHolderForm.settings_DateFormat);
                    }

                }
            });
        }

        mViewHolderForm.mExpandableLayout_dateFormatScrollView.addView(mViewHolderForm.mExpandableLayout_dateFormatLlContent, lp);
    }


    private void initFontstyle() {

        mViewHolderForm.mExpandableLayout_fontStyle.getHeaderLayout().addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (mViewHolderForm.mExpandableLayout_fontStyle.isOpened()) {
                    ((ImageView) v.findViewById(R.id.view_fontstyle_header_icon)).setImageDrawable(getResources().getDrawable(R.drawable.ic_expand_up));
                } else {
                    ((ImageView) v.findViewById(R.id.view_fontstyle_header_icon)).setImageDrawable(getResources().getDrawable(R.drawable.ic_expand_down));
                }
            }
        });


        final TextView fontStyleTitle = ((TextView) mViewHolderForm.mExpandableLayout_fontStyle.getHeaderLayout().findViewById(R.id.view_fontstyle_header_title));
        fontStyleTitle.setText(getHumanReadableTypeface(mFormValue_General.getTypeface()));

        int count = 0;
        for (String s : getResources().getStringArray(R.array.settings_array_fontstyle)) {

            RelativeLayout ll_row = new RelativeLayout(getContext());
            RelativeLayout.LayoutParams lp_row = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            ll_row.setPadding(50, 25, 25, 25);


            LinearLayout ll_textContainer = new LinearLayout(getContext());
            LinearLayout.LayoutParams lp_llContainer = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);


            TextView tV = new TextView(getContext());
            tV.setTextAppearance(getContext().getApplicationContext(), R.style.TextAppearance_AppCompat_Medium);
            LinearLayout.LayoutParams lp_tv = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);


            tV.setTextColor(s.equals(getHumanReadableTypeface(mFormValue_General.getTypeface())) ?
                    getContext().getResources().getColor(R.color.Font_Light) : getContext().getResources().getColor(R.color.Font_Dark));
            tV.setText(s);


            TextView tVPreview = new TextView(getContext());
            tVPreview.setTextAppearance(getContext().getApplicationContext(), R.style.TextAppearance_AppCompat_Small);
            LinearLayout.LayoutParams lp_tvPreview = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lp_tvPreview.leftMargin = 50;

            tVPreview.setTextColor(s.equals(getHumanReadableTypeface(mFormValue_General.getTypeface())) ?
                    getContext().getResources().getColor(R.color.Font_Light) : getContext().getResources().getColor(R.color.Font_Tint));
            tVPreview.setText("// " + getResources().getString(R.string.general_fontpreview));
            tVPreview.setTypeface(getTypeface(count));


            ImageView iV_check = new ImageView(getContext());
            iV_check.setImageBitmap(Bitmap.createScaledBitmap(((BitmapDrawable) getContext().getResources().getDrawable(R.drawable.ic_tick)).getBitmap(), getContext().getResources().getDimensionPixelOffset(R.dimen.fab_icon_size), getContext().getResources().getDimensionPixelOffset(R.dimen.fab_icon_size), false));
            iV_check.setColorFilter(getContext().getResources().getColor(R.color.White), PorterDuff.Mode.SRC_ATOP);
            iV_check.setVisibility(s.equals(getHumanReadableTypeface(mFormValue_General.getTypeface())) ? View.VISIBLE : View.INVISIBLE);
            RelativeLayout.LayoutParams lp_iVcheck = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            lp_iVcheck.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);

            ll_row.setBackgroundColor(s.equals(getHumanReadableTypeface(mFormValue_General.getTypeface())) ?
                    getContext().getResources().getColor(R.color.Tint7) :
                    count % 2 == 0 ? getContext().getResources().getColor(R.color.Tint5) : getContext().getResources().getColor(R.color.Tint6));

            ll_textContainer.addView(tV, lp_tv);
            ll_textContainer.addView(tVPreview, lp_tvPreview);


            ll_row.addView(ll_textContainer);
            ll_row.addView(iV_check, lp_iVcheck);
            mViewHolderForm.mExpandableLayout_fontStyleLlContent.addView(ll_row, lp_row);
            count++;


            ll_row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    LinearLayout lparent = (LinearLayout) v.getParent();
                    int count = lparent.getChildCount();

                    for (int i = 0; i < count; i++) {

                        lparent.getChildAt(i).setBackgroundColor(i % 2 == 0 ? getContext().getResources().getColor(R.color.Tint5) : getContext().getResources().getColor(R.color.Tint6));
                        ((TextView) ((LinearLayout) ((RelativeLayout) lparent.getChildAt(i)).getChildAt(0)).getChildAt(0)).setTextColor(getContext().getResources().getColor(R.color.Font_Dark));
                        ((TextView) ((LinearLayout) ((RelativeLayout) lparent.getChildAt(i)).getChildAt(0)).getChildAt(1)).setTextColor(getContext().getResources().getColor(R.color.Font_Tint));
                        (((RelativeLayout) lparent.getChildAt(i)).getChildAt(1)).setVisibility(View.GONE);
                    }
                    String textOption = (String) ((TextView) ((LinearLayout) ((RelativeLayout) v).getChildAt(0)).getChildAt(0)).getText();


                    String oldFontStyle = mViewHolderForm.settings_FontStyle;


                    if (textOption.equals("DEFAULT")) {
                        mViewHolderForm.settings_FontStyle = "JOSEFINSANS";
                    } else if (textOption.equals("Josefin Sans")) {
                        mViewHolderForm.settings_FontStyle = "JOSEFINSANS";
                    } else if (textOption.equals("Whitney Book")) {
                        mViewHolderForm.settings_FontStyle = "WHITNEYBOOK";
                    } else if (textOption.equals("Corbel")) {
                        mViewHolderForm.settings_FontStyle = "CORBEL";
                    }


                    if (oldFontStyle != mViewHolderForm.settings_FontStyle) {
                        onFontStyleSelected(mViewHolderForm.settings_FontStyle);
                    }


                    ((TextView) ((LinearLayout) ((RelativeLayout) v).getChildAt(0)).getChildAt(0)).setTextColor(getContext().getResources().getColor(R.color.Font_Light));
                    ((TextView) ((LinearLayout) ((RelativeLayout) v).getChildAt(0)).getChildAt(1)).setTextColor(getContext().getResources().getColor(R.color.Font_Light));
                    ((RelativeLayout) v).getChildAt(1).setVisibility(View.VISIBLE);
                    v.setBackgroundColor(getContext().getResources().getColor(R.color.Tint7));
                    fontStyleTitle.setText(textOption);
                }
            });
        }
    }

    private String getHumanReadableTypeface(String typeface) {

        String val = "";
        if (typeface == "") {
            val = getResources().getString(R.string.fragment_formgenerator_dialog_start_fontstyleselect);
        } else if (typeface == "JOSEFINSANS" || typeface.equals("JOSEFINSANS")) {
            val = "Josefin Sans";
        } else if (typeface == "WHITNEYBOOK" || typeface.equals("WHITNEYBOOK")) {
            val = "Whitney Book";
        } else if (typeface == "CORBEL" || typeface.equals("CORBEL")) {
            val = "Corbel";
        }
        return val;
    }


    private Typeface getTypeface(int position) {
        Typeface typeface;
        switch (position) {
            case 0:
                typeface = Typeface.createFromAsset(getResources().getAssets(), "fonts/JosefinSansRegular.ttf");
                break;
            case 1:
                typeface = Typeface.createFromAsset(getResources().getAssets(), "fonts/WhitneyBook.ttf");

                break;
            case 2:
                typeface = Typeface.createFromAsset(getResources().getAssets(), "fonts/Corbel.ttf");
                break;

            default:

                typeface = Typeface.DEFAULT;
                break;
        }
        return typeface;
    }




    /*

    private void updateDates(String dateFormat) {


        ContentValues mContentValues = new ContentValues();
        mContentValues.clear();

        SimpleDateFormat oldFormat = new SimpleDateFormat(mFormValue_General.getDateFormatOld());
        mContentValues.put(SQLiteHelper.COLUMN_GENERAL_DATEFORMAT_OLD, dateFormat);
        mContentValues.put(SQLiteHelper.COLUMN_GENERAL_DATEFORMAT, dateFormat);

        ((MainActivity) getActivity()).mDataSource.updateGeneralEntry(mContentValues, mFormValue_General.getId());

        List<Form_Entries_Experience> mEntriesList_Experience = ((MainActivity) getActivity()).mDataSource.getAllExperienceEntries();
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(dateFormat);


        // Update all Experience Date Entries
        for (Form_Entries_Experience e : mEntriesList_Experience) {

            try {

                mContentValues.clear();
                java.util.Date dateBegin = oldFormat.parse(e.getDateBegin().toCustomString());
                java.util.Date dateEnd = oldFormat.parse(e.getDateEnd().toCustomString());
                mContentValues.put(SQLiteHelper.COLUMN_EXPERIENCE_DATEBEGINN, mSimpleDateFormat.format(dateBegin));
                mContentValues.put(SQLiteHelper.COLUMN_EXPERIENCE_DATEEND, mSimpleDateFormat.format(dateEnd));
                ((MainActivity) getActivity()).mDataSource.updateExperienceEntry(mContentValues, e.getId());

            } catch (Exception u) {
                u.printStackTrace();
                continue;
            }
        }

        // Update Date of Birth
        mContentValues.clear();

        Form_Entries_BasicInfo mEntry_BasicInformation = ((MainActivity) getActivity()).mDataSource.getBasicInformationEntry();
        try {

            java.util.Date dateBorn = oldFormat.parse(mEntry_BasicInformation.getBorn().toCustomString());
            mContentValues.put(SQLiteHelper.COLUMN_BASICINFORMATION_BORN, mSimpleDateFormat.format(dateBorn));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (mContentValues.size() > 0) {
            ((MainActivity) getActivity()).mDataSource.updateBasicInformationEntry(mContentValues, mEntry_BasicInformation.getId());

        }

        mViewHolderForm.mViewDrawForm.initDataFields();
        setBitmap(mViewHolderForm.mViewDrawForm.drawBitmap());

    }

    */


}

package com.example.lukas.resume.datamanagement;

import android.content.ContentValues;
import android.content.Context;

import com.example.lukas.resume.R;
import com.example.lukas.resume.helpers.TimeUtils;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by luq89 on 08-Mar-17.
 */

public final class UserDataHub {

    private static Form_Entries_BasicInfo basicInfoData;
    private static List<Form_Entries_Education> educationData;
    private static List<Form_Entries_Experience> experienceData;
    private static Form_Entries_General generalData;
    private static List<Form_Entries_Skill> skillData;
    private static List<Form_Entries_Language> languageData;
    private static List<Form_Entries_Connect> connectData;

    private static DataSource dataSource;
    private static Context context;

    private static TimeUtils timeUtils;

    private UserDataHub(DataSource dataSource, Context context) {
        UserDataHub.dataSource = dataSource;
        UserDataHub.dataSource.open();
        UserDataHub.context = context;
        timeUtils = TimeUtils.newInstance(context);
    }

    public static UserDataHub newInstance(DataSource dataSource, Context context) {
        return new UserDataHub(dataSource, context);
    }

    @SuppressWarnings("unchecked")
    public static void refreshUserData() {

        if (TimeUtils.canRefresh(DateTime.now())) {

            try {
                basicInfoData = ((List<Form_Entries_BasicInfo>) dataSource.getSingle(SQLiteHelper
                        .TABLE_BASICINFORMATION)).get(0);
                generalData = ((List<Form_Entries_General>) dataSource.getSingle(SQLiteHelper
                        .TABLE_GENERAL)).get(0);


                educationData = (List<Form_Entries_Education>) dataSource.getAll(SQLiteHelper
                        .TABLE_EDUCATION);

                experienceData = (List<Form_Entries_Experience>) dataSource.getAll(SQLiteHelper
                        .TABLE_EXPERIENCE);

                skillData = (List<Form_Entries_Skill>) dataSource.getAll(SQLiteHelper
                        .TABLE_SKILL);

                languageData = (List<Form_Entries_Language>) dataSource.getAll(SQLiteHelper
                        .TABLE_LANGUAGE);

                connectData = (List<Form_Entries_Connect>) dataSource.getAll(SQLiteHelper
                        .TABLE_CONNECT);

            } catch (ClassCastException e) {
            }
        }
    }


    public static void insert(String tableName, ContentValues values) {
        dataSource.insert(tableName, values);
    }


    public static void update(String tableName, ContentValues values, long id) {
        dataSource.update(tableName, values, id);
        refreshUserData();
    }


    public static void update(String tableName, ContentValues values) {
        dataSource.update(tableName, values, generalData.getId());
    }

    public static void replace(String tableName, String keys, ContentValues values) {
        dataSource.replace(tableName, keys, values);
    }

    public static void delete(String tableName, long id) {
        dataSource.delete(tableName, id);
        refreshUserData();
    }

    public static void reset() {
        dataSource.reset();
    }

    public static UserEntry getEntry(String tableName, String keys, long id) {
        return dataSource.getEntry(tableName, keys, id);
    }

    public static boolean isTableEmpty(String tableName) {
        return dataSource.isEmpty(tableName);
    }

    public static int getEntriesCount(String tableName) {
        return dataSource.getCount(tableName);
    }


    public static Form_Entries_BasicInfo getBasicInfoData() {
        refreshUserData();
        return basicInfoData;
    }


    public static Form_Entries_General getGeneralData() {
        refreshUserData();
        return generalData;
    }

    public static List<Form_Entries_Skill> getSkillData() {
        refreshUserData();
        return skillData;
    }

    public static List<Form_Entries_Education> getEducationData() {
        refreshUserData();
        return educationData;
    }

    public static List<Form_Entries_Language> getLanguageData() {
        refreshUserData();
        return languageData;
    }

    public static List<Form_Entries_Connect> getConnectData() {
        refreshUserData();
        return connectData;
    }

    public static List<Form_Entries_Experience> getExperienceData() {
        refreshUserData();
        return experienceData;
    }

    public static SimpleDateFormat getDateFormat() {

        if (null != generalData.getDateFormat() && !generalData.getDateFormat()
                .isEmpty()) {
            return new SimpleDateFormat(generalData.getDateFormat());
        } else {

            SimpleDateFormat newFormat = new SimpleDateFormat(getResourceString(
                    R.string.default_dateformat), Locale.GERMAN);

            return newFormat;
        }
    }

    private static String getResourceString(int resId) {
        return context.getApplicationContext().getResources().getString(resId);
    }


    private static boolean isValidTableEntry(String value) {
        return null == value;
    }
}

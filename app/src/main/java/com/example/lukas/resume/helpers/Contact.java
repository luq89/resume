package com.example.lukas.resume.helpers;

/**
 * Created by luq89 on 07-Mar-17.
 */

public final class Contact {

    public final static String NAME = "name";
    public final static String MAIL = "mail";
    public final static String PHONE = "phone";
    public final static String BIRTHDAY = "birthday";
    public final static String ADDRESS = "address";

}

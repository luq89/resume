package com.example.lukas.resume.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lukas.resume.ContentFragment;
import com.example.lukas.resume.MainActivity;
import com.example.lukas.resume.R;
import com.example.lukas.resume.datamanagement.Form_Entries_Connect;
import com.example.lukas.resume.datamanagement.SQLiteHelper;
import com.example.lukas.resume.datamanagement.UserDataHub;
import com.example.lukas.resume.helpers.Utils;
import com.example.lukas.resume.views.dialogs.InputDialogs;

import java.util.List;

/**
 * Created by Lukas on 14.05.2015.
 */
public class CustomExpandableListViewAdapterConnect extends BaseExpandableListAdapter {

    public static class ViewHolderGroup {
        TextView textView_Rating;
        TextView textView_GroupTitle;
        ImageView imageView_Icon;
        ImageView imageView_DeleteEntry;
        Utils mUtils;
        //ImageView imageView_EditEntry;
    }

    public static class ViewHolderChild {
        TextView textView_Category;
    }

    public ViewHolderGroup mViewHolderGroup;
    public ViewHolderChild mViewHolderChild;
    Context context;
    //ArrayList<ListEntry> mEntriesList;

    List<Form_Entries_Connect> mFormConnectEntriesList;

    public CustomExpandableListViewAdapterConnect(Context context, ContentFragment parent, List<Form_Entries_Connect> mFormSkillEntriesList) {
        this.context = context;
        this.mFormConnectEntriesList = mFormSkillEntriesList;


    }


    public void deleteEntry(long id) {
        UserDataHub.delete(SQLiteHelper.TABLE_CONNECT, id);
        notifyDataSetChanged();
    }

    public void editEntry(long id) {

        FragmentTransaction ft = ((MainActivity) context).getSupportFragmentManager().beginTransaction();
        Fragment dialog = ((MainActivity) context).getSupportFragmentManager().findFragmentByTag("dialog");

        if (dialog != null) {
            ft.remove(dialog);
        }
        ft.addToBackStack(null);

        //Create and show dialog.
        InputDialogs newDialog = InputDialogs.newInstance(id, true, 2);
        newDialog.show(ft, "dialog");

    }


    @Override
    public int getGroupCount() {
        return mFormConnectEntriesList.size();
    }


    @Override
    public int getChildrenCount(int groupPosition) {
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupPosition;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        mViewHolderGroup = new ViewHolderGroup();
        mViewHolderGroup.mUtils = Utils.newInstance(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.customexpandablelistview_connect_group, null);

        mViewHolderGroup.imageView_DeleteEntry = (ImageView) convertView.findViewById(R.id.fragment_formgenerator_fifthstep_connect_fab_deleteentry);
        mViewHolderGroup.imageView_Icon = (ImageView) convertView.findViewById(R.id.customexpandablelistview_connect_group_image);

        if (mFormConnectEntriesList.size() > 0) {
            mViewHolderGroup.imageView_Icon.setImageResource(Utils.optionToResId(mFormConnectEntriesList.get(groupPosition).getOption()));
            mViewHolderGroup.textView_GroupTitle = (TextView) convertView.findViewById(R.id.customexpandablelistview_connect_group_title);
            //mViewHolderGroup.textView_GroupTitle.setTextForEditables(mFormConnectEntriesList.get(groupPosition).getOption() + " - " + mFormConnectEntriesList.get(groupPosition).getUser());
            mViewHolderGroup.textView_GroupTitle.setText(mFormConnectEntriesList.get(groupPosition).getOption());
        }


        mViewHolderGroup.imageView_DeleteEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(context)

                        .setMessage("Are you sure you want to delete this entry?")
                        .setIcon(0)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                deleteEntry(mFormConnectEntriesList.get(groupPosition).getId());
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        return null;

    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}

package com.example.lukas.resume.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.lukas.resume.R;

/**
 * Created by Lukas on 14.05.2015.
 */
public class CustomSpinnerAdapter_NoIcon extends ArrayAdapter {


    String[] val;


    public CustomSpinnerAdapter_NoIcon(Context ctx, int txtViewResourceId, String[] objects) {
        super(ctx, txtViewResourceId, objects);

        val = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }


    public View getCustomView(int position, View convertView,
                              ViewGroup parent) {


        LayoutInflater inflater = LayoutInflater.from(getContext());

        View mySpinner = inflater.inflate(R.layout.customspinner_no_icon, parent,
                false);


        TextView title = (TextView) mySpinner
                .findViewById(R.id.customspinner_connect_title);
        title.setText(val[position]);

        /*

        TextView subSpinner = (TextView) mySpinner
                .findViewById(R.id.sub_text_seen);
        subSpinner.setTextEditables(spinnerSubs[position]);

        ImageView left_icon = (ImageView) mySpinner
                .findViewById(R.id.left_pic);
        left_icon.setImageResource(total_images[position]);

        */

        return mySpinner;
    }

}
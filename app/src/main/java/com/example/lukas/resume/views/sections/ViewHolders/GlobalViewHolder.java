package com.example.lukas.resume.views.sections.ViewHolders;

import android.content.Context;
import android.view.View;
import android.view.ViewStub;

import com.example.lukas.resume.ContentFragment;
import com.example.lukas.resume.R;
import com.getbase.floatingactionbutton.FloatingActionButton;

import java.text.SimpleDateFormat;

/**
 * Created by luq89 on 08-Mar-17.
 */

public class GlobalViewHolder {

    private Context context;
    private ContentFragment parent;

    private ViewStub contentView;
    private SimpleDateFormat simpleDateFormat;
    private FloatingActionButton showPreviewFab;


    public GlobalViewHolder(Context context, ContentFragment parent, ViewStub contentView) {
        this.context = context;
        this.parent = parent;
        this.contentView = contentView;
        this.showPreviewFab = (FloatingActionButton) findViewWithinSubviewById(R.id
                .fragment_formgenerator_secondstep_fab_showPreview);
    }

    public GlobalViewHolder(Context context, ContentFragment parent) {
        this.context = context;
        this.parent = parent;
    }

    public void setOnClickListenerForShowPreviewFab(View.OnClickListener listener) {
        this.showPreviewFab.setOnClickListener(listener);

    }

    public void setFabPressed(boolean newState) {
        if (null != showPreviewFab) {
            showPreviewFab.setPressed(newState);
        }
    }

    public void setLayoutResource(int resource) {
        this.contentView.setLayoutResource(resource);
    }

    public View findViewWithinSubviewById(int id) {
        return contentView.findViewById(id);
    }

    public ViewStub getContentView() {
        return contentView;
    }

    public View getSubView() {
        return contentView.inflate();
    }


    public SimpleDateFormat getSimpleDateFormat() {
        return simpleDateFormat;
    }

    public void setSimpleDateFormat(SimpleDateFormat simpleDateFormat) {
        this.simpleDateFormat = simpleDateFormat;
    }

    public FloatingActionButton getShowPreviewFab() {
        return showPreviewFab;
    }

    public void setShowPreviewFab(FloatingActionButton showPreviewFab) {
        this.showPreviewFab = showPreviewFab;
    }

    public Context getContext() {
        return context;
    }

    public ContentFragment getParent() {
        return parent;
    }


}
package com.example.lukas.resume.views.dialogs;

import android.content.ContentValues;
import android.view.View;

/**
 * Created by luq89 on 10-Mar-17.
 */
public interface ICustomDialog {

    ContentValues getContentValues();

    ContentValues getContentValues(long mId);

    void initDialog();

    void initOnClickListeners();

    void readEntry();

    void readEntry(long entryId);

    void setOnClickListenerForSaveAction(View.OnClickListener listener);

    void setOnClickListenerForDismissAction(View.OnClickListener listener);

    void onError(String message);

}

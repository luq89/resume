package com.example.lukas.resume.helpers;


import android.content.Context;
import android.util.Log;

import com.example.lukas.resume.R;
import com.example.lukas.resume.datamanagement.UserDataHub;

import org.joda.time.DateTime;

/**
 * Created by luq89 on 17-Mar-17.
 */

public class DateFormatUtils {

    private static Context context;

    private static String selectedDateFormat;

    public static DateFormatUtils newInstance(Context context) {
        return new DateFormatUtils(context);
    }

    private DateFormatUtils(Context context) {
        DateFormatUtils.context = context;
        selectedDateFormat = UserDataHub.getGeneralData().getDateFormat();
    }

    public static String[] getDateFormats() {
        return context.getResources().getStringArray(R.array.settings_array_dateformat);
    }

    private static void toCustomString(DateTime now) {
        Log.v("TimtUtils updateTime", now.toString());
    }

    public static String getSelectedDateFormat() {
        return selectedDateFormat;
    }

    public static void setSelectedDateFormat(String newDateFormat) {
        selectedDateFormat = newDateFormat;
    }
}

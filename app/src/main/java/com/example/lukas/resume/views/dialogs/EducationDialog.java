package com.example.lukas.resume.views.dialogs;

import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.example.lukas.resume.R;
import com.example.lukas.resume.datamanagement.Form_Entries_Education;
import com.example.lukas.resume.datamanagement.SQLiteHelper;
import com.example.lukas.resume.datamanagement.UserDataHub;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class EducationDialog implements ICustomDialog {


    private TextView saveTextView;
    private LinearLayout dissmissLinearLayout;
    private TextView fromDateTextView;
    private TextView toDateTextView;
    private Date fromDateDate;
    private Date toDateDate;
    private TextView locationTextView;
    private TextView subjectTextView;
    private TextView descriptionTextView;
    private Switch onGoingSwitch;
    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;

    private Calendar calendar;

    private View rootView;
    private Context context;

    public EducationDialog(final View rootView, final Context context) {
        super();

        this.rootView = rootView;
        this.context = context;
        this.calendar = Calendar.getInstance();

        initDialog();

    }

    @Override
    public void initDialog() {

        this.saveTextView = (TextView) rootView.findViewById(R.id
                .fragment_formgenerator_dialog_skills_save);

        this.dissmissLinearLayout = (LinearLayout) rootView.findViewById(R.id
                .fragment_formgenerator_dialog_skill_dismiss);

        this.subjectTextView = (TextView) rootView.findViewById(R.id
                .fragment_formgenerator_dialog_input_subject);

        this.locationTextView = (TextView) rootView.findViewById(R.id
                .fragment_formgenerator_dialog_input_location);

        this.descriptionTextView = (TextView) rootView.findViewById(R.id
                .fragment_formgenerator_dialog_input_description);

        this.fromDateTextView = (TextView) rootView.findViewById(R.id
                .fragment_formgenerator_dialog_fromdate);

        this.toDateTextView = (TextView) rootView.findViewById(R.id
                .fragment_formgenerator_dialog_todate);

        this.onGoingSwitch = (Switch) rootView.findViewById(R.id
                .fragment_formgenerator_dialog_todate_ongoing);

        initOnClickListeners();
    }

    @Override
    public void initOnClickListeners() {

        this.onGoingSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                toDateTextView.setEnabled(!isChecked);

                int color = isChecked ?
                        context.getResources().getColor(R.color.Font_LightGrey) :
                        context.getResources().getColor(R.color.Font_Dark);

                toDateTextView.setTextColor(color);

                if (isChecked) {
                    toDateTextView.setText(" ");
                } else {
                    Calendar cal = Calendar.getInstance();
                    toDateTextView.setText(UserDataHub.getDateFormat().format(cal.getTime()));
                    toDateDate = cal.getTime();
                }
            }

        });

        this.fromDatePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                fromDateTextView.setText(UserDataHub.getDateFormat().format(newDate
                        .getTime()));
                fromDateDate = newDate.getTime();

            }

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get
                (Calendar.DAY_OF_MONTH));


        this.toDatePickerDialog = new DatePickerDialog(context, new DatePickerDialog
                .OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                toDateTextView.setText(UserDataHub.getDateFormat().format(newDate.getTime()));
                toDateDate = newDate.getTime();

            }

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get
                (Calendar.DAY_OF_MONTH));

        fromDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fromDatePickerDialog.show();
            }
        });

        toDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toDatePickerDialog.show();
            }
        });
    }

    @Override
    public void readEntry() {

    }

    @Override
    public ContentValues getContentValues() {


        ContentValues contentValues = new ContentValues();

        contentValues.put(
                SQLiteHelper.COLUMN_EDUCATION_DATEBEGINN,
                UserDataHub.getDateFormat().format(fromDateDate)
        );

        contentValues.put(
                SQLiteHelper.COLUMN_EDUCATION_DATEEND,
                UserDataHub.getDateFormat().format(toDateDate)
        );

        contentValues.put(
                SQLiteHelper.COLUMN_EDUCATION_INSTITUTION,
                subjectTextView.getText().toString()
        );

        contentValues.put(
                SQLiteHelper.COLUMN_EDUCATION_LOCATION,
                locationTextView.getText().toString()
        );

        contentValues.put(
                SQLiteHelper.COLUMN_EDUCATION_DEGREE,
                descriptionTextView.getText().toString());

        return contentValues;
    }

    @Override
    public ContentValues getContentValues(long mId) {
        return null;
    }

    @Override
    public void readEntry(long entryId) {

        List<Form_Entries_Education> eduList = UserDataHub.getEducationData();
        Form_Entries_Education entry = eduList.get(Long.valueOf(entryId).intValue());

        if (null != entry) {

            fromDateTextView.setText(entry.getDateBegin());
            toDateTextView.setText(entry.getDateEnd());
            locationTextView.setText(entry.getLocation());
            subjectTextView.setText(entry.getInstitution());
            descriptionTextView.setText(entry.getDegree());

            try {
                fromDateDate = UserDataHub.getDateFormat().parse(entry.getDateBegin());
                toDateDate = UserDataHub.getDateFormat().parse(entry.getDateEnd());
            } catch (ParseException e) {
                fromDateDate = toDateDate = calendar.getTime();
            }

        }


    }

    @Override
    public void setOnClickListenerForSaveAction(View.OnClickListener listener) {
        this.saveTextView.setOnClickListener(listener);
    }

    @Override
    public void setOnClickListenerForDismissAction(View.OnClickListener listener) {
        this.saveTextView.setOnClickListener(listener);
    }

    @Override
    public void onError(String message) {

    }


}
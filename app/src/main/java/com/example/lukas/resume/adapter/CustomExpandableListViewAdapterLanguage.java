package com.example.lukas.resume.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lukas.resume.ContentFragment;
import com.example.lukas.resume.MainActivity;
import com.example.lukas.resume.R;
import com.example.lukas.resume.datamanagement.Form_Entries_Language;
import com.example.lukas.resume.datamanagement.SQLiteHelper;
import com.example.lukas.resume.datamanagement.UserDataHub;
import com.example.lukas.resume.views.dialogs.InputDialogs;

import java.util.List;

/**
 * Created by Lukas on 14.05.2015.
 */
public class CustomExpandableListViewAdapterLanguage extends BaseExpandableListAdapter {

    public static class ViewHolderGroup {
        TextView textView_Rating;
        TextView textView_GroupTitle;
        //ImageView imageView_icon;
        ImageView imageView_DeleteEntry;
        ImageView imageView_EditEntry;
    }

    public static class ViewHolderChild {
        TextView textView_Name;
    }

    public ViewHolderGroup mViewHolderGroup;
    public ViewHolderChild mViewHolderChild;
    final private ContentFragment fragment;
    Context context;
    //ArrayList<ListEntry> mEntriesList;

    List<Form_Entries_Language> mFormLanguageEntriesList;

    public CustomExpandableListViewAdapterLanguage(Context context, ContentFragment parent, List<Form_Entries_Language> mFormLanguageEntriesList) {
        fragment = parent;
        this.context = context;
        this.mFormLanguageEntriesList = mFormLanguageEntriesList;
        //mEntriesList = new ArrayList<>();
    }


    public void deleteEntry(long id) {
        UserDataHub.delete(SQLiteHelper.TABLE_LANGUAGE, id);
        notifyDataSetChanged();
    }

    public void editEntry(long id) {


        FragmentTransaction ft = ((MainActivity) context).getSupportFragmentManager().beginTransaction();
        Fragment dialog = ((MainActivity) context).getSupportFragmentManager().findFragmentByTag("dialog");

        if (dialog != null) {
            ft.remove(dialog);
        }
        ft.addToBackStack(null);

        //Create and show dialog.
        InputDialogs newDialog = InputDialogs.newInstance(id, true, 3);
        newDialog.show(ft, "dialog");

    }


    @Override
    public int getGroupCount() {
        return mFormLanguageEntriesList.size();
    }


    @Override
    public int getChildrenCount(int groupPosition) {
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupPosition;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        mViewHolderGroup = new ViewHolderGroup();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.customexpandablelistview_language_group, null);

        mViewHolderGroup.imageView_DeleteEntry = (ImageView) convertView.findViewById(R.id.fragment_formgenerator_thirdstep_language_fab_deleteentry);
        mViewHolderGroup.imageView_EditEntry = (ImageView) convertView.findViewById(R.id.fragment_formgenerator_thirdstep_language_fab_editentry);


        if (mFormLanguageEntriesList.size() > 0) {

            //mViewHolderGroup.imageView_icon = (ImageView) convertView.findViewById(R.id.customexpandablelistview_language_group_image);
            mViewHolderGroup.textView_GroupTitle = (TextView) convertView.findViewById(R.id.customexpandablelistview_language_group_title);
            mViewHolderGroup.textView_GroupTitle.setText(mFormLanguageEntriesList.get(groupPosition).getLanguage());
            mViewHolderGroup.textView_Rating = (TextView) convertView.findViewById(R.id.customexpandablelistview_language_rating);
            mViewHolderGroup.textView_Rating.setText(String.valueOf((int) mFormLanguageEntriesList.get(groupPosition).getRating() + "/5"));

        }


        // recycle the already inflated view
        //mViewHolderGroup = (ViewHolderGroup) convertView.getTag();

        //mViewHolderGroup.index = mEntriesList.get(groupPosition).index;

        mViewHolderGroup.imageView_DeleteEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(context)

                        .setMessage("Are you sure you want to delete this entry?")
                        .setIcon(0)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                deleteEntry(mFormLanguageEntriesList.get(groupPosition).getId());
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });

        mViewHolderGroup.imageView_EditEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editEntry(mFormLanguageEntriesList.get(groupPosition).getId());
            }
        });

        return convertView;

    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        return null;

    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }


}

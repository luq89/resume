package com.example.lukas.resume.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.lukas.resume.R;

/**
 * Created by Lukas on 14.05.2015.
 */
public class CustomSpinnerAdapter_FontStyle extends ArrayAdapter {


    String[] val;


    Typeface typeface_WhitneyBook;
    Typeface typeface_JosefinSans;
    Typeface typeface_Corbel;

    Context ctx;


    public enum Typefaces {JOSEFINSANS, WHITNEYBOOK, CORBEL}

    public Typefaces mTypefaces;


    public CustomSpinnerAdapter_FontStyle(Context ctx, int txtViewResourceId, String[] objects) {
        super(ctx, txtViewResourceId, objects);

        this.ctx = ctx;
        val = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }


    public View getCustomView(int position, View convertView,
                              ViewGroup parent) {


        LayoutInflater inflater = LayoutInflater.from(getContext());

        View mySpinner = inflater.inflate(R.layout.customspinner_fontstyle, parent,
                false);

        TextView fontStyle = (TextView) mySpinner
                .findViewById(R.id.customspinner_textview_fontstyle);

        TextView preview = (TextView) mySpinner
                .findViewById(R.id.customspinner_fontstyle_preview);
        preview.setText(ctx.getString(R.string.general_fontpreview));

        switch (position) {
            case 0:
                typeface_JosefinSans = Typeface.createFromAsset(ctx.getAssets(), "fonts/JosefinSansRegular.ttf");
                fontStyle.setText(val[position]);
                preview.setTypeface(typeface_JosefinSans);
                break;
            case 1:
                typeface_WhitneyBook = Typeface.createFromAsset(ctx.getAssets(), "fonts/WhitneyBook.ttf");
                fontStyle.setText(val[position]);
                preview.setTypeface(typeface_WhitneyBook);
                break;
            case 2:
                typeface_Corbel = Typeface.createFromAsset(ctx.getAssets(), "fonts/Corbel.ttf");
                fontStyle.setText(val[position]);
                preview.setTypeface(typeface_Corbel);
                break;

            default:
                break;
        }

        return mySpinner;

    }


}
package com.example.lukas.resume.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.lukas.resume.pageritems.SamplePagerItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luq89 on 07-Mar-17.
 */

public class CustomFragmentPagerAdapter extends FragmentPagerAdapter {

    private List<SamplePagerItem> mTabs = new ArrayList<>();

    public CustomFragmentPagerAdapter(FragmentManager fm, List<SamplePagerItem> mTabs) {
        super(fm);
        this.mTabs = mTabs;
    }

    @Override
    public Fragment getItem(int i) {
        return mTabs.get(i).createFragment();
    }

    @Override
    public int getCount() {
        return mTabs.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabs.get(position).getTitle();
    }
}
package com.example.lukas.resume.views.dialogs;


import android.app.Activity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lukas.resume.R;
import com.example.lukas.resume.datamanagement.Form_Entries_Connect;
import com.example.lukas.resume.datamanagement.Form_Entries_Skill;
import com.example.lukas.resume.datamanagement.SQLiteHelper;
import com.example.lukas.resume.datamanagement.UserDataHub;

import java.text.SimpleDateFormat;

/**
 * Created by Lukas on 21.05.2015.
 */


public class InputDialogs extends DialogFragment {


    public class ViewHolderImpressum {

    }


    SimpleDateFormat mSimpleDateFormat;

    public EducationDialog educationDialog;
    public ExperienceDialog experienceDialog;
    public SkillsetDialog skillsetDialog;
    public LanguageDialog languageDialog;
    public ConnectDialog connectDialog;
    public ViewHolderImpressum mViewHolderImpressum;

    ContentValues mContentValues = new ContentValues();

    static long mId = -1;
    static boolean isDialogInEditMode = false;
    static String mDialogTypes[] = {"Education", "Experience", "Skill", "Language", "Connect", "Impressum"};
    static String mDialogType;

    OnFragmentInteractionListener mCallback;


    public static InputDialogs newInstance(long id, boolean inEditMode, int type) {
        InputDialogs f = new InputDialogs();
        mId = id;
        isDialogInEditMode = inEditMode;
        mDialogType = mDialogTypes[type];
        f.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        return f;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        if (null != getDialog()) {
            getDialog().setCanceledOnTouchOutside(false);
        }

        View rootView = null;

        switch (mDialogType) {

            case "Education":

                rootView = inflater.inflate(
                        R.layout.fragment_formgenerator_dialog_education,
                        container,
                        false
                );
                initEducationDialog(rootView);

                break;

            case "Experience":

                rootView = inflater.inflate(
                        R.layout.fragment_formgenerator_dialog_experience,
                        container,
                        false
                );
                initExperienceDialog(rootView);

                break;

            case "Skill":


                rootView = inflater.inflate(R.layout.fragment_formgenerator_dialog_skills, container, false);

                initSkillDialog(rootView);

                break;


            case "Language":
                rootView = inflater.inflate(R.layout.fragment_formgenerator_dialog_language, container, false);
                initLanguageDialog(rootView);

                break;

            case "Connect":

                /*

                connectDialog = new ViewHolderConnect(rootView, getActivity());
                rootView = inflater.inflate(R.layout.fragment_formgenerator_dialog_connect, container, false);

                connectDialog.textView_Save = (TextView) rootView.findViewById(R.id.fragment_formgenerator_dialog_connect_save);
                connectDialog.textView_Save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        //if (connectDialog.editText_user.getText().length() == 0) {
                        if (connectDialog.textView_connectOptionTitle.getText().length() == 0) {
                            connectDialog.linearLayout_container_errormessage.setVisibility(View.VISIBLE);
                        } else {
                            saveDataToDatabase((int) mId);
                        }


                    }
                });

                connectDialog.linearLayout_dismiss = (LinearLayout) rootView.findViewById(R.id.fragment_formgenerator_dialog_connect_dismiss);
                connectDialog.linearLayout_dismiss.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                    }
                });

                connectDialog.linearLayout_container_errormessage = (LinearLayout) rootView.findViewById(R.id.fragment_formgenerator_dialog_connect_errormessage);
                connectDialog.mExpandableLayout = (ExpandableLayout) rootView.findViewById(R.id.fragment_formgenerator_dialog_connect_expandablelayout_connectoption);
                connectDialog.connectOptionsScrollView = (ScrollView) connectDialog.mExpandableLayout.getContentLayout().findViewById(R.id.view_dateformat_content_scollview);
                initConnectOptions();
                connectDialog.mExpandableLayout.show();


                //connectDialog.editText_user = (EditText) rootView.findViewById(R.id.fragment_formgenerator_dialog_connect_input_user);

                if (!isDialogInEditMode) {
                    mFormConnectEntriesList = ((MainActivity) getActivity()).mDataSource.getAllConnectEntries();
                } else {

                    Form_Entries_Connect entry = ((MainActivity) getActivity()).mDataSource.getConnectEntry(mId);
                    connectDialog.mExpandableLayout.setVisibility(View.GONE);

                    //connectDialog.editText_user.setTextForEditables(entry.getUser());
                }

                */

                break;


            case "Impressum":

                mViewHolderImpressum = new ViewHolderImpressum();
                rootView = inflater.inflate(R.layout.fragment_formgenerator_dialog_impressum, container, false);

                (rootView.findViewById(R.id.fragment_formgenerator_dialog_impressum_dissmis)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                    }
                });

                for (String s : getResources().getStringArray(R.array.fragment_formgenerator_dialog_impressum_credits_array)) {

                    TextView tv = new TextView(getContext());
                    tv.setTextColor(getResources().getColor(R.color.Font_Dark));

                    tv.setText(s);

                    ((LinearLayout) rootView.findViewById(R.id.fragment_formgenerator_dialog_impressum_credits_container)).addView(tv);
                }

                for (String s : getResources().getStringArray(R.array.fragment_formgenerator_dialog_impressum_fonts_array)) {

                    TextView tv = new TextView(getContext());
                    tv.setTextColor(getResources().getColor(R.color.Font_Dark));

                    tv.setText(s);

                    ((LinearLayout) rootView.findViewById(R.id.fragment_formgenerator_dialog_impressum_fonts_container)).addView(tv);
                }


                for (String s : getResources().getStringArray(R.array.fragment_formgenerator_dialog_impressum_additoinal_array)) {

                    TextView tv = new TextView(getContext());
                    tv.setTextColor(getResources().getColor(R.color.Font_Dark));

                    tv.setText(s);

                    ((LinearLayout) rootView.findViewById(R.id.fragment_formgenerator_dialog_impressum_additional_container)).addView(tv);
                }

                break;
        }
        return rootView;
    }

    private void initSkillDialog(View rootView) {
        skillsetDialog = new SkillsetDialog(rootView, getActivity());


        skillsetDialog.setOnClickListenerForSaveAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                        /*


                        if (isDialogInEditMode == false) {
                            int cat =  skillsetDialog.getSelectedCategory();
                            int subCat = skillsetDialog.getSelectedSubCategory();




                            if (cat != -1 && subCat != 1) {

                                // Check if

                                    UserDataHub.replace(SQLiteHelper.TABLE_SKILL, mContentValues,
                                            );
                                    dismiss();


                            } else {
                                skillsetDialog.onError(getString(R.string.errormessage_entryexists));
                            }
                        } else {

                            if (isDialogInEditMode) {

                                UserDataHub.update(
                                        SQLiteHelper.TABLE_SKILL,
                                        experienceDialog.getContentValues(),
                                        mId
                                );
                                dismiss();

                            } else {

                                // Check if
                                if (isValidSkillEntry(mContentValues)) {
                                    UserDataHub.insert(SQLiteHelper.TABLE_SKILL, mContentValues);
                                    dismiss();
                                } else {

                                    skillsetDialog.onError(getString(R.string.errormessage_entryexists));

                                }

                            }

                            mCallback.onUpdateSkillAdapter();
                        }
                        */
            }
        });


        skillsetDialog.setOnClickListenerForDismissAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        if (isDialogInEditMode) {
            skillsetDialog.readEntry(mId);
        } else {

        }


    }

    private void initLanguageDialog(View rootView) {

        languageDialog = new LanguageDialog(rootView, getActivity(), isDialogInEditMode);

        languageDialog.setOnClickListenerForSaveAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (isDialogInEditMode) {

                    UserDataHub.update(SQLiteHelper.TABLE_LANGUAGE, languageDialog
                            .getContentValues(), mId);

                } else {
                    UserDataHub.insert(SQLiteHelper.TABLE_LANGUAGE, languageDialog
                            .getContentValues());

                    //if (!languageDialog.Language.equals("")) {
                    //languageDialog.onError(getString(R.string.errormessage_entryexists));


                }
                dismiss();
                mCallback.onUpdateLanguageAdapter();

            }


        });

        languageDialog.setOnClickListenerForDismissAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        if (isDialogInEditMode) {
            languageDialog.readEntry(mId);

        } else {

                    /*
                    languageDialog.gridLayout_titleContainer = (GridLayout) rootView.findViewById(R.id.fragment_formgenerator_dialog_gridlayout);
                    languageDialog.linearLayout_container_pager = (LinearLayout) rootView.findViewById(R.id.fragment_formgenerator_dialog_language_pager_container);
                    languageDialog.linearLayout_container_pager.setVisibility(View.GONE);

                    Form_Entries_Skill entry = ((MainActivity) getActivity()).mDataSource.getSkillEntry(mId);
                    languageDialog.textView_Title.setText("Edit: " + entry.getCategory() + " - " + entry.getSubCategory());
                    languageDialog.seekBar.setProgress((int) entry.getRating());

                    mFormSkillEntriesList = ((MainActivity) getActivity()).mDataSource.getAllSkillEntries();
                    languageDialog.editText_lang = (EditText) rootView.findViewById(R.id.fragment_formgenerator_dialog_language_input_lan);
                    */

        }


    }

    private void initExperienceDialog(View rootView) {

        experienceDialog = new ExperienceDialog(rootView, getActivity());
        experienceDialog.setOnClickListenerForSaveAction(new View.OnClickListener() {

            public void onClick(View v) {

                if (isDialogInEditMode) {
                    UserDataHub.update(
                            SQLiteHelper.TABLE_EXPERIENCE,
                            experienceDialog.getContentValues()
                    );


                } else {
                    UserDataHub.update(
                            SQLiteHelper.TABLE_EXPERIENCE,
                            experienceDialog.getContentValues());
                }

                mCallback.onUpdateExperienceAdapter();
                dismiss();
            }
        });

        experienceDialog.setOnClickListenerForDismissAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        // mID is != -1 if a dialog gets opened in editmode.
        if (isDialogInEditMode) {
            experienceDialog.readEntry(mId);
        } else {
            //experienceDialog.textView_FromDate.setTextForEditables(mSimpleDateFormat.format(mCalendar.getTime()));

                    /*
                    educationDialog.textView_FromDate.setText(mSimpleDateFormat.format(mCalendarEducation.getTime()));
                    educationDialog.textView_ToDate.setText(mSimpleDateFormat.format(mCalendarEducation.getTime()));
                    educationDialog.textView_ToDate.setText(mSimpleDateFormat.format(mCalendarEducation.getTime()));
                    educationDialog.date_FromDate = mCalendarEducation.getTime();
                    educationDialog.date_ToDate = mCalendarEducation.getTime();
                    */
        }


    }

    private void initEducationDialog(View rootView) {

        educationDialog = new EducationDialog(rootView, getActivity());

        educationDialog.setOnClickListenerForSaveAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isDialogInEditMode) {
                    UserDataHub.update(
                            SQLiteHelper.TABLE_EDUCATION,
                            educationDialog.getContentValues()
                    );


                } else {
                    UserDataHub.insert(
                            SQLiteHelper.TABLE_EDUCATION,
                            educationDialog.getContentValues());
                }

                mCallback.onUpdateEducationAdapter();
                dismiss();
            }
        });

        educationDialog.setOnClickListenerForDismissAction(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        // mID is != -1 if a dialog gets opened in editmode.
        if (isDialogInEditMode) {

            educationDialog.readEntry(mId);


        } else {
            //experienceDialog.textView_FromDate.setTextForEditables(mSimpleDateFormat.format(mCalendar.getTime()));

                    /*
                    educationDialog.textView_FromDate.setText(mSimpleDateFormat.format(mCalendarEducation.getTime()));
                    educationDialog.textView_ToDate.setText(mSimpleDateFormat.format(mCalendarEducation.getTime()));
                    educationDialog.textView_ToDate.setText(mSimpleDateFormat.format(mCalendarEducation.getTime()));
                    educationDialog.date_FromDate = mCalendarEducation.getTime();
                    educationDialog.date_ToDate = mCalendarEducation.getTime();
                    */
        }
    }


    private String getSubCategory(int cat, int subcat) {


        String[] arr = {};

        switch (cat) {
            case 0:
                arr = getResources().getStringArray(R.array.fragment_formgenerator_dialog_skills_array_subcategory_general);
                break;

            case 1:
                arr = getResources().getStringArray(R.array.fragment_formgenerator_dialog_skills_array_subcategory_development);
                break;
            case 2:
                arr = getResources().getStringArray(R.array.fragment_formgenerator_dialog_skills_array_subcategory_design);
                break;
            default:

        }
        return arr[subcat];

    }


    private void saveDataToDatabase(int mId) {


        switch (mDialogType) {

            case "Skill":


                break;


            case "Language":


                break;


            case "Connect":

                 /*

                if (isDialogInEditMode) {

                    Form_Entries_Connect entry = ((MainActivity) getActivity()).mDataSource.getConnectEntry(mId);
                    mContentValues.clear();
                    mContentValues.put(SQLiteHelper.COLUMN_CONNECT_OPTION, entry.getOption());
                    //mContentValues.put(SQLiteHelper.COLUMN_CONNECT_USER, connectDialog.editText_user.getText().toCustomString());
                    ((MainActivity) getActivity()).mDataSource.updateConnectEntry(mContentValues, mId);
                    dismiss();

                } else {

                    mContentValues.clear();
                    mContentValues.put(SQLiteHelper.COLUMN_CONNECT_OPTION, (String) connectDialog.textView_connectOptionTitle.getText());
                    //mContentValues.put(SQLiteHelper.COLUMN_CONNECT_USER, connectDialog.editText_user.getText().toCustomString());

                    if (isValidConnectEntry(mContentValues)) {
                        ((MainActivity) getActivity()).mDataSource.createConnectEntry(mContentValues);
                        dismiss();
                    } else {

                        connectDialog.linearLayout_container_errormessage.setBackgroundColor(getResources().getColor(R.color.Tint4));
                        ((TextView) ((RelativeLayout) connectDialog.linearLayout_container_errormessage.getChildAt(1)).getChildAt(1)).setText(getString(R.string.errormessage_entryexists));
                        connectDialog.linearLayout_container_errormessage.setVisibility(View.VISIBLE);


                    }

                }

                mCallback.onUpdateConnectAdapter();

                */

                break;

        }


    }

    private boolean isValidSkillEntry(ContentValues cV) {

        boolean result = true;

        for (Form_Entries_Skill skill : UserDataHub.getSkillData()) {

            if (cV.get(SQLiteHelper.COLUMN_SKILL_CATEGORY).equals(skill.getCategory()) && cV.get(SQLiteHelper.COLUMN_SKILL_SUBCATEGORY).equals(skill.getSubCategory())) {
                result = false;
            }
        }
        return result;

    }

    private boolean isValidConnectEntry(ContentValues cV) {

        boolean result = true;


        for (Form_Entries_Connect connection : UserDataHub.getConnectData()) {

            if (cV.get(SQLiteHelper.COLUMN_CONNECT_OPTION).equals(connection.getOption())) {
                result = false;
            }
        }
        return result;
    }


    /*
    private void initConnectOptions() {


        Utils mUtils = new Utils(getContext());

        connectDialog.mExpandableLayout_connectOptionsLlContent = (LinearLayout) new LinearLayout(getContext());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        connectDialog.mExpandableLayout_connectOptionsLlContent.setOrientation(LinearLayout.VERTICAL);

        connectDialog.mExpandableLayout.getHeaderLayout().addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (connectDialog.mExpandableLayout.isOpened()) {
                    ((ImageView) v.findViewById(R.id.view_dateformat_header_icon)).setImageDrawable(getResources().getDrawable(R.drawable.ic_expand_up));

                } else {
                    ((ImageView) v.findViewById(R.id.view_dateformat_header_icon)).setImageDrawable(getResources().getDrawable(R.drawable.ic_expand_down));
                }
            }
        });


        String[] optionsArr = getContext().getApplicationContext().getResources().getStringArray(R.array.fragment_formgenerator_dialog_connect_array);
        connectDialog.textView_connectOptionTitle = ((TextView) connectDialog.mExpandableLayout.getHeaderLayout().findViewById(R.id.view_dateformat_header_title));
        connectDialog.textView_connectOptionTitle.setText(optionsArr[0]);
        connectDialog.textView_connectOptionTitle.setTextAppearance(getContext().getApplicationContext(), R.style.TextAppearance_AppCompat_Medium);
        connectDialog.textView_connectOptionTitle.setTextColor(getResources().getColor(R.color.Font_Dark));

        int count = 0;
        for (String s : optionsArr) {

            RelativeLayout ll_row = new RelativeLayout(getContext());
            RelativeLayout.LayoutParams lp_row = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            ll_row.setPadding(50, 25, 25, 25);


            LinearLayout ll_textContainer = new LinearLayout(getContext());
            LinearLayout.LayoutParams lp_llContainer = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);


            ImageView iV_option = new ImageView(getContext());
            iV_option.setImageBitmap(mUtils.optionToBitmap(s,
                    getContext().getResources().getDimensionPixelOffset(R.dimen.fab_icon_size), getContext().getResources().getDimensionPixelOffset(R.dimen.fab_icon_size)));


            TextView tV = new TextView(getContext());
            tV.setTextAppearance(getContext().getApplicationContext(), R.style.TextAppearance_AppCompat_Medium);
            LinearLayout.LayoutParams lP_tV = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            tV.setTextColor(getContext().getResources().getColor(R.color.Font_Dark));
            tV.setText(s);
            lP_tV.leftMargin = 40;

            ImageView iV_check = new ImageView(getContext());
            iV_check.setImageBitmap(Bitmap.createScaledBitmap(((BitmapDrawable) getContext().getResources().getDrawable(R.drawable.ic_tick)).getBitmap(), getContext().getResources().getDimensionPixelOffset(R.dimen.fab_icon_size), getContext().getResources().getDimensionPixelOffset(R.dimen.fab_icon_size), false));
            iV_check.setColorFilter(getContext().getResources().getColor(R.color.White), PorterDuff.Mode.SRC_ATOP);
            iV_check.setVisibility(View.INVISIBLE);

            RelativeLayout.LayoutParams lp_iVcheck = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            lp_iVcheck.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);


            ll_row.setBackgroundColor(
                    count % 2 == 0 ? getContext().getResources().getColor(R.color.Tint5) : getContext().getResources().getColor(R.color.Tint6));


            ll_textContainer.addView(iV_option);
            ll_textContainer.addView(tV, lP_tV);


            ll_row.addView(ll_textContainer);
            ll_row.addView(iV_check, lp_iVcheck);
            connectDialog.mExpandableLayout_connectOptionsLlContent.addView(ll_row, lp_row);
            count++;


            ll_row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    LinearLayout lparent = (LinearLayout) (ViewGroup) v.getParent();
                    int count = lparent.getChildCount();

                    for (int i = 0; i < count; i++) {

                        lparent.getChildAt(i).setBackgroundColor(i % 2 == 0 ? getContext().getResources().getColor(R.color.Tint5) : getContext().getResources().getColor(R.color.Tint6));
                        ((TextView) ((LinearLayout) ((RelativeLayout) lparent.getChildAt(i)).getChildAt(0)).getChildAt(1)).setTextColor(getContext().getResources().getColor(R.color.Font_Dark));
                        (((RelativeLayout) lparent.getChildAt(i)).getChildAt(1)).setVisibility(View.GONE);
                    }
                    connectDialog.textView_connectOptionTitle.setText((String) ((TextView) ((LinearLayout) ((RelativeLayout) v).getChildAt(0)).getChildAt(1)).getText());
                    ((TextView) ((LinearLayout) ((RelativeLayout) v).getChildAt(0)).getChildAt(1)).setTextColor(getContext().getResources().getColor(R.color.Font_Light));
                    ((RelativeLayout) v).getChildAt(1).setVisibility(View.VISIBLE);
                    v.setBackgroundColor(getContext().getResources().getColor(R.color.Tint7));
                }
            });
        }

        connectDialog.connectOptionsScrollView.addView(connectDialog.mExpandableLayout_connectOptionsLlContent, lp);
    }

    */


    @Override
    public void show(FragmentManager manager, String tag) {
        super.show(manager, tag);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    public interface OnFragmentInteractionListener {


        // TODO: Update argument type and name
        void onUpdateEducationAdapter();

        // TODO: Update argument type and name
        void onUpdateExperienceAdapter();

        // TODO: Update argument type and name
        void onUpdateSkillAdapter();

        // TODO: Update argument type and name
        void onUpdateLanguageAdapter();

        // TODO: Update argument type and name
        void onUpdateConnectAdapter();


    }

}

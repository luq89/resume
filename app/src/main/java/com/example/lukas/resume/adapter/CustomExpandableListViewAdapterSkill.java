package com.example.lukas.resume.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lukas.resume.ContentFragment;
import com.example.lukas.resume.MainActivity;
import com.example.lukas.resume.R;
import com.example.lukas.resume.datamanagement.Form_Entries_Skill;
import com.example.lukas.resume.datamanagement.SQLiteHelper;
import com.example.lukas.resume.datamanagement.UserDataHub;
import com.example.lukas.resume.helpers.Utils;
import com.example.lukas.resume.views.dialogs.InputDialogs;

import java.util.List;

/**
 * Created by Lukas on 14.05.2015.
 */
public class CustomExpandableListViewAdapterSkill extends BaseExpandableListAdapter {

    public static class ViewHolderGroup {
        TextView textView_Rating;
        TextView textView_GroupTitle;
        ImageView imageView_icon;
        ImageView imageView_DeleteEntry;
        ImageView imageView_EditEntry;
    }

    public static class ViewHolderChild {
        TextView textView_Category;
        TextView textView_SubCategory;
    }

    public ViewHolderGroup mViewHolderGroup;
    public ViewHolderChild mViewHolderChild;
    final private ContentFragment fragment;
    Context context;
    //ArrayList<ListEntry> mEntriesList;

    List<Form_Entries_Skill> mFormSkillEntriesList;

    public CustomExpandableListViewAdapterSkill(Context context, ContentFragment parent, List<Form_Entries_Skill> mFormSkillEntriesList) {
        fragment = parent;
        this.context = context;
        this.mFormSkillEntriesList = mFormSkillEntriesList;
        //mEntriesList = new ArrayList<>();
    }


    public void deleteEntry(long id) {
        UserDataHub.delete(SQLiteHelper.TABLE_SKILL, id);
        notifyDataSetChanged();
    }

    public void editEntry(long id) {


        FragmentTransaction ft = ((MainActivity) context).getSupportFragmentManager().beginTransaction();
        Fragment dialog = ((MainActivity) context).getSupportFragmentManager().findFragmentByTag("dialog");

        if (dialog != null) {
            ft.remove(dialog);
        }
        ft.addToBackStack(null);

        //Create and show dialog.
        InputDialogs newDialog = InputDialogs.newInstance(id, true, 2);
        newDialog.show(ft, "dialog");

    }


    @Override
    public int getGroupCount() {
        return mFormSkillEntriesList.size();
    }


    @Override
    public int getChildrenCount(int groupPosition) {
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupPosition;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        mViewHolderGroup = new ViewHolderGroup();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.customexpandablelistview_skill_group, null);

        mViewHolderGroup.imageView_DeleteEntry = (ImageView) convertView.findViewById(R.id.fragment_formgenerator_thirdstep_skill_fab_deleteentry);
        mViewHolderGroup.imageView_EditEntry = (ImageView) convertView.findViewById(R.id.fragment_formgenerator_thirdstep_skill_fab_editentry);


        if (mFormSkillEntriesList.size() > 0) {

            mViewHolderGroup.imageView_icon = (ImageView) convertView.findViewById(R.id.customexpandablelistview_skill_group_image);
            mViewHolderGroup.imageView_icon.setImageResource(Utils.newInstance(context).optionToResId
                    (mFormSkillEntriesList.get(groupPosition).getSubCategory()));


            mViewHolderGroup.textView_GroupTitle = (TextView) convertView.findViewById(R.id.customexpandablelistview_skill_group_title);
            mViewHolderGroup.textView_GroupTitle.setText(mFormSkillEntriesList.get(groupPosition).getCategory() + " - " + mFormSkillEntriesList.get(groupPosition).getSubCategory());


            mViewHolderGroup.textView_Rating = (TextView) convertView.findViewById(R.id.customexpandablelistview_skill_rating);
            mViewHolderGroup.textView_Rating.setText(String.valueOf((int) mFormSkillEntriesList.get(groupPosition).getRating() + "/5"));

        }


        // recycle the already inflated view
        //mViewHolderGroup = (ViewHolderGroup) convertView.getTag();

        //mViewHolderGroup.index = mEntriesList.get(groupPosition).index;

        mViewHolderGroup.imageView_DeleteEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(context)

                        .setMessage("Are you sure you want to delete this entry?")
                        .setIcon(0)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                deleteEntry(mFormSkillEntriesList.get(groupPosition).getId());
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });

        mViewHolderGroup.imageView_EditEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editEntry(mFormSkillEntriesList.get(groupPosition).getId());
            }
        });

        return convertView;

    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        return null;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }


}

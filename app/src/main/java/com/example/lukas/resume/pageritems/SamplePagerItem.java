package com.example.lukas.resume.pageritems;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.example.lukas.resume.ContentFragment;
import com.example.lukas.resume.common.slidingtablayout.SlidingTabLayout;
import com.example.lukas.resume.helpers.Enums.SectionTitles;

/**
 * Created by luq89 on 07-Mar-17.
 */

public class SamplePagerItem {

    private final CharSequence mTitle;
    private final int mIndicatorColor;
    private final int mDividerColor;

    public SamplePagerItem(SectionTitles section) {
        mTitle = section.getName();
        mIndicatorColor = section.getResourceColor();
        mDividerColor = section.getDividerColor();
    }

    /**
     * @return A new {@link Fragment} to be displayed by a {@link ViewPager}
     */
    public Fragment createFragment() {
        return ContentFragment.newInstance(mTitle, mIndicatorColor, mDividerColor);
    }

    /**
     * @return the title which represents this tab. In this sample this is used directly by
     * {@link android.support.v4.view.PagerAdapter#getPageTitle(int)}
     */
    public CharSequence getTitle() {
        return mTitle;
    }

    /**
     * @return the color to be used for indicator on the {@link SlidingTabLayout}
     */
    int getIndicatorColor() {
        return mIndicatorColor;
    }

    /**
     * @return the color to be used for right divider on the {@link SlidingTabLayout}
     */
    int getDividerColor() {
        return mDividerColor;
    }
}

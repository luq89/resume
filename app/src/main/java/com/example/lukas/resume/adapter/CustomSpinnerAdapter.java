package com.example.lukas.resume.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lukas.resume.R;

/**
 * Created by Lukas on 14.05.2015.
 */
public class CustomSpinnerAdapter extends ArrayAdapter {


    String[] val;


    public CustomSpinnerAdapter(Context ctx, int txtViewResourceId, String[] objects) {
        super(ctx, txtViewResourceId, objects);

        val = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }


    public View getCustomView(int position, View convertView,
                              ViewGroup parent) {


        LayoutInflater inflater = LayoutInflater.from(getContext());

        View mySpinner = inflater.inflate(R.layout.customspinner, parent,
                false);


        TextView title = (TextView) mySpinner
                .findViewById(R.id.customspinner_connect_title);
        title.setText(val[position]);


        ImageView image = (ImageView) mySpinner.findViewById(R.id.customspinner_connect_image);
        image.setImageResource(optionToResId(val[position]));


        return mySpinner;
    }


    private int optionToResId(String option) {

        int resId;

        switch (option) {

            //Icons Social Connect

            case "Facebook":
                resId = R.drawable.ic_social_facebook;
                break;
            case "Twitter":
                resId = R.drawable.ic_social_twitter;
                break;
            case "Printerest":
                resId = R.drawable.ic_social_printerest;
                break;
            case "Youtube":
                resId = R.drawable.ic_social_youtube;
                break;
            case "Google+":
                resId = R.drawable.ic_social_google;
                break;
            case "LinkedIn":
                resId = R.drawable.ic_social_linkedin;
                break;
            case "Wordpress":
                resId = R.drawable.ic_social_wordpress;
                break;
            case "Email":
                resId = R.drawable.ic_social_email;
                break;

            case "StackExchange":
                resId = R.drawable.ic_social_stackexchange;
                break;

            case "GitHub":
                resId = R.drawable.ic_social_github;
                break;


            //Icons Skill General

            case "Microsoft Office":
                resId = R.drawable.ic_general_msoffice;
                break;

            //Icons Skill Development

            case "C/C++":
                resId = R.drawable.ic_skill_development_cpp;
                break;
            case "CSharp":
                resId = R.drawable.ic_skill_development_csharp;
                break;

            case "HTML/CSS":
                resId = R.drawable.ic_skill_development_html;
                break;

            case "HTML 5":
                resId = R.drawable.ic_skill_development_html5;
                break;

            case "JavaScript":
                resId = R.drawable.ic_skill_development_javascript;
                break;

            case "PHP":
                resId = R.drawable.ic_skill_development_php;
                break;

            case "Phyton":
                resId = R.drawable.ic_skill_development_phyton;
                break;

            case "Ruby":
                resId = R.drawable.ic_skill_development_ruby;
                break;

            case "SQL":
                resId = R.drawable.ic_skill_development_sql;
                break;

            case "XML":
                resId = R.drawable.ic_skill_development_xml;
                break;


            //Icons Skill Design

            case "Blender":
                resId = R.drawable.ic_design_blender;
                break;

            case "InDesign":
                resId = R.drawable.ic_design_id;
                break;

            case "Fireworks":
                resId = R.drawable.ic_design_fw;
                break;

            case "Photoshop":
                resId = R.drawable.ic_design_ps;
                break;

            case "Illustrator":
                resId = R.drawable.ic_design_ai;
                break;

            case "AfterEffects":
                resId = R.drawable.ic_design_ae;
                break;

            default:
                resId = R.drawable.ic_social_facebook;
                break;
        }

        return resId;
    }


}
package com.example.lukas.resume.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lukas.resume.ContentFragment;
import com.example.lukas.resume.MainActivity;
import com.example.lukas.resume.R;
import com.example.lukas.resume.datamanagement.Form_Entries_Experience;
import com.example.lukas.resume.datamanagement.SQLiteHelper;
import com.example.lukas.resume.datamanagement.UserDataHub;
import com.example.lukas.resume.views.dialogs.InputDialogs;

import java.util.List;

/**
 * Created by Lukas on 14.05.2015.
 */
public class CustomExpandableListViewAdapterExperience extends BaseExpandableListAdapter {

    public static class ViewHolderGroup {
        TextView textView_GroupTitle;
        ImageView imageView_DeleteEntry;
        ImageView imageView_EditEntry;
    }

    public static class ViewHolderChild {
        TextView textView_Description;
        TextView textView_Location;
    }

    public ViewHolderGroup mViewHolderGroup;
    public ViewHolderChild mViewHolderChild;

    Context context;


    List<Form_Entries_Experience> mFormExperienceEntriesList;

    public CustomExpandableListViewAdapterExperience(Context context, ContentFragment parent, List<Form_Entries_Experience> mFormExperienceEntriesList) {
        this.context = context;
        this.mFormExperienceEntriesList = mFormExperienceEntriesList;
    }


    public void deleteEntry(long id) {
        UserDataHub.delete(SQLiteHelper.TABLE_EXPERIENCE, id);
        notifyDataSetChanged();
    }

    public void editEntry(long id) {


        FragmentTransaction ft = ((MainActivity) context).getSupportFragmentManager().beginTransaction();
        Fragment dialog = ((MainActivity) context).getSupportFragmentManager().findFragmentByTag("dialog");

        if (dialog != null) {
            ft.remove(dialog);
        }
        ft.addToBackStack(null);

        //Create and show dialog.
        InputDialogs newDialog = InputDialogs.newInstance(id, true, 3);
        newDialog.show(ft, "dialog");

    }


    @Override
    public int getGroupCount() {
        return mFormExperienceEntriesList.size();
    }


    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupPosition;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        mViewHolderGroup = new ViewHolderGroup();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.customexpandablelistview_experience_group, null);

        mViewHolderGroup.textView_GroupTitle = (TextView) convertView.findViewById(R.id.customexpandablelistview_experience_group_title);


        String dateBegin = mFormExperienceEntriesList.get(groupPosition).getDateBegin().toString();
        String dateEnd = mFormExperienceEntriesList.get(groupPosition).getDateEnd().toString() == "" ? "" : (" - " + mFormExperienceEntriesList.get(groupPosition).getDateEnd());


        String titleString = ": " + mFormExperienceEntriesList.get(groupPosition).getProfession();

        mViewHolderGroup.textView_GroupTitle.setText(dateBegin + dateEnd + titleString);

        mViewHolderGroup.imageView_DeleteEntry = (ImageView) convertView.findViewById(R.id.fragment_formgenerator_thirdstep_experience_fab_deleteentry);
        mViewHolderGroup.imageView_EditEntry = (ImageView) convertView.findViewById(R.id.fragment_formgenerator_thirdstep_experience_fab_editentry);


        // recycle the already inflated view
        //mViewHolderGroup = (ViewHolderGroup) convertView.getTag();

        //mViewHolderGroup.index = mEntriesList.get(groupPosition).index;

        mViewHolderGroup.imageView_DeleteEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(context)

                        .setMessage("Are you sure you want to delete this entry?")
                        .setIcon(0)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                deleteEntry(mFormExperienceEntriesList.get(groupPosition).getId());
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });

        mViewHolderGroup.imageView_EditEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editEntry(mFormExperienceEntriesList.get(groupPosition).getId());
            }
        });

        return convertView;

    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.customexpandablelistview_experience_child, null);


        mViewHolderChild = new ViewHolderChild();

        mViewHolderChild.textView_Description = (TextView) convertView.findViewById(R.id.customexpandablelistview_child_input_description);

        mViewHolderChild.textView_Description.setText(mFormExperienceEntriesList.get(groupPosition).getDescription());


        mViewHolderChild.textView_Location = (TextView) convertView.findViewById(R.id.customexpandablelistview_child_input_location);

        mViewHolderChild.textView_Location.setText(mFormExperienceEntriesList.get(groupPosition).getBorn());


        return convertView;

    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }


}

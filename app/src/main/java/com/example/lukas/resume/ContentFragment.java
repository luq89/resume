package com.example.lukas.resume;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;

import com.example.lukas.resume.datamanagement.UserDataHub;
import com.example.lukas.resume.views.dialogs.InputDialogs;
import com.example.lukas.resume.views.dialogs.WelcomeDialog;
import com.example.lukas.resume.views.sections.ViewHolders.BasicInfoViewHolder;
import com.example.lukas.resume.views.sections.ViewHolders.ConnectViewHolder;
import com.example.lukas.resume.views.sections.ViewHolders.EducationViewHolder;
import com.example.lukas.resume.views.sections.ViewHolders.GlobalViewHolder;
import com.example.lukas.resume.views.sections.ViewHolders.SkillsetViewHolder;
import com.example.lukas.resume.views.sections.ViewHolders.ViewHolderFinish;

import java.util.Map;

import static com.example.lukas.resume.helpers.Enums.SectionTitles.BASICINFORMATION;
import static com.example.lukas.resume.helpers.Enums.SectionTitles.EDUCATION;

public class ContentFragment extends Fragment {

    private static final String KEY_TITLE = "title";
    private static final String KEY_INDICATOR_COLOR = "indicator_color";
    private static final String KEY_DIVIDER_COLOR = "divider_color";
    private boolean SHOW_START_DIALOG = true;

    private GlobalViewHolder globalViewHolder;
    private ViewHolderFinish mViewHolderFinish;
    public BasicInfoViewHolder basicInfoSectionView;
    public SkillsetViewHolder skillsetSectionView;
    public ConnectViewHolder connectSectionView;
    public EducationViewHolder educationSectionView;


    public static ContentFragment newInstance(CharSequence title, int indicatorColor,
                                              int dividerColor) {

        Bundle bundle = new Bundle();
        bundle.putCharSequence(KEY_TITLE, title);
        bundle.putInt(KEY_INDICATOR_COLOR, indicatorColor);
        bundle.putInt(KEY_DIVIDER_COLOR, dividerColor);
        ContentFragment fragment = new ContentFragment();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        Bundle args = getArguments();
        final View rootView = inflater.inflate(R.layout.fragment_formgenerator, container, false);
        ViewStub viewStubContainer = (ViewStub) rootView.findViewById(R.id
                .fragment_formgenerator_viewstub_container);

        this.globalViewHolder = new GlobalViewHolder(getActivity(), this, viewStubContainer);


        if (args.getCharSequence(KEY_TITLE).equals(BASICINFORMATION.getName())) {

            initFirstStep();

            if (this.SHOW_START_DIALOG &&
                    UserDataHub.getGeneralData().getShowStartDialogAsBoolean()) {
                showWelcomeDialogIfEnabled();
            }


        } else if (args.getCharSequence(KEY_TITLE).equals(EDUCATION.getName())) {
            initEducationSection();

        }
/*

        } else if (args.getCharSequence(KEY_TITLE).equals(titles[3])) {

            globalViewHolder.setLayoutResource(R.layout.fragment_formgenerator_section_skillset);

            initSkillsetSection();


        } else if (args.getCharSequence(KEY_TITLE).equals(titles[4])) {

            globalViewHolder.setLayoutResource(R.layout.fragment_formgenerator_section_connect);

            initConnectSection();


        } else if (args.getCharSequence(KEY_TITLE).equals(titles[5])) {

            globalViewHolder.setLayoutResource(R.layout.fragment_formgenerator_section_finish);

            initFinishSection();

        }

        */

        return rootView;
    }

    private void initFinishSection() {
        mViewHolderFinish = new ViewHolderFinish(
                globalViewHolder.getContext(),
                globalViewHolder.getParent()
        );

        mViewHolderFinish.setOnClickListenerForExportFab(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment preview = getFragmentManager().findFragmentByTag("dialog");

                if (preview != null) {
                    ft.remove(preview);
                }
                ft.addToBackStack(null);

                //Create and show dialog.
                FormFragment newPreview = FormFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString("CachePath", getActivity().getCacheDir().toString());
                newPreview.setTargetFragment(ContentFragment.this, 0);
                newPreview.setArguments(bundle);
                newPreview.show(ft, "preview");
            }
        });


        mViewHolderFinish.setOnClickListenerForShowImpressumTextView(new View
                .OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment dialog = getFragmentManager().findFragmentByTag("dialog");
                if (dialog != null) {
                    ft.remove(dialog);
                }
                ft.addToBackStack(null);

                //Create and show dialog.
                InputDialogs newDialog = InputDialogs.newInstance(-1, false, 5);
                newDialog.show(ft, "dialog");
            }
        });


        mViewHolderFinish.setOnClickListenerForShowResetProgress(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(getActivity())

                        .setMessage("Are you sure you want to reset your progress?")
                        .setIcon(0)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                UserDataHub.reset();
                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }


        });


    }

    private void initConnectSection() {
        connectSectionView = new ConnectViewHolder(
                globalViewHolder.getContext(),
                globalViewHolder.getParent()
        );

        connectSectionView.setOnClickListenerForAddSkillFab(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment dialog = getFragmentManager().findFragmentByTag("dialog");
                if (dialog != null) {
                    ft.remove(dialog);
                }
                ft.addToBackStack(null);

                //Create and show dialog.
                InputDialogs newDialog = InputDialogs.newInstance(1, false, 4);
                newDialog.show(ft, "dialog");

            }
        });
    }

    private void initEducationSection() {
        educationSectionView = new EducationViewHolder(globalViewHolder);
    }

    private void initSkillsetSection() {

        skillsetSectionView = new SkillsetViewHolder(
                globalViewHolder.getContext(),
                globalViewHolder.getParent()
        );

        globalViewHolder.setOnClickListenerForShowPreviewFab(showPreviewOnClickListener());
        skillsetSectionView.setOnClickListenerForAddSkillFab(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment dialog = getFragmentManager().findFragmentByTag("dialog");
                if (dialog != null) {
                    ft.remove(dialog);
                }
                ft.addToBackStack(null);

                //Create and show dialog.
                InputDialogs newDialog = InputDialogs.newInstance(1, false, 2);

                newDialog.show(ft, "dialog");

            }
        });

        skillsetSectionView.setOnClickListenerForAddLanguageFab(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment dialog = getFragmentManager().findFragmentByTag("dialog");
                if (dialog != null) {
                    ft.remove(dialog);
                }
                ft.addToBackStack(null);

                //Create and show dialog.
                InputDialogs newDialog = InputDialogs.newInstance(1, false, 3);

                newDialog.show(ft, "dialog");
            }
        });
    }

    private void initFirstStep() {


        basicInfoSectionView = new BasicInfoViewHolder(
                globalViewHolder
        );

        basicInfoSectionView.setClickListenerForImportContactButton(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickContact(v);
            }
        });

        basicInfoSectionView.setClickListenerForUploadImage(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImage(v);
            }
        });
    }

    private void showWelcomeDialogIfEnabled() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.addToBackStack(null);
        //Create and show dialog.
        WelcomeDialog newDialog = WelcomeDialog.newInstance(getActivity());
        newDialog.setTargetFragment(ContentFragment.this, 0);
        newDialog.show(ft, "dialog");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    public View.OnClickListener showPreviewOnClickListener() {

        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment preview = getFragmentManager().findFragmentByTag("dialog");

                if (preview != null) {
                    ft.remove(preview);
                }
                ft.addToBackStack(null);
                //Create and show dialog.
                FormFragment newPreview = FormFragment.newInstance();
                newPreview.setTargetFragment(ContentFragment.this, 0);
                newPreview.show(ft, "preview");
            }
        };
    }


    public void updateContact(Map<String, String> map) {
        String NAME = "name";
        String MAIL = "mail";
        String PHONE = "phone";
        String BIRTHDAY = "birthday";
        String ADDRESS = "address";


        if (null != basicInfoSectionView) {


            //Name
            basicInfoSectionView.setTextForEditables(
                    basicInfoSectionView.getNameEditable(),
                    map.get(NAME)
            );


            //Birthday

            /*

            if (null != map.get(BIRTHDAY)) {


                String format = getResources().getString(R.string.default_dateformat);

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-DD", Locale.GERMAN);
                try {
                    java.util.Date date = df.parse(map.get(BIRTHDAY));
                    globalViewHolder.mSimpleDateFormat = new SimpleDateFormat(format, Locale.GERMAN);
                    basicInfoSectionView.textView_Born.setTextForEditables(globalViewHolder.mSimpleDateFormat.format(date));
                    basicInfoSectionView.textView_Born.setTextColor(getResources().getColor(R.color.Font_Dark));

                } catch (ParseException e) {

                    if (!map.containsKey(BIRTHDAY)) {
                        java.util.Date date = new Date();
                        basicInfoSectionView.textView_Born.setTextForEditables(globalViewHolder.mSimpleDateFormat.format(date.getTime()));

                    } else {
                        basicInfoSectionView.textView_Born.setTextForEditables(map.get(BIRTHDAY));
                    }
                }
            } else {
                java.util.Date date = new Date();
                basicInfoSectionView.textView_Born.setTextForEditables(globalViewHolder.mSimpleDateFormat.format(date.getTime()));
                basicInfoSectionView.textView_Born.setTextColor(getResources().getColor(R.color.Font_Error));
            }
            */

            //Address
            basicInfoSectionView.setTextForEditables(
                    basicInfoSectionView.getNameEditable(),
                    map.get(NAME)
            );

            //Email
            basicInfoSectionView.setTextForEditables(
                    basicInfoSectionView.getEmailEditable(),
                    map.get(MAIL)
            );

            //Phone
            basicInfoSectionView.setTextForEditables(
                    basicInfoSectionView.getAddressEditable(),
                    map.get(PHONE)
            );
        }

        resetImportContactFab();
    }

    public void resetImportContactFab() {
        basicInfoSectionView.setImportContactButtonPressed(false);
    }


    public void updateAvatarImage(byte[] byteArray) {
        Bitmap bm = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        basicInfoSectionView.setAvatar(bm);
        basicInfoSectionView.setVisibilityOfProgressIndicator(View.INVISIBLE);
    }


    public void pickImage(View v) {
        int PICK_IMAGE = 1;

        basicInfoSectionView.setVisibilityOfProgressIndicator(View.VISIBLE);
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        getActivity().startActivityForResult(intent, PICK_IMAGE);
    }

    public void pickContact(View v) {
        int PICK_CONTACT = 2015;

        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        getActivity().startActivityForResult(intent, PICK_CONTACT);
    }


    /*
     * @TODO what dialog?
     */
    public void showDialog() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment dialog = getFragmentManager().findFragmentByTag("dialog");

        if (dialog != null) {
            ft.remove(dialog);
        }
        ft.addToBackStack(null);

        //Create and show dialog.
        InputDialogs newDialog = InputDialogs.newInstance(-1, false, 0);

        newDialog.show(ft, "dialog");

    }


    public void updateEducationAdapter() {
        educationSectionView.updateAdapter();
    }


    public void updateExperienceAdapter() {
    }


    public void updateSkillAdapter() {

        /*
        if (null != skillsetSectionView.adapterSkillset) {
            skillsetSectionView.adapterSkillset.notifyDataSetInvalidated();
        }
        */

        skillsetSectionView.updateAdapter();
    }


    public void updateLanguageAdapter() {
        /*
        if (null != skillsetSectionView.adapterLanguage) {
            skillsetSectionView.adapterLanguage.notifyDataSetInvalidated();
        }
        */

        skillsetSectionView.updateAdapter();


    }


    public void updateConnectAdapter() {
        connectSectionView.updateAdapter();
    }


    @Override
    public void onResume() {
        super.onResume();
        globalViewHolder.setFabPressed(false);

    }

    /*

    @Override
    public void onDismissPreview() {

    }




    @Override
    public void onDateFormatSelected(String dateFormat) {

        if ("" != dateFormat) {
            ContentValues inputValues = new ContentValues();
            inputValues.clear();
            inputValues.put(SQLiteHelper.COLUMN_GENERAL_DATEFORMAT, dateFormat);
            UserDataHub.update(SQLiteHelper.TABLE_GENERAL, inputValues);
        }
    }


    @Override
    public void onFontStyleSelected(String fontStyle) {

        ContentValues inputValues = new ContentValues();
        inputValues.clear();

        if (null != fontStyle && "" != fontStyle) {
            inputValues.put(SQLiteHelper.COLUMN_GENERAL_TYPEFACE, fontStyle);
        }

        if (inputValues.size() > 0) {
            UserDataHub.update(SQLiteHelper.TABLE_GENERAL, inputValues);
            UserDataHub.refreshUserData();
            UserDataHub.replace(
                    SQLiteHelper.TABLE_GENERAL, SQLiteHelper
                            .COLUMN_GENERAL_TYPEFACE,
                    inputValues
            );
        }
    }

    @Override
    public void onSwitchToogle(boolean option) {

        ContentValues inputValues = new ContentValues();
        inputValues.clear();

        inputValues.put(SQLiteHelper.COLUMN_GENERAL_SHOWSTARTDIALOG, option == true ? "true" : "false");

        if (inputValues.size() > 0) {
            UserDataHub.update(SQLiteHelper.TABLE_GENERAL, inputValues);
            UserDataHub.refreshUserData();
        }
    }

    @Override
    public void onGlobalDismissStartDialog() {
        this.SHOW_START_DIALOG = false;
    }

    */


}
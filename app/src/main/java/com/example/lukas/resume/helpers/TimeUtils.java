package com.example.lukas.resume.helpers;


import android.content.Context;
import android.util.Log;

import net.danlew.android.joda.JodaTimeAndroid;

import org.joda.time.DateTime;

/**
 * Created by luq89 on 17-Mar-17.
 */

public class TimeUtils {

    private static Context context;
    private static DateTime lastUpdated;
    private static final long REFRESHRATE = 2000;
    private static boolean isInitialStart;

    public static TimeUtils newInstance(Context context) {
        return new TimeUtils(context);
    }

    private TimeUtils(Context context) {
        TimeUtils.context = context;
        JodaTimeAndroid.init(context);
        lastUpdated = DateTime.now();
        isInitialStart = true;
    }


    public static boolean canRefresh(DateTime now) {
        boolean canRefresh = false;
        long diffInMilli = now.getMillis() - lastUpdated.getMillis();

        if (isInitialStart) {
            canRefresh = true;
            isInitialStart = false;
        } else if (diffInMilli > REFRESHRATE) {
            canRefresh = true;
            lastUpdated = now;
            //toCustomString(now);
        }

        Log.v("diff", String.valueOf(diffInMilli) + " : " + String.valueOf(canRefresh));

        return canRefresh;
    }

    private static void toCustomString(DateTime now) {
        Log.v("TimtUtils updateTime", now.toString());
    }


}

package com.example.lukas.resume.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.lukas.resume.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Lukas on 14.05.2015.
 */
public class CustomSpinnerAdapter_DateFormat extends ArrayAdapter {


    String[] val;


    public CustomSpinnerAdapter_DateFormat(Context ctx, int txtViewResourceId, String[] objects) {
        super(ctx, txtViewResourceId, objects);

        val = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }


    public View getCustomView(int position, View convertView,
                              ViewGroup parent) {


        LayoutInflater inflater = LayoutInflater.from(getContext());

        View mySpinner = inflater.inflate(R.layout.customspinner_dateformat, parent,
                false);


        TextView dateFormat = (TextView) mySpinner
                .findViewById(R.id.customspinner_textview_date_format);
        dateFormat.setText(val[position]);

        Calendar date = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(val[position], Locale.GERMAN);

        TextView preview = (TextView) mySpinner
                .findViewById(R.id.customspinner_textview_preview);
        preview.setText("// " + df.format(date.getTime()));


        return mySpinner;

    }


}
package com.example.lukas.resume.datamanagement;

/**
 * Created by luq89 on 13-Mar-17.
 */

interface UserEntry {


    long getId();

    void setId(long id);

    String toCustomString();

}

package com.example.lukas.resume;


import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.danielnilsson9.colorpickerview.view.ColorPickerView;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Lukas on 19.11.2015.
 */
public class FormGeneratorFragment_Dialog_ColorPicker extends DialogFragment {


    static ArrayList<Integer> mColorHistory;
    static int mInitialColor;
    int mSelectedColor;

    OnFragmentInteractionListener mCallback;


    /*
    public static FormGeneratorFragment_Dialog_ColorPicker newInstance(int id, int target,  int initColor, ArrayList<Integer> colorHistory) {
        FormGeneratorFragment_Dialog_ColorPicker f = new FormGeneratorFragment_Dialog_ColorPicker();

        mInitialColor = initColor;
        mColorHistory = colorHistory;
        mId = id;
        mTarget = target;

        f.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        return f;
    }

    public FormGeneratorFragment_Dialog_ColorPicker() {

    }
    */


    public static FormGeneratorFragment_Dialog_ColorPicker newInstance(int initColor, ArrayList<Integer> colorHistory) {
        FormGeneratorFragment_Dialog_ColorPicker f = new FormGeneratorFragment_Dialog_ColorPicker();

        mInitialColor = initColor;
        mColorHistory = colorHistory;
        f.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        return f;
    }

    public FormGeneratorFragment_Dialog_ColorPicker() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_formgenerator_dialog_colorpicker, container, false);


        TextView textView_Title = (TextView) rootView.findViewById(R.id.fragment_form_dialog_colorpicker_title);

        //textView_Title.setTextForEditables(mTarget == 0 ? getResources().getString(R.string.fragment_formgenerator_dialog_colorpicker_title_tint) : getResources().getString(R.string.fragment_formgenerator_dialog_colorpicker_title_font));
        textView_Title.setText(getResources().getString(R.string.fragment_formgenerator_dialog_colorpicker_title_font));

        TextView textView_Apply = (TextView) rootView.findViewById(R.id.fragment_form_dialog_colorpicker_apply);
        textView_Apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (null != mCallback && mSelectedColor != mInitialColor) {
                    mCallback.onFormColorSelected(mSelectedColor);

                }
                dismiss();
            }
        });

        LinearLayout linearLayout_dismiss = (LinearLayout) rootView.findViewById(R.id.fragment_form_dialog_colorpicker_dismiss);
        linearLayout_dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        final LinearLayout mLinearLayout = (LinearLayout) rootView.findViewById(R.id.fragment_formgenerator_dialog_colorpicker_container);
        final ColorPickerView colorPickerView = new ColorPickerView(getActivity());
        final LinearLayout mLinearLayout_Preview = (LinearLayout) rootView.findViewById(R.id.fragment_formgenerator_dialog_colorpicker_colorpreview);
        final EditText mEditText_HexValue = (EditText) rootView.findViewById(R.id.fragment_formgenerator_dialog_colorpicker_hexvalue);


        mLinearLayout_Preview.setBackground(changeColorOfDrawable(mInitialColor));
        colorPickerView.setColor(mInitialColor);
        mEditText_HexValue.setText(String.format("#%06X", (0xFFFFFF & mInitialColor)));
        mSelectedColor = mInitialColor;


        colorPickerView.setOnColorChangedListener(new ColorPickerView.OnColorChangedListener() {
            @Override
            public void onColorChanged(int i) {
                Drawable drawable = getActivity().getResources().getDrawable(R.drawable.helper_roundish_square_small);
                drawable.setTint(i);
                mLinearLayout_Preview.setBackground(drawable);
                mEditText_HexValue.setText(String.format("#%06X", (0xFFFFFF & i)));
                mEditText_HexValue.setTextColor(getResources().getColor(R.color.Font_Light));
                mSelectedColor = i;


            }
        });


        TextWatcher mTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                Pattern colorPattern = Pattern.compile("#([0-9a-f]{6})");
                Matcher m = colorPattern.matcher(s);

                if (m.matches()) {
                    colorPickerView.setColor(Color.parseColor(String.valueOf(s)));
                    mEditText_HexValue.setTextColor(getResources().getColor(R.color.Font_Light));
                    mLinearLayout_Preview.setBackground(changeColorOfDrawable(Color.parseColor(String.valueOf(s))));
                    mSelectedColor = Color.parseColor(String.valueOf(s));


                } else {

                    mEditText_HexValue.setTextColor(getResources().getColor(R.color.Font_Error));

                }
            }
        };

        mEditText_HexValue.addTextChangedListener(mTextWatcher);


        LinearLayout mLinearLayout_colorHistory = (LinearLayout) rootView.findViewById(R.id.fragment_formgenerator_dialog_colorpicker_colorhistory);


        for (int i = 0; i < mColorHistory.size(); i++) {

            LinearLayout ll = new LinearLayout(getActivity());
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    50, 50);

            lp.setMargins(0, 15, 0, 0);
            ll.setBackground(changeColorOfDrawable(mColorHistory.get(i)));
            mLinearLayout_colorHistory.addView(ll, lp);

            final int index = i;

            ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    colorPickerView.setColor(mColorHistory.get(index));
                    mEditText_HexValue.setText(String.format("#%06X", (0xFFFFFF & mColorHistory.get(index))));
                    mEditText_HexValue.setTextColor(getResources().getColor(R.color.Font_Light));
                    mLinearLayout_Preview.setBackground(changeColorOfDrawable(mColorHistory.get(index)));
                    mSelectedColor = mColorHistory.get(index);

                }
            });
        }


        mLinearLayout.addView(colorPickerView, 600, 400);

        return rootView;


    }


    private Drawable changeColorOfDrawable(int color) {

        Drawable drawable = getActivity().getResources().getDrawable(R.drawable.helper_roundish_square_small);
        drawable.setTint(color);
        return drawable;
    }


    @Override
    public void show(FragmentManager manager, String tag) {
        super.show(manager, tag);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {

            mCallback = (OnFragmentInteractionListener) getTargetFragment();

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public interface OnFragmentInteractionListener {
        void onFormColorSelected(int color);
    }
}

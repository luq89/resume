package com.example.lukas.resume.views.sections.ViewHolders;

import android.content.Context;
import android.view.View;
import android.widget.ExpandableListView;

import com.example.lukas.resume.ContentFragment;
import com.example.lukas.resume.R;
import com.example.lukas.resume.adapter.CustomExpandableListViewAdapterLanguage;
import com.example.lukas.resume.adapter.CustomExpandableListViewAdapterSkill;
import com.example.lukas.resume.datamanagement.UserDataHub;
import com.getbase.floatingactionbutton.FloatingActionButton;

/**
 * Created by luq89 on 08-Mar-17.
 */


public class SkillsetViewHolder extends GlobalViewHolder {

    private FloatingActionButton addSkillFab;
    private FloatingActionButton addLanguageFab;
    private ExpandableListView expandableListViewSkill;
    private CustomExpandableListViewAdapterSkill adapterSkill;

    private ExpandableListView expandableListViewLanguage;
    private CustomExpandableListViewAdapterLanguage adapterLanguage;


    public SkillsetViewHolder(Context context, ContentFragment parent) {
        super(context, parent);


        //Skill

        this.expandableListViewSkill = (ExpandableListView) super.findViewWithinSubviewById(R.id
                .fragment_formgenerator_fourthstep_skill_expandablelistview);

        this.adapterSkill = new CustomExpandableListViewAdapterSkill(super.getContext(), super
                .getParent(), UserDataHub.getSkillData());

        this.expandableListViewSkill.setAdapter(adapterSkill);

        this.addSkillFab = (FloatingActionButton) super.findViewWithinSubviewById(R.id
                .fragment_formgenerator_skillset_fab_addskill);

        this.addSkillFab = (FloatingActionButton) super.findViewWithinSubviewById(R.id.fragment_formgenerator_skillset_fab_addlanguage);


        //Language

        this.expandableListViewLanguage = (ExpandableListView) super.findViewWithinSubviewById(
                R.id.fragment_formgenerator_fourthstep_language_expandablelistview);

        this.adapterLanguage = new CustomExpandableListViewAdapterLanguage(super.getContext(), super
                .getParent(), UserDataHub.getLanguageData());

        this.expandableListViewSkill.setAdapter(adapterSkill);

        this.addLanguageFab = (FloatingActionButton) super.findViewWithinSubviewById(R.id
                .fragment_formgenerator_skillset_fab_addskill);

    }

    public void setOnClickListenerForAddSkillFab(View.OnClickListener listener) {
        this.addSkillFab.setOnClickListener(listener);
    }

    public void setOnClickListenerForAddLanguageFab(View.OnClickListener listener) {
        this.addLanguageFab.setOnClickListener(listener);
    }

    private void updateSkillAdapter() {
        this.expandableListViewSkill.setAdapter(new CustomExpandableListViewAdapterSkill(
                super.getContext(),
                super.getParent(),
                UserDataHub.getSkillData()
        ));
    }

    private void updateLanguageAdapter() {
        this.expandableListViewLanguage.setAdapter(new CustomExpandableListViewAdapterLanguage(
                super.getContext(),
                super.getParent(),
                UserDataHub.getLanguageData()
        ));
    }

    public void updateAdapter() {
        updateSkillAdapter();
        updateLanguageAdapter();
    }

}


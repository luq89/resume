package com.example.lukas.resume.datamanagement;

/**
 * Created by Lukas on 06.05.2015.
 */

public class Form_Entries_Language implements UserEntry {


    private long id;

    private String language_Name;
    private double language_Rating;

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public String getLanguage() {
        return language_Name;
    }

    public void setLanguage(String lang) {
        this.language_Name = lang;
    }


    public double getRating() {
        return language_Rating;
    }

    public void setRating(double rating) {
        this.language_Rating = rating;
    }


    @Override
    public String toCustomString() {
        return
                "ID" + id + " ; "
                        + "Language: " + language_Name + " ; "
                        + "Rating: " + language_Rating + " ; "
                ;
    }

}

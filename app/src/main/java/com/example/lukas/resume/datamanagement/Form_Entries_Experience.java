package com.example.lukas.resume.datamanagement;

/**
 * Created by Lukas on 06.05.2015.
 */

public class Form_Entries_Experience implements UserEntry {


    private long id;

    private String experience_DateBeginn;
    private String experience_DateEnd;
    private String experience_Subject;
    private String experience_Location;
    private String experience_Description;


    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }


    public String getDateBegin() {
        return experience_DateBeginn;
    }

    public void setDateBeginn(String dateBegin) {
        this.experience_DateBeginn = dateBegin;
    }


    public String getDateEnd() {
        return experience_DateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.experience_DateEnd = dateEnd;
    }


    //Profession
    public String getProfession() {
        return experience_Subject;
    }

    public void setSubject(String subject) {
        this.experience_Subject = subject;
    }


    //Born
    public String getBorn() {
        return experience_Location;
    }

    public void setBorn(String location) {
        this.experience_Location = location;
    }


    //Description
    public String getDescription() {
        return experience_Description;
    }

    public void setDescription(String description) {
        this.experience_Description = description;
    }


    // Will be used by the ArrayAdapter in the ListView
    @Override
    public String toCustomString() {
        return
                "ID" + id + " ; "
                        + "DateBegin: " + experience_DateBeginn + " ; "
                        + "DateEnd: " + experience_DateEnd + " ; "
                        + "What: " + experience_Subject + " ; "
                        + "Where: " + experience_Location + " ; "
                ;
    }

}
